/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Different types of page label numerals.
 * @author Christian Packenius, 2013.
 */
public enum XSPageLabelStandardType implements XSPdfContent {
    /**
     * Use "usual" arabic numerals for page labeling: 1, 2, 3, 4, ...
     */
    DecimalArabicPageLabel("D"),

    /**
     * Use uppercase roman numerals for page labeling: I, II, III, IV, ...
     */
    UpperRomanPageLabel("R"),

    /**
     * Use lowercase roman numerals for page labeling: i, ii, iii, iv, ...
     */
    LowerRomanPageLabel("r"),

    /**
     * Use uppercase letters for page labeling: A, B, C, D, ...
     */
    UpperLettersPageLabel("A"),

    /**
     * Use lowercase letters for page labeling: a, b, c, d, ...
     */
    LowerLettersPageLabel("a");

    private final String numeralType;

    XSPageLabelStandardType(String numeralType) {
        this.numeralType = "/S/" + numeralType;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        return numeralType;
    }
}
