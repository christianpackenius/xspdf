package com.packenius.library.xspdf;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Christian Packenius, 2013.
 */
abstract class XST1StdFont {
	abstract String getFontName();

	/**
	 * Maps a character code (0..65535) to the width of this glyph.
	 */
	final double[] glyphWidths = new double[65536];

	/**
	 * Maps a character code (0..65535) to the byte in the standard font
	 * encoding.
	 */
	final byte[] standardEncodingCodeFromUnicodeCharacter = new byte[65536];

	/**
	 * Maps a glyph fontType pair ("glyphname1-glyphname2") to the kerning
	 * distance.
	 */
	final Map<String, Double> glyphPairKerning = new HashMap<String, Double>();

	double getAscender() {
		// Please override!
		return 1.0;
	}

	double getDescender() {
		// Please override!
		return 0.0;
	}

	XST1StdFont() {
		addCharMetrics();
		addKernPairs();
	}

	abstract void addCharMetrics();

	void addKernPairs() {
		// Overwrite!
	}

	double getKerning(String glyph1, String glyph2) {
		Double kerning = glyphPairKerning.get(glyph1 + "-" + glyph2);
		if (kerning == null) {
			return 0;
		}
		return kerning;
	}

	/**
	 * If a character is part of this font but not part of the standard font
	 * encoding, get another encoding here.
	 * 
	 * @param character
	 * @return
	 */
	XSAlternativeFontEncoding getAlternativeFontEncoding(char character) {
		String fontName = getFontName();
		// System.out.println("XST1StdFont.name := " + fontName);
		if (fontName.equals("Symbol")) {
			if (character == '\uF8FF') {
				return XST1AltFontEncoding4.instance;
			}
			throw new XSPdfException(
					"Symbol with character 0x" + Integer.toHexString(character) + " is standard encoded!");
		}
		if (fontName.equals("ZapfDingbats")) {
			throw new XSPdfException(fontName + " has no alternative font encoding!");
		}
		if (standardEncodingCodeFromUnicodeCharacter[character] != 0) {
			// Programming error?!
			throw new XSPdfException(
					fontName + " with character 0x" + Integer.toHexString(character) + " is standard encoded!");
		}
		if (XST1AltFontEncoding1.altFontEncoding1[character] != 0) {
			return XST1AltFontEncoding1.instance;
		}
		if (XST1AltFontEncoding2.altFontEncoding2[character] != 0) {
			return XST1AltFontEncoding2.instance;
		}
		if (XST1AltFontEncoding3.altFontEncoding3[character] != 0) {
			return XST1AltFontEncoding3.instance;
		}
		throw new XSPdfException("Glyph 0x" + Integer.toHexString(character) + " does not exist in font " + fontName);
	}
}
