package com.packenius.library.xspdf;

class XST1StdFontSymbol extends XST1StdFont {
	@Override
	String getFontName() {
		return "Symbol";
	}

	/**
	 * Pairwise data: First of each pair is a unicode character. Second is the
	 * width (in 1/1000) of this character.
	 */
	private static final char[] validGlyphsAndWidths = new char[] { '\u0020', (char) 250, '\u0021', (char) 333,
			'\u0023', (char) 500, '\u0025', (char) 833, '\u0026', (char) 778, '\u0028', (char) 333, '\u0029',
			(char) 333, '\u002b', (char) 549, '\u002c', (char) 250, '\u002e', (char) 250, '\u002f', (char) 278,
			'\u0030', (char) 500, '\u0031', (char) 500, '\u0032', (char) 500, '\u0033', (char) 500, '\u0034',
			(char) 500, '\u0035', (char) 500, '\u0036', (char) 500, '\u0037', (char) 500, '\u0038', (char) 500,
			'\u0039', (char) 500, '\u003a', (char) 278, '\u003b', (char) 278, '\u003c', (char) 549, '\u003d',
			(char) 549, '\u003e', (char) 549, '\u003f', (char) 444, '\u005b', (char) 333, '\u005d', (char) 333,
			'\u005f', (char) 500, '\u007b', (char) 480, '\u007c', (char) 200, '\u007d', (char) 480, '\u00ac',
			(char) 713, '\u00b0', (char) 400, '\u00b1', (char) 549, '\u00b5', (char) 576, '\u00d7', (char) 549,
			'\u00f7', (char) 549, '\u0192', (char) 500, '\u0391', (char) 722, '\u0392', (char) 667, '\u0393',
			(char) 603, '\u0395', (char) 611, '\u0396', (char) 611, '\u0397', (char) 722, '\u0398', (char) 741,
			'\u0399', (char) 333, '\u039a', (char) 722, '\u039b', (char) 686, '\u039c', (char) 889, '\u039d',
			(char) 722, '\u039e', (char) 645, '\u039f', (char) 722, '\u03a0', (char) 768, '\u03a1', (char) 556,
			'\u03a3', (char) 592, '\u03a4', (char) 611, '\u03a5', (char) 690, '\u03a6', (char) 763, '\u03a7',
			(char) 722, '\u03a8', (char) 795, '\u03b1', (char) 631, '\u03b2', (char) 549, '\u03b3', (char) 411,
			'\u03b4', (char) 494, '\u03b5', (char) 439, '\u03b6', (char) 494, '\u03b7', (char) 603, '\u03b8',
			(char) 521, '\u03b9', (char) 329, '\u03ba', (char) 549, '\u03bb', (char) 549, '\u03bd', (char) 521,
			'\u03be', (char) 493, '\u03bf', (char) 549, '\u03c0', (char) 549, '\u03c1', (char) 549, '\u03c2',
			(char) 439, '\u03c3', (char) 603, '\u03c4', (char) 439, '\u03c5', (char) 576, '\u03c6', (char) 521,
			'\u03c7', (char) 549, '\u03c8', (char) 686, '\u03c9', (char) 686, '\u03d1', (char) 631, '\u03d2',
			(char) 620, '\u03d5', (char) 603, '\u03d6', (char) 713, '\u2022', (char) 460, '\u2026', (char) 1000,
			'\u2032', (char) 247, '\u2033', (char) 411, '\u2044', (char) 167, '\u20ac', (char) 750, '\u2111',
			(char) 686, '\u2118', (char) 987, '\u211c', (char) 795, '\u2126', (char) 768, '\u2135', (char) 823,
			'\u2190', (char) 987, '\u2191', (char) 603, '\u2192', (char) 987, '\u2193', (char) 603, '\u2194',
			(char) 1042, '\u21b5', (char) 658, '\u21d0', (char) 987, '\u21d1', (char) 603, '\u21d2', (char) 987,
			'\u21d3', (char) 603, '\u21d4', (char) 1042, '\u2200', (char) 713, '\u2202', (char) 494, '\u2203',
			(char) 549, '\u2205', (char) 823, '\u2206', (char) 612, '\u2207', (char) 713, '\u2208', (char) 713,
			'\u2209', (char) 713, '\u220b', (char) 439, '\u220f', (char) 823, '\u2211', (char) 713, '\u2212',
			(char) 549, '\u2217', (char) 500, '\u221a', (char) 549, '\u221d', (char) 713, '\u221e', (char) 713,
			'\u2220', (char) 768, '\u2227', (char) 603, '\u2228', (char) 603, '\u2229', (char) 768, '\u222a',
			(char) 768, '\u222b', (char) 274, '\u2234', (char) 863, '\u223c', (char) 549, '\u2245', (char) 549,
			'\u2248', (char) 549, '\u2260', (char) 549, '\u2261', (char) 549, '\u2264', (char) 549, '\u2265',
			(char) 549, '\u2282', (char) 713, '\u2283', (char) 713, '\u2284', (char) 713, '\u2286', (char) 713,
			'\u2287', (char) 713, '\u2295', (char) 768, '\u2297', (char) 768, '\u22a5', (char) 658, '\u22c5',
			(char) 250, '\u2320', (char) 686, '\u2321', (char) 686, '\u2329', (char) 329, '\u232a', (char) 329,
			'\u25ca', (char) 494, '\u2660', (char) 753, '\u2663', (char) 753, '\u2665', (char) 753, '\u2666',
			(char) 753, '\uf6d9', (char) 790, '\uf6da', (char) 790, '\uf6db', (char) 890, '\uf8e5', (char) 500,
			'\uf8e6', (char) 603, '\uf8e7', (char) 1000, '\uf8e8', (char) 790, '\uf8e9', (char) 790, '\uf8ea',
			(char) 786, '\uf8eb', (char) 384, '\uf8ec', (char) 384, '\uf8ed', (char) 384, '\uf8ee', (char) 384,
			'\uf8ef', (char) 384, '\uf8f0', (char) 384, '\uf8f1', (char) 494, '\uf8f2', (char) 494, '\uf8f3',
			(char) 494, '\uf8f4', (char) 494, '\uf8f5', (char) 686, '\uf8f6', (char) 384, '\uf8f7', (char) 384,
			'\uf8f8', (char) 384, '\uf8f9', (char) 384, '\uf8fa', (char) 384, '\uf8fb', (char) 384, '\uf8fc',
			(char) 494, '\uf8fd', (char) 494, '\uf8fe', (char) 494, '\uf8ff', (char) 790, };

	/**
	 * Pairwise data: First is the character code, second is the standard font
	 * encoding byte within the PDF text stream.
	 */
	private static final char[] standardEncodingCodes = new char[] { (char) 123, (char) 123, (char) 124, (char) 124,
			(char) 125, (char) 125, (char) 172, (char) 216, (char) 176, (char) 176, (char) 177, (char) 177, (char) 181,
			(char) 109, (char) 215, (char) 180, (char) 247, (char) 184, (char) 32, (char) 32, (char) 33, (char) 33,
			(char) 35, (char) 35, (char) 37, (char) 37, (char) 38, (char) 38, (char) 40, (char) 40, (char) 402,
			(char) 166, (char) 41, (char) 41, (char) 43, (char) 43, (char) 44, (char) 44, (char) 46, (char) 46,
			(char) 47, (char) 47, (char) 48, (char) 48, (char) 49, (char) 49, (char) 50, (char) 50, (char) 51,
			(char) 51, (char) 52, (char) 52, (char) 53, (char) 53, (char) 54, (char) 54, (char) 55, (char) 55,
			(char) 56, (char) 56, (char) 57, (char) 57, (char) 58, (char) 58, (char) 59, (char) 59, (char) 60,
			(char) 60, (char) 61, (char) 61, (char) 62, (char) 62, (char) 63, (char) 63, (char) 63193, (char) 211,
			(char) 63194, (char) 210, (char) 63195, (char) 212, (char) 63717, (char) 96, (char) 63718, (char) 189,
			(char) 63719, (char) 190, (char) 63720, (char) 226, (char) 63721, (char) 227, (char) 63722, (char) 228,
			(char) 63723, (char) 230, (char) 63724, (char) 231, (char) 63725, (char) 232, (char) 63726, (char) 233,
			(char) 63727, (char) 234, (char) 63728, (char) 235, (char) 63729, (char) 236, (char) 63730, (char) 237,
			(char) 63731, (char) 238, (char) 63732, (char) 239, (char) 63733, (char) 244, (char) 63734, (char) 246,
			(char) 63735, (char) 247, (char) 63736, (char) 248, (char) 63737, (char) 249, (char) 63738, (char) 250,
			(char) 63739, (char) 251, (char) 63740, (char) 252, (char) 63741, (char) 253, (char) 63742, (char) 254,
			(char) 8226, (char) 183, (char) 8230, (char) 188, (char) 8242, (char) 162, (char) 8243, (char) 178,
			(char) 8260, (char) 164, (char) 8364, (char) 160, (char) 8465, (char) 193, (char) 8472, (char) 195,
			(char) 8476, (char) 194, (char) 8486, (char) 87, (char) 8501, (char) 192, (char) 8592, (char) 172,
			(char) 8593, (char) 173, (char) 8594, (char) 174, (char) 8595, (char) 175, (char) 8596, (char) 171,
			(char) 8629, (char) 191, (char) 8656, (char) 220, (char) 8657, (char) 221, (char) 8658, (char) 222,
			(char) 8659, (char) 223, (char) 8660, (char) 219, (char) 8704, (char) 34, (char) 8706, (char) 182,
			(char) 8707, (char) 36, (char) 8709, (char) 198, (char) 8710, (char) 68, (char) 8711, (char) 209,
			(char) 8712, (char) 206, (char) 8713, (char) 207, (char) 8715, (char) 39, (char) 8719, (char) 213,
			(char) 8721, (char) 229, (char) 8722, (char) 45, (char) 8727, (char) 42, (char) 8730, (char) 214,
			(char) 8733, (char) 181, (char) 8734, (char) 165, (char) 8736, (char) 208, (char) 8743, (char) 217,
			(char) 8744, (char) 218, (char) 8745, (char) 199, (char) 8746, (char) 200, (char) 8747, (char) 242,
			(char) 8756, (char) 92, (char) 8764, (char) 126, (char) 8773, (char) 64, (char) 8776, (char) 187,
			(char) 8800, (char) 185, (char) 8801, (char) 186, (char) 8804, (char) 163, (char) 8805, (char) 179,
			(char) 8834, (char) 204, (char) 8835, (char) 201, (char) 8836, (char) 203, (char) 8838, (char) 205,
			(char) 8839, (char) 202, (char) 8853, (char) 197, (char) 8855, (char) 196, (char) 8869, (char) 94,
			(char) 8901, (char) 215, (char) 8992, (char) 243, (char) 8993, (char) 245, (char) 9001, (char) 225,
			(char) 9002, (char) 241, (char) 91, (char) 91, (char) 913, (char) 65, (char) 914, (char) 66, (char) 915,
			(char) 71, (char) 917, (char) 69, (char) 918, (char) 90, (char) 919, (char) 72, (char) 920, (char) 81,
			(char) 921, (char) 73, (char) 922, (char) 75, (char) 923, (char) 76, (char) 924, (char) 77, (char) 925,
			(char) 78, (char) 926, (char) 88, (char) 927, (char) 79, (char) 928, (char) 80, (char) 929, (char) 82,
			(char) 93, (char) 93, (char) 931, (char) 83, (char) 932, (char) 84, (char) 933, (char) 85, (char) 934,
			(char) 70, (char) 935, (char) 67, (char) 936, (char) 89, (char) 945, (char) 97, (char) 946, (char) 98,
			(char) 947, (char) 103, (char) 948, (char) 100, (char) 949, (char) 101, (char) 95, (char) 95, (char) 950,
			(char) 122, (char) 951, (char) 104, (char) 952, (char) 113, (char) 953, (char) 105, (char) 954, (char) 107,
			(char) 955, (char) 108, (char) 957, (char) 110, (char) 958, (char) 120, (char) 959, (char) 111, (char) 960,
			(char) 112, (char) 961, (char) 114, (char) 962, (char) 86, (char) 963, (char) 115, (char) 964, (char) 116,
			(char) 965, (char) 117, (char) 966, (char) 102, (char) 967, (char) 99, (char) 9674, (char) 224, (char) 968,
			(char) 121, (char) 969, (char) 119, (char) 977, (char) 74, (char) 978, (char) 161, (char) 981, (char) 106,
			(char) 982, (char) 118, (char) 9824, (char) 170, (char) 9827, (char) 167, (char) 9829, (char) 169,
			(char) 9830, (char) 168, };

	@Override
	void addCharMetrics() {
		for (int i = 0; i < validGlyphsAndWidths.length; i += 2) {
			glyphWidths[validGlyphsAndWidths[i]] = validGlyphsAndWidths[i + 1] / 1000.0;
		}
		for (int i = 0; i < standardEncodingCodes.length; i += 2) {
			standardEncodingCodeFromUnicodeCharacter[standardEncodingCodes[i]] = (byte) standardEncodingCodes[i + 1];
		}
	}
}
