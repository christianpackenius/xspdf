package com.packenius.library.xspdf;

/**
 * New (20150710) class for all fonts.
 * @author Christian Packenius, 2015.
 */
public interface XSFontType {
    /**
     * Returns the readable name of the font.
     * @return Readable font name.
     */
    String getFontName();

    /**
     * Returns the logical name of the font (within the PDF file).
     * @return Logical font name.
     */
    String getLogicalFontName();

    /**
     * Returns a double[65536] array with glyph widhts.
     * @return double[] array.
     */
    double[] getGlyphWidths();

    /**
     * Returns an byte[65536] array with standard encodings.
     * @return byte[] array.
     */
    byte[] getStandardEncodingCodeFromUnicodeCharacter();

    /**
     * Returns the alternative font encoding for the given character.
     * @param ch Searched character
     * @return Font encoding containing the given character.
     */
    XSAlternativeFontEncoding getAlternativeFontEncoding(char ch);

    /**
     * Returns the bold / italic / ordinary font variation of this font.
     * @param fontParm Parms: 0=ordinary, 1=bold, 2=italic, 3=bold-italic.
     * @return Font variation.
     */
    XSFontType getFontWithParms(int fontParm);

    /**
     * Returns the ascender of this font.
     * @return Ascender value (usually 1.0).
     */
    double getAscender();

    /**
     * Returns the descender of this font.
     * @return Descender value (usually 0.0).
     */
    double getDescender();
}
