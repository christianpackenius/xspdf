package com.packenius.library.xspdf.hacks;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSDimension;
import com.packenius.library.xspdf.XSPDF;

/**
 * Non-rectangular text column.
 */
public class Example_Hack001 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		final String doNotFillColumnName = "do not fill!";
		xsPDF.use(NO_MARGIN, NO_INDENTATION);
		XSDimension pagesize = xsPDF.getPageSize();
		double width = pagesize.width;
		double height = pagesize.height;
		int fontSize = (int) xsPDF.getFontSize();
		for (int i = 0; i < width - fontSize; i++) {
			xsPDF.addColumn(i + fontSize, i, width - i, 1, doNotFillColumnName, 0);
			xsPDF.addColumn(0, height - 1 - i, width - i - fontSize, 1, doNotFillColumnName, 0);
		}
		xsPDF.addColumns(1, null);
		xsPDF.addContentListener(new XSContentListenerAdapter() {
			@Override
			public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
				if (doNotFillColumnName.equals(columnName)) {
					xsPDF.nextColumn();
				}
			}
		});
		for (int i = 0; i < 453; i++) {
			xsPDF.print("" + i);
		}
		xsPDF.createPdf("pdf/Hack 001.pdf");
	}
}
