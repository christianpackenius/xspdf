package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSDimension;
import com.packenius.library.xspdf.XSPDF;

/**
 * Creating columns.
 */
public class Example017 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.rotatePage();
		xsPDF.setFontSize(10.5);
		xsPDF.setColumnBackgroundColor(new Color(128, 255, 192));
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.addColumns(2, "two columns");
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.addColumns(3, "three columns");
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.addColumns(2, 2, "2x2 columns");
		xsPDF.print(LOREM_IPSUM);
		xsPDF.addContentListener(new XSContentListenerAdapter() {
			@Override
			public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
				if ("second column".equals(columnName)) {
					xsPDF.setColumnBackgroundColor(Color.LIGHT_GRAY);
				}
			}
		});
		xsPDF.newPage();
		XSDimension pageSize = xsPDF.getPageSize();
		double width = pageSize.width;
		double height = pageSize.height;
		double k = Math.min(width, height);
		double m = k * 0.7;
		double margin = k / 20.0;
		xsPDF.addColumn(margin, margin, m, m, "first column", margin);
		xsPDF.addColumn(width - m - margin, height - m - margin, m, m, "second column", margin);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.createPdf("pdf/Example 017.pdf");
	}
}
