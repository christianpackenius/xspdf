package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Playing with fonts.
 */
public class Example012 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setFontSize(24.0).setFontParameters(BOLD).print("This");
		xsPDF.setFont(HELVETICA, 10).print("is");
		xsPDF.setFont(COURIER, 14).print("an");
		xsPDF.setFont(TIMES, 12, ITALIC).print("apple:");
		xsPDF.setFont(SYMBOL).print("\uf8ff\r");
		xsPDF.setFont(HELVETICA, 40).print("Another big one:\r");
		xsPDF.setFont(SYMBOL, 80).print("\uf8ff\r");
		xsPDF.setFont(HELVETICA, 12).print("Some ZapfDingbats icons:");
		xsPDF.setFont(ZAPFDINGBATS, 48).print("\u260e\u2708\u270c\n");
		xsPDF.setTextFillColor(Color.GREEN).print("\u2714");
		xsPDF.setTextFillColor(Color.RED).print("\u2716");
		xsPDF.setTextFillColor(Color.MAGENTA).print("\u2735");
		xsPDF.setTextFillColor(Color.CYAN).print("\u2756");
		xsPDF.setTextFillColor(Color.ORANGE).print("\u27a0");
		xsPDF.createPdf("pdf/Example 012.pdf");
	}
}
