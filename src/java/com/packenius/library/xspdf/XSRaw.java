/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.ArrayList;
import java.util.List;

/**
 * A "raw" object holds a set of raw PDF operators that can be included into PDF content streams.
 * @author Christian Packenius, 2013.
 */
public class XSRaw {
    List<XSRawContent> content = new ArrayList<XSRawContent>();

    // ***************************************
    // ***** Path construction operators *****
    // ***************************************

    /**
     * Set the start point for a new sub path. The "current point" will be set to the given coordinates.
     * @param x X coordinate of the new current point.
     * @param y Y coordinate of the new current point.
     * @return This XSRaw object.
     */
    public XSRaw moveTo(double x, double y) {
        content.add(new XSRawMoveTo(x, y));
        return this;
    }

    /**
     * Append a straight line from the current point to the given coordinates.
     * @param x X coordinate of the destination point.
     * @param y Y coordinate of the destination point.
     * @return This XSRaw object.
     */
    public XSRaw lineTo(double x, double y) {
        content.add(new XSRawLineTo(x, y));
        return this;
    }

    /**
     * Append a cubic B�zier curve from the current point to (x3/y3) using the control points (x1/y1) and (x2/y2).
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     * @return This XSRaw object.
     */
    public XSRaw curveTo(double x1, double y1, double x2, double y2, double x3, double y3) {
        content.add(new XSRawCurveTo(x1, y1, x2, y2, x3, y3));
        return this;
    }

    /**
     * Append a cubic B�zier curve from the current point to (x3/y3) using the current point and (x2/y2) as control points.
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     * @return This XSRaw object.
     */
    public XSRaw curveToNoX1Y1(double x2, double y2, double x3, double y3) {
        content.add(new XSRawCurveToNoX1Y1(x2, y2, x3, y3));
        return this;
    }

    /**
     * Append a cubic B�zier curve from the current point to (x3/y3) using the current point and (x1/y1) as control points.
     * @param x1
     * @param y1
     * @param x3
     * @param y3
     * @return This XSRaw object.
     */
    public XSRaw curveToNoX2Y2(double x1, double y1, double x3, double y3) {
        content.add(new XSRawCurveToNoX2Y2(x1, y1, x3, y3));
        return this;
    }

    /**
     * Close path by appending a straight line from current point to the start point of the sub path.
     * @return This XSRaw object.
     */
    public XSRaw close() {
        content.add(new XSRawStaticOrder(" h"));
        return this;
    }

    /**
     * Append a complete rectangle subpath to the current path.
     * @param x Upper-left corner of the rectangle.
     * @param y Upper-left corner of the rectangle.
     * @param width Width of the rectangle.
     * @param height Height of the rectangle.
     * @return This XSRaw object.
     */
    public XSRaw rectangle(double x, double y, double width, double height) {
        content.add(new XSRawRectangle(x, y, width, height));
        return this;
    }

    // ***********************************
    // ***** Path-painting operators *****
    // ***********************************

    /**
     * Stroke path.
     * @return This XSRaw object.
     */
    public XSRaw strokePath() {
        content.add(new XSRawStaticOrder(" S"));
        return this;
    }

    /**
     * Close and stroke path.
     * @return This XSRaw object.
     */
    public XSRaw closeAndStrokePath() {
        content.add(new XSRawStaticOrder(" s"));
        return this;
    }

    /**
     * Fill path using nonzero winding number rule.
     * @return This XSRaw object.
     */
    public XSRaw fillPathUsingNonzeroWindingNumberRule() {
        content.add(new XSRawStaticOrder(" f"));
        return this;
    }

    /**
     * Fill path using even-odd rule.
     * @return This XSRaw object.
     */
    public XSRaw fillPathUsingEvenOddRule() {
        content.add(new XSRawStaticOrder(" f*"));
        return this;
    }

    /**
     * Fill and stroke path using nonzero winding number rule.
     * @return This XSRaw object.
     */
    public XSRaw fillAndStrokePathUsingNonzeroWindingNumberRule() {
        content.add(new XSRawStaticOrder(" B"));
        return this;
    }

    /**
     * Fill and stroke path using even-odd rule.
     * @return This XSRaw object.
     */
    public XSRaw fillAndStrokePathUsingEvenOddRule() {
        content.add(new XSRawStaticOrder(" B*"));
        return this;
    }

    /**
     * Close, fill and stroke path using nonzero winding number rule.
     * @return This XSRaw object.
     */
    public XSRaw closeFillAndStrokePathUsingNonzeroWindingNumberRule() {
        content.add(new XSRawStaticOrder(" b"));
        return this;
    }

    /**
     * Close, fill and stroke path using even-odd rule.
     * @return This XSRaw object.
     */
    public XSRaw closeFillAndStrokePathUsingEvenOddRule() {
        content.add(new XSRawStaticOrder(" b*"));
        return this;
    }

    /**
     * Ends path without having any visual effekt.
     * @return This XSRaw object.
     */
    public XSRaw closePathWithoutDrawing() {
        content.add(new XSRawStaticOrder(" n"));
        return this;
    }

    // ***********************************
    // ***** Clipping path operators *****
    // ***********************************

    /**
     * "Modify the current clipping path by intersecting it with the current path, using the nonzero winding number rule to
     * determine which regions lie inside the clipping path." (V1.7 PDF specification.)
     * @return This XSRaw object.
     */
    public XSRaw modifyClippingPathUsingNonzeroWindingNumberRule() {
        content.add(new XSRawStaticOrder(" W"));
        return this;
    }

    /**
     * "Modify the current clipping path by intersecting it with the current path, using the even-odd rule to determine which regions lie inside the clipping path."
     * (V1.7 PDF specification.)
     * @return This XSRaw object.
     */
    public XSRaw modifyClippingPathUsingEvenOddRule() {
        content.add(new XSRawStaticOrder(" W*"));
        return this;
    }

    // ***************************
    // ***** Color operators *****
    // ***************************

    /**
     * Set color space to use with stroking operators.
     * @param name Name of the color space to use, for example "/DeviceRGB".
     * @return This XSRaw object.
     */
    public XSRaw setStrokingColorSpace(String name) {
        if (!name.startsWith("/")) {
            name = "/" + name;
        }
        content.add(new XSRawStaticOrder(" " + name + " CS"));
        return this;
    }

    /**
     * Set color space to use with non-stroking operators.
     * @param name Name of the color space to use, for example "/DeviceRGB".
     * @return This XSRaw object.
     */
    public XSRaw setNonStrokingColorSpace(String name) {
        if (!name.startsWith("/")) {
            name = "/" + name;
        }
        content.add(new XSRawStaticOrder(" " + name + " cs"));
        return this;
    }

    /**
     * Set color to use with stroking operators.
     * @param colors Color values.
     * @return This XSRaw object.
     */
    public XSRaw setStrokingColor(double... colors) {
        String s = "";
        for (double color : colors) {
            s += " " + color;
        }
        content.add(new XSRawStaticOrder(s + " SC"));
        return this;
    }

    /**
     * Set color to use with non-stroking operators.
     * @param colors Color values.
     * @return This XSRaw object.
     */
    public XSRaw setNonStrokingColor(double... colors) {
        String s = "";
        for (double color : colors) {
            s += " " + color;
        }
        content.add(new XSRawStaticOrder(s + " sc"));
        return this;
    }

    /**
     * Set the stroking color to the given gray value.
     * @param grayValue Gray value.
     * @return This XSRaw object.
     */
    public XSRaw setStrokingColorGray(double grayValue) {
        content.add(new XSRawStaticOrder(" " + grayValue + " G"));
        return this;
    }

    /**
     * Set the non-stroking color to the given gray value.
     * @param grayValue Gray value.
     * @return This XSRaw object.
     */
    public XSRaw setNonStrokingColorGray(double grayValue) {
        content.add(new XSRawStaticOrder(" " + grayValue + " g"));
        return this;
    }

    /**
     * Set the stroking color to the given RGB values.
     * @param red Red value.
     * @param green Green value.
     * @param blue Blue value.
     * @return This XSRaw object.
     */
    public XSRaw setStrokingColorRGB(double red, double green, double blue) {
        content.add(new XSRawStaticOrder(" " + red + " " + green + " " + blue + " RG"));
        return this;
    }

    /**
     * Set the non-stroking color to the given RGB values.
     * @param red Red value.
     * @param green Green value.
     * @param blue Blue value.
     * @return This XSRaw object.
     */
    public XSRaw setNonStrokingColorRGB(double red, double green, double blue) {
        content.add(new XSRawStaticOrder(" " + red + " " + green + " " + blue + " rg"));
        return this;
    }

    /**
     * Set the stroking color to the given CMYK values.
     * @param cyan Cyan value.
     * @param magenta Magenta value.
     * @param yellow Yellow value.
     * @param black Black value.
     * @return This XSRaw object.
     */
    public XSRaw setStrokingColorCMYK(double cyan, double magenta, double yellow, double black) {
        content.add(new XSRawStaticOrder(" " + cyan + " " + magenta + " " + yellow + " " + black + " K"));
        return this;
    }

    /**
     * Set the non-stroking color to the given CMYK values.
     * @param cyan Cyan value.
     * @param magenta Magenta value.
     * @param yellow Yellow value.
     * @param black Black value.
     * @return This XSRaw object.
     */
    public XSRaw setNonStrokingColorCMYK(double cyan, double magenta, double yellow, double black) {
        content.add(new XSRawStaticOrder(" " + cyan + " " + magenta + " " + yellow + " " + black + " k"));
        return this;
    }

    // ************************************
    // ***** Graphics state operators *****
    // ************************************

    /**
     * Push the current graphics state onto the graphics state stack.
     * @return This XSRaw object.
     */
    public XSRaw saveGraphicsState() {
        content.add(new XSRawStaticOrder(" q"));
        return this;
    }

    /**
     * Pop the current graphics state from the graphics state stack.
     * @return This XSRaw object.
     */
    public XSRaw restoreGraphicsState() {
        content.add(new XSRawStaticOrder(" Q"));
        return this;
    }

    /**
     * "Modify the current transformation matrix (CTM) by concatenating the specified matrix."
     * @param a
     * @param b
     * @param c
     * @param d
     * @param e
     * @param f
     * @return This XSRaw object.
     */
    public XSRaw modifyCurrentTransformationMatrix(double a, double b, double c, double d, double e, double f) {
        content.add(new XSRawStaticOrder(" " + a + " " + b + " " + c + " " + d + " " + e + " " + f + " cm"));
        return this;
    }

    /**
     * Set the line width.
     * @param lineWidth New line width.
     * @return This XSRaw object.
     */
    public XSRaw setLineWidth(double lineWidth) {
        content.add(new XSRawStaticOrder(" " + lineWidth + " w"));
        return this;
    }

    /**
     * Set the line cap.
     * @param lineCap New line cap (0, 1 or 2).
     * @return This XSRaw object.
     */
    public XSRaw setLineCap(int lineCap) {
        content.add(new XSRawStaticOrder(" " + lineCap + " J"));
        return this;
    }

    /**
     * Set the line cap.
     * @param lineJoin New line cap (0, 1 or 2).
     * @return This XSRaw object.
     */
    public XSRaw setLineJoin(int lineJoin) {
        content.add(new XSRawStaticOrder(" " + lineJoin + " j"));
        return this;
    }

    /**
     * Set the miter limit.
     * @param miterLimit New miter limit.
     * @return This XSRaw object.
     */
    public XSRaw setMiterLimit(int miterLimit) {
        content.add(new XSRawStaticOrder(" " + miterLimit + " M"));
        return this;
    }

    /**
     * Set the line dash pattern.
     * @param dashPhase First dash phase (usually 0).
     * @param dashArray Dash array, starting with black.
     * @return This XSRaw object.
     */
    public XSRaw setLineDashPattern(int dashPhase, int... dashArray) {
        String s = " [";
        for (int i : dashArray) {
            s += " " + i;
        }
        content.add(new XSRawStaticOrder(s + " ] " + dashPhase + " d"));
        return this;
    }

    String getContent(double x, double y, double xFactor, double yFactor) {
        StringBuilder sb = new StringBuilder();
        for (XSRawContent raw : content) {
            sb.append(raw.getRawContent(x, y, xFactor, yFactor));
        }
        return sb.toString();
    }
}
