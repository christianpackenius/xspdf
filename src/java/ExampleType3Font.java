import java.io.IOException;

import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSType3Font;
import com.packenius.library.xspdf.XSType3Glyph;

/**
 * Non-encoded "Hello world" for testing output files.
 * 
 * @author Christian Packenius, 2015.
 */
public class ExampleType3Font implements XS {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSType3Font t3font = new XSType3Font("MyType3Font");

		XSType3Glyph t3glyph = new XSType3Glyph(300, 0, 300, 1000, 0);
		t3font.add(' ', t3glyph);

		t3glyph = new XSType3Glyph(1000, 0, 1000, 1000, 0);
		t3glyph.m_moveTo(0, 0);
		t3glyph.l_lineTo(400, 1000);
		t3glyph.l_lineTo(800, 0);
		t3glyph.l_lineTo(400, 400);
		t3glyph.h_fillSubPath();
		t3font.add('A', t3glyph);

		t3glyph = new XSType3Glyph(700, 0, 500, 1000, 0);
		t3glyph.m_moveTo(0, 0);
		t3glyph.l_lineTo(0, 1000);
		t3glyph.l_lineTo(500, 750);
		t3glyph.l_lineTo(300, 500);
		t3glyph.l_lineTo(500, 250);
		t3glyph.h_fillSubPath();
		t3font.add('B', t3glyph);

		t3glyph = new XSType3Glyph(700, 0, 500, 1000, 0);
		t3glyph.m_moveTo(500, 1000);
		t3glyph.l_lineTo(350, 500);
		t3glyph.l_lineTo(500, 0);
		t3glyph.l_lineTo(0, 500);
		t3glyph.h_fillSubPath();
		t3font.add('C', t3glyph);

		t3glyph = new XSType3Glyph(700, 0, 500, 1000, 0);
		t3glyph.m_moveTo(0, 0);
		t3glyph.l_lineTo(0, 1000);
		t3glyph.l_lineTo(500, 500);
		t3glyph.h_fillSubPath();
		t3font.add('D', t3glyph);

		XSPDF xspdf = new XSPDF();
		xspdf.setTextParagraphIndentationSpaces(0);
		xspdf.setFont(HELVETICA).print("Eine Type3-Schrift: ");
		xspdf.setContentEncoding(NO_ENCODING);
		xspdf.setFont(t3font);
		xspdf.print("A B C D ABCD");
		xspdf.setFont(HELVETICA).print("<- und wieder normal!");
		xspdf.createPdf("hello-world-2.pdf");
	}
}
