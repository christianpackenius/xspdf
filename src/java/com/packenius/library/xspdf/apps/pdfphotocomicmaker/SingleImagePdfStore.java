/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.swing.JOptionPane;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * @author Christian Packenius, 2013.
 */
public class SingleImagePdfStore implements XS, ActionListener {
    private final ImagePanel imagePanel;

    private final BufferedImage image;

    private final File imageFile;

    private final List<SpeechBalloonMarker> markers;

    /**
     * Constructor.
     * @param imagePanel
     */
    public SingleImagePdfStore(ImagePanel imagePanel) {
        this.imagePanel = imagePanel;
        image = imagePanel.image;
        imageFile = imagePanel.imageFile;
        markers = imagePanel.markers;
    }

    /**
     * Stores the image including the markers in a PDF file.
     * @throws IOException
     */
    public void storeAsPdf() throws IOException {
        XSPDF xsPDF = new XSPDF();
        xsPDF.setPageMode(FULL_SCREEN).setContentEncoding(NO_ENCODING);
        int width = image.getWidth();
        int height = image.getHeight();
        // System.out.println(width + ", " + height);
        xsPDF.setUnitType(USER_UNITS);
        xsPDF.setPageSize(width, height);
        xsPDF.addColumn(0, 0, width, height, "Image column", 0);
        xsPDF.nextColumn();
        xsPDF.setImage(image, 0, 0, width, height, 0);

        xsPDF.setLayerTransparency(1, 0.7);
        int layerID = 2;
        for (SpeechBalloonMarker marker : markers) {
            xsPDF.addRawObjectToLayer(marker.getRawObject(), 1);
            xsPDF.setLayerID(layerID); // For text.
            marker.addTextToPDF(xsPDF, layerID);
            layerID++;
        }

        // xsPDF.createPdf(imageFile.getName() + ".pdf");
        File pdfFile = new File(imageFile.getCanonicalPath() + ".pdf");
        xsPDF.createPdf(pdfFile);
        Desktop.getDesktop().open(pdfFile);
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
        try {
            storeAsPdf();
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(imagePanel, exception.getMessage());
        }
    }
}
