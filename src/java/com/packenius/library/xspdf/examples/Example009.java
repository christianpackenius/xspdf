package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSUnicodeMapping;

/**
 * Card symbols and text fill color.
 */
public class Example009 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setFont(SYMBOL, 72.0);
		xsPDF.print("\u2660"); // spade
		xsPDF.print(XSUnicodeMapping.getCodeFromName("club"));
		xsPDF.setTextFillColor(Color.RED);
		xsPDF.print("\n\u2665"); // heart
		xsPDF.print("\u2666"); // diamond
		xsPDF.createPdf("pdf/Example 009.pdf");
	}
}
