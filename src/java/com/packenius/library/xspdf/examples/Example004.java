package com.packenius.library.xspdf.examples;

import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - shortest way using a static method
 */
public class Example004 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF.create(new File("pdf/Example 004.pdf"), "Hello world");
	}
}
