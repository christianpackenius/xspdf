/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * A "self-made" character for a Type3 font.
 * @author Christian Packenius, 2015.
 */
public class XSType3Glyph {
    final int width;

    final int left;

    final int right;

    final int top;

    final int bottom;

    private final StringBuilder content = new StringBuilder();

    /**
     * Constructor for a new Type3 glyph. The glyphs boundig box is left 0, right 'width + 200', bottom 0 and top 1000.
     * @param width Width of the glyph, including some space between a character of this glyph and the following character.
     */
    public XSType3Glyph(int width) {
        this(width, 0, width + 200, 1000, 0);
    }

    /**
     * Constructor for a new Type3 glyph.
     * @param width Width of the glyph, including some space between a character of this glyph and the following character.
     * @param left Bounding box value of this glyph.
     * @param right Bounding box value of this glyph.
     * @param top Bounding box value of this glyph.
     * @param bottom Bounding box value of this glyph.
     */
    public XSType3Glyph(int width, int left, int right, int top, int bottom) {
        this.width = width;
        this.left = left;
        this.right = right;
        this.top = top;
        this.bottom = bottom;
        content.append(width);
        content.append(" 0 ");
        content.append(left);
        content.append(' ');
        content.append(bottom);
        content.append(' ');
        content.append(right);
        content.append(' ');
        content.append(top);
        content.append(" d1\n");
    }

    /**
     * Starts a new sub path with the given start coordinates.
     * @param x
     * @param y
     */
    public void m_moveTo(int x, int y) {
        content.append(x);
        content.append(' ');
        content.append(y);
        content.append(" m\n");
    }

    /**
     * Line to new coordinates.
     * @param x
     * @param y
     */
    public void l_lineTo(int x, int y) {
        content.append(x);
        content.append(' ');
        content.append(y);
        content.append(" l\n");
    }

    /**
     * Adds a new cubic Bezier curve to the sub path.
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @param x3
     * @param y3
     */
    public void c_cubicBezierCurveTo(int x1, int y1, int x2, int y2, int x3, int y3) {
        content.append(x1);
        content.append(' ');
        content.append(y1);
        content.append(' ');
        content.append(x2);
        content.append(' ');
        content.append(y2);
        content.append(' ');
        content.append(x3);
        content.append(' ');
        content.append(y3);
        content.append(' ');
        content.append(" c\n");
    }

    /**
     * Closes the current sub path and fill it.
     */
    public void h_fillSubPath() {
        content.append("f\n");
    }

    /**
     * Deliveres the content of the glyph (the glyph description).
     * @return Glyph description as postscript order string.
     */
    public String getContent() {
        return content.toString();
    }
}
