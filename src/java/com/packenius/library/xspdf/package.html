<body>
<h2><i>xsPDF</i> - A Java PDF creation library</h2>

<p>"<i>xsPDF</i>" stands for "eXtreme Simple PDF". It lets you create PDF documents programmatically in an easy to use way.</p>

<p><i>xsPDF</i> has the following characteristics:</p>
<ul>
<li>Creation of PDF documents with focus on textual content.</li>
<li>Creation of quite small files with minimal overhead.</li>
<li>Support of different coding styles.</li>
<li>Very small and easy to use API.</li>
<li>Listeners for document control.</li>
<li>Minimal text processing:
  <ul>
    <li>Word wrapping.</li>
    <li>Auto formatting.</li>
    <li>Usage of 14 PDF standard fonts.</li>
    <li>Embedding images.</li>
    <li>Multiple columns on page.</li>
    <li>Alignment</li>
  </ul></li>
</ul>

<p><b>Note:</b> All examples below (Java source files and PDF results) can be found in <i>xsPDF-Examples.zip</i>.</p>

<p><i>xsPDF</i> lets you code in the way you are used to code; here is a typical "Hello world" example in different programming styles:</p>

<h3><b>Example 001</b> - "Hello world" - standard coding</h3>
<pre><code>package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - standard coding
 */
public class Example001 implements XS {
  /**
   * @param args Unused.
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    XSPDF xsPDF = new XSPDF();
    xsPDF.print("Hello world");
    xsPDF.createPdf("pdf/Example 001.pdf");
  }
}
</code></pre>

<h3><b>Example 002</b> - "Hello world" - using fluit API</h3>
<pre><code>package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - using fluit API
 */
public class Example002 implements XS {
  /**
   * @param args Unused.
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    XSPDF.getInstance()
      .print("Hello world")
      .createPdf("pdf/Example 002.pdf");
  }
}
</code></pre>

<h3><b>Example 003</b> - "Hello world" - even shorter using <i>use()</i> method</h3>
<pre><code>package com.packenius.library.xspdf.examples;

import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - even shorter using <i>use()</i> method
 */
public class Example003 implements XS {
  /**
   * @param args Unused.
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    XSPDF.getInstance().use("Hello world", new File("pdf/Example 003.pdf"));
  }
}
</code></pre>

<h3><b>Example 004</b> - "Hello world" - shortest way using a static method</h3>
<pre><code>package com.packenius.library.xspdf.examples;

import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - shortest way using a static method
 */
public class Example004 implements XS {
  /**
   * @param args Unused.
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {
    XSPDF.create(new File("pdf/Example 004.pdf"), "Hello world");
  }
}
</code></pre>

<p>All of these examples generate PDF files with exactly the same content in the sub directory <i>pdf</i>.</p>

<p>Most functions of <i>xsPDF</i> can be used via all of these ways. In the following examples I will only use code fragments, not full classes to avoid reading overhead. And I will only use the standard coding because it is better to read for most people.</p>

<p>All of the examples above implement the interface <i>XS</i> which contains many static constant values for diverse purposes. You should always implement this interface in the relevant classes to make coding easier.</p>

<p>One of the static values in <i>XS</i> is the string constant <i>LOREM_IPSUM</i> which we will use for many of the following examples.</p>

<h3><b>Example 005</b> - Lorem ipsum...</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 005.pdf");
</code></pre>

<p>Take a look at <i>Example 005.pdf</i>. You can read the Lorem ipsum text but there are some more things to take a note of:</p>
<ul>
  <li>The text is written on two pages. Each of the page has the size 210mm x 297mm (this is DIN A4).</li>
  <li>There is a margin around the text on every page.</li>
  <li>The text ist justified (left and right aligned).</li>
  <li>The first line of every paragraph (text block) is indented.</li>
  <li>The last line of every paragraph is left aligned only.</li>
  <li>Automatic word wrapping from line to line and from page to page.</li>
  <li>Usage of font family <i>Times</i> with font size <i>12.</i></li>
</ul>

<p>All of these listed characteristics can be manipulated as shown in the following examples.</p>

<p>Have a look at the page size. You can use one of the predefined page formats in <i>XS</i> like <i>DIN_A4</i> (this is the default page size), <i>DIN_C9</i> (a very small page), <i>DIN_B0</i> (this one is quite large) or the North American formats like <i>INVOICE</i> or <i>LETTER</i>. Of course you can use all of these page formats in one single document. And you can define own page formats if you want to.</p>

<h3><b>Example 006</b> - Different page formats in a single document.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setPageSize(LETTER);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.setPageSize(DIN_B0);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.setUnitType(INCH);
    xsPDF.setPageSize(10, 10);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.setPageSize(DIN_C9);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 006.pdf");
</code></pre>

<p>As you can see, the page format can be changed and will be inheritted to the following pages. Because there is no way to change the page format after adding content to a page, it is neccessary to call <i>newPage()</i> before changing the page format via <i>setPageSize()</i>. The first two pages of <i>Example 006.pdf</i> have the <i>LETTER</i> format. The third page has format <i>DIN B0</i>, so the text only appears on the top of the page. Pages 4 and 5 have a self defined quadratic page format with the size 10inch x 10inch (the unit type <i>inch</i> has been set by <i>setUnitType()</i> before). And finally pages 6 to 39 contain Lorem ipsum on very small DIN C9 pages.</p>

<p>Maybe your PDF viewer is configured to show the single pages in "fit to screen"  mode. Then the text on page 3 will appear quite small and the text on the last pages very big. But register that the printed text has the same size on every page.</p>

<p>The second characteristic is the empty border around the text, the <i>page margin</i>. The standard margin are 20mm on every side. Every time you call <i>setPageSize()</i> the page margin is set to a tenth of the particular size. Here an example: If you create a page with size 40inch x 30inch, the upper and lower margin will be set to 4inch, the left and right to 3inch. But you can overwrite these values with <i>setPageMargin()</i> as shown in <i>Example 007</i>:</p>

<h3><b>Example 007</b> - Pages with different margins.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setPageMargin(NO_MARGIN);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.setUnitType(INCH);
    xsPDF.setPageMargin(new XSPageMargin(0.1));
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 007.pdf");
</code></pre>

<p>In <i>Example 008</i> you can see how to use different alignments for text.</p>

<h3><b>Example 008</b> - Text justifying / aligning.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setPageSize(100, 650);
    xsPDF.setPageMargin(20);
    xsPDF.print("This useless text is left and right aligned.");
    xsPDF.print("The first line is indented and the last line is left aligned only.\n\n");
    xsPDF.setTextParagraphIndentationSpaces(0);
    xsPDF.setLastJustifiedLineRightAligned(true);
    xsPDF.print("This justified text has no intendation in the first line.");
    xsPDF.print("And it is left and right aligned in the last line.\r\r");
    xsPDF.setAlignment(CENTERED);
    xsPDF.print("A centered text example.\r\r");
    xsPDF.setAlignment(LEFT_ALIGNED);
    xsPDF.print("And a left aligned...\r\r");
    xsPDF.setAlignment(RIGHT_ALIGNED);
    xsPDF.print("...and finally a right aligned.");
    xsPDF.createPdf("pdf/Example 008.pdf");
</code></pre>

<p>As I introduced above, PDF has 14 standard fonts for easy usage. All of these are implemented with <i>xsPDF</i>. But there are only few font families to use: <i>Times</i>, <i>Helvetica</i> and <i>Courier</i> with the characteristics <i>ordinary</i>, <i>italic</i>, <i>bold</i> and <i>bold/italic</i>, which are 12 of these 14 fonts. The remaining two fonts have the name <i>Symbol</i> and <i>ZapfDingbats</i> and contain different icons and signs. You can use the different characters of the fonts via their Unicode character.</p>

<p><i>Example 009</i> shows an example of some Non-ASCII characters. The class <i>XSUnicodeMapping</i> assists you in finding the right character for the right symbol. The example also shows how to change the font color.</p>

<h3><b>Example 009</b> - Card symbols and text fill color.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setFont(SYMBOL, 72.0);
    xsPDF.print("\u2660"); // spade
    xsPDF.print(XSUnicodeMapping.getCodeFromName("club"));
    xsPDF.setTextFillColor(Color.RED);
    xsPDF.print("\n\u2665"); // heart
    xsPDF.print("\u2666"); // diamond
    xsPDF.createPdf("pdf/Example 009.pdf");
</code></pre>

<p>Okay, think about this: The method for setting the text color has the name <i>setTextFillColor</i> instead the easier <i>setFontColor</i>. The reason is, that there are two colors which are responsible for inking. Without many words: Have a look at <i>Example 010</i> to understand.</p>

<h3><b>Example 010</b> - Text render modes.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setFontFamily(HELVETICA);
    xsPDF.setFontSize(40);
    xsPDF.setTextFillColor(Color.RED);
    xsPDF.setTextStrokeColor(Color.GREEN);
    xsPDF.print("FILL (default)\r\r");
    xsPDF.setTextRenderMode(TEXT_STROKE);
    xsPDF.print("STROKE\r\r");
    xsPDF.setTextRenderMode(TEXT_FILL_AND_STROKE);
    xsPDF.print("FILL and STROKE\r\r");
    xsPDF.setTextRenderMode(TEXT_INVISIBLE);
    xsPDF.print("INVISIBLE");
    xsPDF.setTextRenderMode(TEXT_FILL);
    xsPDF.print(" (invisible)");
    xsPDF.createPdf("pdf/Example 010.pdf");
</code></pre>

<p>Let's have a small excursion to another issue. Usually, xsPDF automatically formats your code: Spaces are deleted at the beginning and the end of printed text strings, double spaces are changed to single spaces. And if you calls the <i>print()</i> method twice, there will be inserted a space between these strings. But you can omit all of these formattings and format the text for your own, if you want. Have a look to the next example for it.</p>

<h3><b>Example 011</b> - Formatting modes.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setAlignment(LEFT_ALIGNED);
    xsPDF.setTextParagraphIndentationSpaces(0);
    xsPDF.setFont(COURIER, BOLD, ITALIC).print("Auto format:\r\n");
    xsPDF.setFontFamily(HELVETICA);
    xsPDF.print("    This  are        four words.      \r\n");
    xsPDF.print("              This");
    xsPDF.print("are");
    xsPDF.print("four                ");
    xsPDF.print("words.\r\n\r\n");
    xsPDF.setFont(COURIER, BOLD, ITALIC).print("No formatting:\r\n");
    xsPDF.setFontFamily(HELVETICA);
    xsPDF.setTextFormattingMode(NO_FORMATTING);
    xsPDF.print("    This  are        four words.      \r\n");
    xsPDF.print("              This");
    xsPDF.print("are");
    xsPDF.print("four                ");
    xsPDF.print("words.\r\n\r\n");
    xsPDF.setTextFormattingMode(AUTO_FORMAT);
    xsPDF.setFont(COURIER, BOLD, ITALIC).print("Auto format:\r\n");
    xsPDF.setFontFamily(HELVETICA);
    xsPDF.print("    This  are        four words.      \r\n");
    xsPDF.print("              This");
    xsPDF.print("are");
    xsPDF.print("four                ");
    xsPDF.print("words.\r\n\r\n");
    xsPDF.createPdf("pdf/Example 011.pdf");
</code></pre>

<p>The next example doesn't show anything new or magic. It only gives an example of using different fonts, font sizes and font characteristics.</p>

<h3><b>Example 012</b> - Playing with fonts.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setFontSize(24.0).setFontParameters(BOLD).print("This");
    xsPDF.setFont(HELVETICA, 10).print("is");
    xsPDF.setFont(COURIER, 14).print("an");
    xsPDF.setFont(TIMES, 12, ITALIC).print("apple:");
    xsPDF.setFont(SYMBOL).print("\uf8ff\r");
    xsPDF.setFont(HELVETICA, 40).print("Another big one:\r");
    xsPDF.setFont(SYMBOL, 80).print("\uf8ff\r");
    xsPDF.setFont(HELVETICA, 12).print("Some ZapfDingbats icons:");
    xsPDF.setFont(ZAPFDINGBATS, 48).print("\u260e\u2708\u270c\n");
    xsPDF.setTextFillColor(Color.GREEN).print("\u2714");
    xsPDF.setTextFillColor(Color.RED).print("\u2716");
    xsPDF.setTextFillColor(Color.MAGENTA).print("\u2735");
    xsPDF.setTextFillColor(Color.CYAN).print("\u2756");
    xsPDF.setTextFillColor(Color.ORANGE).print("\u27a0");
    xsPDF.createPdf("pdf/Example 012.pdf");
</code></pre>

<p>Another subject of text formatting is the line leading. This is an expression for the gap between two lines. Every time you set a new font size in <i>xsPDF</i>, the line leading will be set to 50% of the font size. This is a common value for text composition to create packed, readable text. If you set the line leading to zero, the line are printed closely and a negative line leading leads to overlapping lines, as you can see in <i>Example 013</i>.</p>

<h3><b>Example 013</b> - Line leading examples.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.rotatePage();
    xsPDF.setFontSize(24.0);
    xsPDF.print("First line (line leading 50% - default).");
    xsPDF.print("\rSecond line - now set line leading to zero:");
    xsPDF.setLineLeading(0);
    xsPDF.print("\rThird line - after this line the line leading will be 300%:");
    xsPDF.setLineLeadingInPercent(300);
    xsPDF.print("\rForth line. Another font size:");
    xsPDF.setFontSize(18);
    xsPDF.print("\nTwo lines with negative line leading between them:");
    xsPDF.print("\rABCDEFG");
    xsPDF.setLineLeading(-10);
    xsPDF.print("\rHIJKLMN");
    xsPDF.createPdf("pdf/Example 013.pdf");
</code></pre>

<p>For now there should be enough examples for text formatting. Let's have a look to another subject: <i>Content listeners</i>. Sometimes it is interesting to know if there 'happens' anything on the page. In the following example a content listener will be installed to the <i>XSPDF</i>-Object that will be informed every time a new text part will be added to the document.</p>

<p>Why do we call it "text part" and not "word"? Because all characters that are next to each other without any spaces are a text part.</p>

<p>See <i>Example 014</i> to figure out this difference and the content listener working and have a look at the <i>Example 014.pdf</i> file: The first line ends with the green expression "part.". In the second line, we disable auto formatting to prevent <i>xsPDF</i> from adding a space between "part" and ".". So the period at the end of the second line will get its own color.</p>

<h3><b>Example 014</b> - Content listener example.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setTextParagraphIndentationSpaces(0);
    xsPDF.setAlignment(LEFT_ALIGNED);
    xsPDF.addContentListener(new XSContentListenerAdapter() {
      Color[] colors = {Color.RED, Color.BLUE, Color.MAGENTA, Color.GREEN};
      int colorID = 0;

      @Override
      public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
        if (textPart != " ") {
          xsPDF.setTextFillColor(colors[colorID++ % colors.length]);
        }
      }
    });
    xsPDF.print("In this sentence the text color will change with every text part.\r\n\n");
    xsPDF.setTextFormattingMode(NO_FORMATTING);
    xsPDF.print("In this sentence the text color will change with every text part");
    xsPDF.print(".\r\n");
    xsPDF.createPdf("pdf/Example 014.pdf");
</code></pre>

<p>And there are more possibilities for color settings and content listeners, as shown in <i>Example 015</i>: It uses the <i>newPage()</i> method to set a gray color to every page (the first page is nearly black, but the gray value gets lighter with every page). Additional, we change the color of the text in every new text line in the <i>newTextLine()</i> method.</p>

<p>Give it a try and play with the colors - the world needs colors!</p>

<h3><b>Example 015</b> - Colored backgrounds and text lines.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.addContentListener(new XSContentListenerAdapter() {
      @Override
      public void newPage(XSPDF xsPDF, int pageNumber) {
        int grayValue = 24 * pageNumber;
        xsPDF.setPageBackgroundColor(new Color(grayValue, grayValue, grayValue));
      }

      @Override
      public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
        int xValue = lineNumber * 18;
        xsPDF.setTextFillColor(new Color(xValue, 255 - xValue, 255 - xValue));
      }
    });
    xsPDF.setTextFillColor(Color.WHITE);
    xsPDF.setPageSize(DIN_A7);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 015.pdf");
</code></pre>

<p>If you read the last example mindful, you will have remarked the parameter <i>columnName</i> of the <i>newTextLine()</i> method asking yourself for the meaning of this item. This leads us to another theme, the <i>columns</i>.</p>

<p><i>xsPDF</i> is written in a way to be usable for small on-the-fly-PDF-creators as well as for power-users who wants to create a PDF document that looks like a magazine, which have printed the articles in many columns.</p>

<p>There are many ways to create columns in xsPDF. The following small <i>Example 016</i> demonstrates that all of the upper examples are created with single-column pages.</p>

<h3><b>Example 016</b> - Colored columns.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.setColumnBackgroundColor(Color.PINK);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 016.pdf");
</code></pre>

<p>You see - our famous Lorem ipsum is embedded in (now beautiful pink) columns. The next example also colorizes the columns and shows the different ways to create columns.</p>

<h3><b>Example 017</b> - Creating columns.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    xsPDF.rotatePage();
    xsPDF.setFontSize(10.5);
    xsPDF.setColumnBackgroundColor(new Color(128, 255, 192));
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.addColumns(2, "two columns");
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.addColumns(3, "three columns");
    xsPDF.print(LOREM_IPSUM);
    xsPDF.newPage();
    xsPDF.addColumns(2, 2, "2x2 columns");
    xsPDF.print(LOREM_IPSUM);
    xsPDF.addContentListener(new XSContentListenerAdapter() {
      @Override
      public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
        if ("second column".equals(columnName)) {
          xsPDF.setColumnBackgroundColor(Color.LIGHT_GRAY);
        }
      }
    });
    xsPDF.newPage();
    XSDimension pageSize = xsPDF.getPageSize();
    double width = pageSize.width;
    double height = pageSize.height;
    double k = Math.min(width, height);
    double m = k * 0.7;
    double margin = k / 20.0;
    xsPDF.addColumn(margin, margin, m, m, "first column", margin);
    xsPDF.addColumn(width - m - margin, height - m - margin, m, m, "second column", margin);
    xsPDF.print(LOREM_IPSUM);
    xsPDF.createPdf("pdf/Example 017.pdf");
</code></pre>

<p>Five landscape (rotated) DIN A4 pages are created in <i>Example 017</i>: The first page is as you know it - a single column on a single page. The second and third pages have corresponding columns side by side. The fourth page has four columns, arranged in two lines and two columns. And the fifth page?...</p>

<p>On the fifth page are two user defined columns that overlap. And there is a content listener that gives the second column an own color (light gray - the first column is green) to differentiate it from the first one. Beautiful, isn't it?</p>

<p>But let me shortly explain you the rules behind the gaps of the columns:<p>
<ul>
 <li>Every time you set a page size, there is automatically defined a page margin (as you could read in the explanation between <i>Example 006</i> and <i>Example 007</i>).</li>
 <li>When setting a page margin, there are also set gaps between (maybe in the future defined) columns.</li>
 <li>When creating columns by calling a <i>addColumns()</i> method, this gaps are used to define margins around the columns.</li>
 <li>When creating user defined columns by calling <i>addColumn()</i>, you have to add a margin parameter.</li>
 <li>When column backgrounds are painted, <i>xsPDF</i> uses half of the margin value to draw the background around the real column.</li>
 <li>When overlapping columns will be filled, the previous defined columns are filled first and overlay following defined columns. The overlapping areas of the following columns will not be filled with printed text, as you can see in the last example.</li>
</ul>

<p>Puh, okay, that's it. But there is only one way to find out how to work with this rules: "Learning by doing" in combination with "trial and error". ;-)</p>

<p>The next exciting subject is the usage of <i>images</i>. The bad message at first: Currently there is no way to embed images into the text flow with <i>xsPDF</i>. But it is planned for future versions of <i>xsPDF</i>. Images can only bet 'set' into a column before any text is printed into this column. The reason therefor: Text in a column will flow around the images of this (and previously defined) column. To cut a long short story, have a look at the (allowedly a little enlarged)<i>Example 018</i>. I use increasing numbers instead the Lorum ipsum text to make it easier to follow the text flow of this example.</p>

<h3><b>Example 018</b> - Using images.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    final BufferedImage image = ImageIO.read(new File("img/graffiti.jpg"));
    xsPDF.setFontSize(10.0);
    xsPDF.setUnitType(MM);
    xsPDF.setPageSize(300, 200);
    xsPDF.addColumn(50, 40, 50, 48, "inner yellow column", 15);
    xsPDF.addColumn(20, 20, 170, 160, "second pink column", 20);
    xsPDF.addColumn(200, 20, 80, 160, "third green column", 20);
    xsPDF.addContentListener(new XSContentListenerAdapter() {
      @Override
      public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
        if (columnName.equals("inner yellow column")) {
          xsPDF.setColumnBackgroundColor(Color.yellow);
        }
        else if (columnName.equals("second pink column")) {
          xsPDF.setImage(image, 55, 50, 60, 60, 6); // Image-A
          xsPDF.setImage(image, 160, 50, 30, 30, 6); // Image-B
          xsPDF.setColumnBackgroundColor(Color.pink);
        }
        else { // Third green column.
          xsPDF.setImage(image, 2, 2, 20, 20, 4); // Image-C
          xsPDF.setImage(image, -20, 105, 30, 30, 6); // Image-D
          xsPDF.setColumnBackgroundColor(Color.green);
        }
      }
    });
    for (int i = 1; i <= 1000; i++) {
      xsPDF.print("" + i);
    }
    xsPDF.createPdf("pdf/Example 018.pdf");
</code></pre>

<p><i>Example 018</i> is surely not self-explanatory, so I will give some hints:</p>
<ul>
 <li>Every image that is set into the document will be assigned to the 'current' column. The coordinates of the image has to be relative to the origin of this column.</li>
 <li><i>Image-A</i> is the big image on the left side. It is part of the pink column and overlaid with the yellow column which is defined first.</li>
 <li><i>Image-B</i> is the smaller image in the middle which is overlapping the pink and the green column.</li>
 <li><i>Image-C</i> is the small image in the upper left corner of the green column.</li>
 <li><i>Image-D</i> is the small image beneath <i>Image-B</i>  which is overlaid by the pink column because it's defined in the green column.</li>
</ul>

<p><i>A small note that matches at this place:</i> Commonly people draw coordinate systems with the origin bottom/left. But nearly all 'things' in the computer world use them top/left, with the exception of 'things' like the operation system OS/2 or (<i>drum roll</i>) PDF. But during designing <i>xsPDF</i> I decided to develop the API in a way people are used to work. So the origin of everything (pages, columns, images) is top/left.</p>

<p>I want to close this small <i>xsPDF</i> tutorial by giving you an imagination of the power of <i>layers</i>, a very late implemented feature. Layers gives you the possibility to create different planes on a single PDF page without interfering each other. Layers with greater numbers are drawn above layers with lower numbers. <i>Example 019</i> creates a single paged PDF file with three layers:</p>
<ul>
 <li>A background layer with a fullsized image (you know this image from <i>Example 018</i>). This layer has the ID <i>Integer.MIN_VALUE</i>. This avoids all other layers to lay behind it.</li>
 <li>Then comes a layer with the ID <i>-100</i> that shows big letters "TEST".</li>
 <li>Finally there is the standard layer with ID <i>zero</i>. This layer contains the text "This is a small text on the standard layer".</li>
</ul>

<h3><b>Example 019</b> - Background image using different layers.</h3>
<pre><code>    XSPDF xsPDF = new XSPDF();
    final BufferedImage image = ImageIO.read(new File("img/graffiti.jpg"));
    xsPDF.setContentEncoding(NO_ENCODING);
    xsPDF.setPageSize(300, 300);
    xsPDF.setLayerID(Integer.MIN_VALUE);
    xsPDF.addColumn(0, 0, 300, 300, "background column", 0);
    xsPDF.setLayerID(-100);
    xsPDF.addColumn(0, 20, 300, 300, "single standard column", 0);
    xsPDF.setLayerID(0);
    xsPDF.addColumn(0, 10, 300, 300, "single standard column", 0);
    xsPDF.setImage(image, 0, 0, 300, 300, 0);
    xsPDF.nextColumn();
    xsPDF.setFont(HELVETICA, 150.0);
    xsPDF.setTextFillColor(Color.YELLOW);
    xsPDF.setTextRenderMode(TEXT_FILL);
    xsPDF.setAlignment(CENTERED);
    xsPDF.setLineLeading(0);
    xsPDF.setTextParagraphIndentationSpaces(0);
    xsPDF.print("TE");
    xsPDF.print("ST");
    xsPDF.nextColumn();
    xsPDF.setFont(COURIER, 45.0, BOLD);
    xsPDF.setTextFillColor(Color.WHITE).setTextStrokeColor(Color.BLACK);
    xsPDF.setTextRenderMode(TEXT_FILL_AND_STROKE);
    xsPDF.print("This is a small text on the standard layer.");
    xsPDF.createPdf("pdf/Example 019.pdf");
</code></pre>

</body>

