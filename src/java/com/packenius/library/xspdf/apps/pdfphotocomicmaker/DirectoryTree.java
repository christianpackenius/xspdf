/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.Component;
import java.awt.Font;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

/**
 * Creates a JTree object that contains a file system view.
 */
public class DirectoryTree implements TreeSelectionListener, TreeCellRenderer {
    /**
     * JTree object.
     */
    public final JTree tree;

    private List<FileListener> listeners = new ArrayList<FileListener>();

    /**
     * Constructor.
     */
    public DirectoryTree() {
        File[] roots = FileSystemView.getFileSystemView().getRoots();
        // FileSystem fileSystem = FileSystems.getDefault();
        // Iterable<Path> roots = fileSystem.getRootDirectories();
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("root", true);
        for (File root : roots) {
            DefaultMutableTreeNode leaf = new DefaultMutableTreeNode(root);
            leaf.setAllowsChildren(true);
            rootNode.add(leaf);
        }
        tree = new JTree(rootNode);
        tree.setRootVisible(false);
        tree.setCellRenderer(this);
        tree.addTreeSelectionListener(this);
    }

    /**
     * @see javax.swing.event.TreeSelectionListener#valueChanged(javax.swing.event.TreeSelectionEvent)
     */
    public void valueChanged(TreeSelectionEvent e) {
        TreePath path = e.getPath();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
        node.setAllowsChildren(true);
        if (node.isLeaf()) {
            File dir = (File) node.getUserObject();
            informListener(dir);
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        node.add(new DefaultMutableTreeNode(file, true));
                    }
                }
            }
        }
    }

    void addFileListener(FileListener listener) {
        listeners.add(listener);
    }

    private void informListener(File dir) {
        for (FileListener listener : listeners) {
            listener.changedDirectory(dir);
        }
    }

    /**
     * @see javax.swing.tree.TreeCellRenderer#getTreeCellRendererComponent(javax.swing.JTree, java.lang.Object, boolean,
     *      boolean, boolean, int, boolean)
     */
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf,
            int row, boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        File file = (File) node.getUserObject();
        String name = file.getName();
        if (name == null || name.length() == 0) {
            name = file.toString();
        }
        JLabel label = new JLabel(name);
        label.setFont(tree.getFont().deriveFont(selected ? Font.BOLD : Font.PLAIN));
        return label;
    }
}
