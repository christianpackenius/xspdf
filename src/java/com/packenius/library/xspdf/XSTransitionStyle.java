/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Transition styles for presentation mode.
 */
public enum XSTransitionStyle {
    /**
     * Split style.
     */
    Split("Split"),

    /**
     * Blinds style.
     */
    Blinds("Blinds"),

    /**
     * Box style.
     */
    Box("Box"),

    /**
     * Wipe style.
     */
    Wipe("Wipe"),

    /**
     * Dissolve style.
     */
    Dissolve("Dissolve"),

    /**
     * Glitter style.
     */
    Glitter("Glitter"),

    /**
     * Replace style (default).
     */
    Replace("R"),

    /**
     * Fly style.
     */
    Fly("Fly"),

    /**
     * Push style.
     */
    Push("Push"),

    /**
     * Cover style.
     */
    Cover("Cover"),

    /**
     * Uncover style.
     */
    Uncover("Uncover"),

    /**
     * Fade style.
     */
    Fade("Fade");

    final String string;

    XSTransitionStyle(String style) {
        string = "/S/" + style;
    }
}
