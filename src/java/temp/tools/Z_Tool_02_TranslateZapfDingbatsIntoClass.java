/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * @author Christian Packenius, 2013.
 */
class Z_Tool_02_TranslateZapfDingbatsIntoClass {
    /**
     * @param args
     * @throws IOException
     */
    // @SuppressWarnings("resource")
    public static void main(String[] args) throws IOException {
        BufferedReader in = null;
        PrintStream out = null;
        try {
            in = new BufferedReader(new FileReader(new File("docs/zapf-dingbats-orga.txt")));
            out = new PrintStream("src/java/com/packenius/library/xspdf/XSUnicodeZapfDingbats.java");
            out.println("package com.packenius.library.xspdf;");
            out.println();
            out.println("/**");
            out.println(" * ZapfDingbats character names and their unicode codes.");
            out.println(" */");
            out.println("interface XSUnicodeZapfDingbats {");
            out.println("  /**");
            out.println("   * Array with ZapfDingbats character names and their unicode codes.");
            out.println("   */");
            out.println("  static final String[] zapfDingbatsCodes = {");
            String s;
            while ((s = in.readLine()) != null) {
                // Example:
                // ((#x275E) "a100")
                if (!s.startsWith("((#x")) {
                    throw new RuntimeException();
                }
                int k1 = s.indexOf(") \"");
                out.println("      \"" + s.substring(k1 + 3, s.length() - 2) + "\", \"\\u" + s.substring(4, 8) + "\",");
            }
            out.println("  };");
            out.println("}");
        } finally {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
        }
    }
}
