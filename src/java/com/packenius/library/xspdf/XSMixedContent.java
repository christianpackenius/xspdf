/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.ArrayList;
import java.util.List;

/**
 * List of mixed content (strings and byte arrays).
 * @author Christian Packenius, 2015.
 */
class XSMixedContent {
    private List<Object> content = new ArrayList<Object>();

    /**
     * Byte content (or <i>null</i> if not created or changed).
     */
    private byte[] bytes = null;

    void append(byte[] ba) {
        bytes = null;
        content.add(ba);
    }

    /**
     * Adds a string to the list beginning.
     * @param s
     */
    void appendAhead(String s) {
        bytes = null;
        content.add(0, s);
    }

    /**
     * Adds a string to the end of the content list.
     * @param s Object to append.
     */
    void append(Object s) {
        bytes = null;

        // Special case: Last object of the (not empty) content is a StringBuilder object.
        // Then concat strings.
        if (!content.isEmpty()) {
            int index = content.size() - 1;
            Object last = content.get(index);
            if (last instanceof StringBuilder) {
                ((StringBuilder) last).append(s.toString());
                return;
            }
        }

        // Just append object.
        content.add(new StringBuilder(s.toString()));
    }

    /**
     * Get all bytes of the mixed content.
     * @return Byte array.
     */
    public byte[] getBytes() {
        if (bytes == null) {
            // Special case: Content is one single byte array.
            int contentSize = content.size();
            if (contentSize == 1) {
                Object object = content.get(0);
                if (object instanceof byte[]) {
                    bytes = (byte[]) object;
                    return bytes;
                }
            }

            // Convert every content string into a byte array.
            for (int i = 0; i < contentSize; i++) {
                Object object = content.get(i);
                if (!(object instanceof byte[])) {
                    content.set(i, object.toString().getBytes());
                }
            }

            // Count bytes.
            int byteCount = 0;
            for (Object object : content) {
                byteCount += ((byte[]) object).length;
            }

            // Create one big byte array.
            bytes = new byte[byteCount];
            int offset = 0;
            for (Object object : content) {
                byte[] ba = (byte[]) object;
                int baLength = ba.length;
                System.arraycopy(ba, 0, bytes, offset, baLength);
                offset += baLength;
            }

            // Content should be one single byte array.
            content.clear();
            content.add(bytes);
        }
        return bytes;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        throw new UnsupportedOperationException();
    }
}
