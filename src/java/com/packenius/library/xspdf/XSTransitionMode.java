/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Pur data object for transition mode in presentation mode between two pages.
 */
public class XSTransitionMode implements XSPdfContent {
    /**
     * Default transition mode - no transition.
     */
    public final static XSTransitionMode defaultInstance = new XSTransitionMode(null, null, null, null, 1.0);

    /**
     * Transition style.
     */
    public final XSTransitionStyle style;

    /**
     * Horizontal or vertical direction.
     */
    public final XSDirection direction;

    /**
     * Motion direction.
     */
    public final XSMotionDirection motionDirection;

    /**
     * Movement direction.
     */
    public final XSMovementDirection movementDirection;

    /**
     * Duration of this effekt.
     */
    public final double duration;

    /**
     * Constructor.
     * @param style Transition style.
     * @param direction Horizontal or vertical direction.
     * @param motionDirection Motion direction.
     * @param movementDirection Movement direction.
     * @param duration Duration in seconds. Default value is 1.
     */
    public XSTransitionMode(XSTransitionStyle style, XSDirection direction, XSMotionDirection motionDirection,
            XSMovementDirection movementDirection, double duration) {
        // Duration check.
        if (duration <= 0.0) {
            throw new XSPdfException("Duration [time in seconds] of transition mode must be positive!");
        }

        // Style check.
        if (style == null) {
            style = XSTransitionStyle.Replace;
        }

        // Direction check.
        if (style == XSTransitionStyle.Split || style == XSTransitionStyle.Blinds) {
            if (direction == null) {
                direction = XSDirection.Horizontal;
            }
        } else if (direction != null) {
            throw new XSPdfException("Direction (H/V) only allowed in transition styles Split or Blinds!");
        }

        // Motion direction check.
        if (style == XSTransitionStyle.Split || style == XSTransitionStyle.Box || style == XSTransitionStyle.Fly) {
            if (motionDirection == null) {
                motionDirection = XSMotionDirection.Inward;
            }
        } else if (motionDirection != null) {
            throw new XSPdfException("Motion direction (I/O) only allowed in transition styles Split, Box or Fly!");
        }

        // Movement direction check.
        if (style == XSTransitionStyle.Wipe || style == XSTransitionStyle.Glitter || style == XSTransitionStyle.Fly
            || style == XSTransitionStyle.Cover || style == XSTransitionStyle.Uncover || style == XSTransitionStyle.Push) {
            if (movementDirection == null) {
                movementDirection = XSMovementDirection.LeftToRight;
            } else if (movementDirection == XSMovementDirection.BottomToTop
                || movementDirection == XSMovementDirection.RightToLeft) {
                if (style != XSTransitionStyle.Wipe) {
                    throw new XSPdfException(
                        "Movement direction BottomToTop and RightToLeft only allowed in transition style Wipe!");
                }
            } else if (movementDirection == XSMovementDirection.TopLeftToBottomRight) {
                if (style != XSTransitionStyle.Glitter) {
                    throw new XSPdfException(
                        "Movement direction TopLeftToBottomRight only allowed in transition style Glitter!");
                }
            }
        } else if (movementDirection != null) {
            throw new XSPdfException(
                "Movement direction only allowed in transition styles Wipe, Glitter, Fly, Cover, Uncover or Push!");
        }

        this.style = style;
        this.direction = direction;
        this.motionDirection = motionDirection;
        this.movementDirection = movementDirection;
        this.duration = duration;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof XSTransitionMode)) {
            return false;
        }

        XSTransitionMode tm2 = (XSTransitionMode) obj;

        if (style != tm2.style) {
            return false;
        }
        if (direction != tm2.direction) {
            return false;
        }
        if (motionDirection != tm2.motionDirection) {
            return false;
        }
        if (movementDirection != tm2.movementDirection) {
            return false;
        }

        return true;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        String styleContent = style.string;
        if (direction != null) {
            styleContent += direction.getPdfContent(xsPDF);
        }
        if (motionDirection != null) {
            styleContent += motionDirection.getPdfContent(xsPDF);
        }
        if (movementDirection != null) {
            styleContent += movementDirection.getPdfContent(xsPDF);
        }
        if (duration != 1.0) {
            styleContent += "/D " + duration;
        }
        return "/Trans<<" + styleContent + ">>";
    }
}
