package com.packenius.library.xspdf;

/**
 * Alternative font encoding - only for SYMBOL font with 'apple' character
 * usage.
 * 
 * @author Christian Packenius, 2013.
 */
class XST1AltFontEncoding4 extends XSAlternativeFontEncoding {
	/**
	 * Maps an Utf-8 (Unicode?) code (0..65535) to the byte in this font
	 * encoding.
	 */
	static final byte[] altFontEncoding4 = new byte[65536];

	private static final String altFontEnc4Diffs = "255 /apple";

	static {
		altFontEncoding4['\uf8ff'] = (byte) 255;
	}

	public static final XST1AltFontEncoding4 instance = new XST1AltFontEncoding4();

	private XST1AltFontEncoding4() {
		// Singleton.
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getPdfDifferencesListContent()
	 */
	@Override
	public String getPdfDifferencesListContent() {
		return altFontEnc4Diffs;
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getID()
	 */
	@Override
	public String getID() {
		return "4";
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getCharacterEncodingMap()
	 */
	@Override
	public byte[] getCharacterEncodingMap() {
		return altFontEncoding4;
	}
}
