/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Special constant values.
 * @author Christian Packenius, 2013.
 */
public enum XSSpecials {
    /**
     * Use this constant to rotate a page. This will be no text rotation but a switch between landscape and portrait mode.
     */
    Rotate,

    /**
     * Constant for creating a new page.
     */
    NewPage,

    /**
     * Deletes all pages.
     */
    DeleteAllPages,

    /**
     * When using justification (left and right aligned) text, last line also should be full justified (left and right aligned).
     */
    LastLineFullJustified,

    /**
     * When using justification (left and right aligned) text, last line shall be only left aligned.
     */
    LastLineLeftAligned,

    /**
     * Set a single column into the page that has no margin.
     */
    SingleFullPageColumn;
}
