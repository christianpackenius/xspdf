/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import com.packenius.library.xspdf.XSUnicodeMapping;

/**
 * @author Christian Packenius, 2013.
 */
class Z_Tool_03_TranslateAfmFileIntoClass {
    /**
     * @param args
     * @throws IOException
     */
    // @SuppressWarnings("resource")
    public static void main(String[] args) throws IOException {
        // Alle Glyphinformationen aus den afm-Dateien sammeln.
        for (File file : new File("docs/afm-files").listFiles()) {
            if (file.getName().toLowerCase().endsWith(".afm")) {
                String fontName = file.getName();
                System.out.println("Bearbeite Font: " + fontName);
                fontName = fontName.substring(0, fontName.length() - 4).replace("-", "");
                String className = "XSType1StdFont" + fontName;
                PrintStream out = null;
                BufferedReader in = null;
                try {
                    out =
                        new PrintStream(new BufferedOutputStream(new FileOutputStream("src/java/com/packenius/library/xspdf/"
                            + className + ".java")));
                    in = new BufferedReader(new FileReader(file));
                    out.println("package com.packenius.library.xspdf;");
                    out.println("class " + className + " extends XSType1StdFont {");
                    int lastCharacterCode = 32;
                    String line;
                    while ((line = in.readLine()) != null) {
                        line = line.replace('\t', ' ');
                        if (line.startsWith("FontName ")) {
                            out.println("  @Override");
                            out.println("  String getFontName() {");
                            out.println("    return \"" + line.substring(9) + "\";");
                            out.println("  }");
                        } else if (line.startsWith("Ascender ")) {
                            out.println("  @Override");
                            out.println("  double getAscender() {");
                            out.println("    return " + Integer.parseInt(line.substring(9)) / 1000.0 + ";");
                            out.println("  }");
                        } else if (line.startsWith("Descender ")) {
                            out.println("  @Override");
                            out.println("  double getDescender() {");
                            out.println("    return " + Integer.parseInt(line.substring(10)) / 1000.0 + ";");
                            out.println("  }");
                        } else if (line.startsWith("StartCharMetrics ")) {
                            out.println("  @Override");
                            out.println("  void addCharMetrics() {");
                        } else if (line.startsWith("EndCharMetrics")) {
                            out.println("  }");
                        } else if (line.startsWith("StartKernPairs ")) {
                            out.println("  @Override");
                            out.println("  void addKernPairs() {");
                        } else if (line.startsWith("EndKernPairs")) {
                            out.println("  }");
                        } else if (line.startsWith("C ")) {
                            int glyphStandardEncoding = Integer.parseInt(line.substring(1, line.indexOf(';')).trim());
                            lastCharacterCode++;
                            while (lastCharacterCode < glyphStandardEncoding) {
                                // System.out.println("Empty code: " +
                                // lastCharacterCode);
                                lastCharacterCode++;
                            }
                            lastCharacterCode = glyphStandardEncoding;
                            if (glyphStandardEncoding < -1 || glyphStandardEncoding > 255) {
                                throw new RuntimeException(line);
                            }
                            int k = line.indexOf(" ; WX ") + 6;
                            if (k < 6) {
                                throw new RuntimeException();
                            }
                            int glyphWidth = Integer.parseInt(line.substring(k, line.indexOf(' ', k)));
                            k = line.indexOf(" ; N ") + 5;
                            if (k < 5) {
                                throw new RuntimeException();
                            }
                            String glyphName = line.substring(k, line.indexOf(' ', k));
                            String unicodeCode = Integer.toHexString(XSUnicodeMapping.getCodeFromName(glyphName));
                            unicodeCode = "0000".substring(unicodeCode.length()) + unicodeCode;
                            String unicodeCharacterAsChar = getUnicodeCharacter(unicodeCode);
                            if (glyphStandardEncoding == -1) {
                                System.out.println("'" + unicodeCharacterAsChar + "'");
                            }
                            out.println("    glyphWidths.put('" + unicodeCharacterAsChar + "', " + glyphWidth + ".0 / 1000.0);");
                            if (glyphStandardEncoding >= 0) {
                                // System.out.println("Glyph: " + glyphName);
                                Character unicodeCharacter = XSUnicodeMapping.getCodeFromName(glyphName);
                                if (unicodeCharacter != null) {
                                    out.println("    standardEncodingCodeFromUnicodeCharacter.put((char) "
                                        + (int) unicodeCharacter + ", (char) " + glyphStandardEncoding + ");");
                                }
                            }
                        }
                        // KPX Aring Otilde -40
                        else if (line.startsWith("KPX ")) {
                            String[] kpa = line.trim().split(" ");
                            if (kpa.length != 4) {
                                throw new RuntimeException();
                            }
                            out.println("    glyphPairKerning.put(\"" + kpa[1] + "-" + kpa[2] + "\", " + kpa[3]
                                + ".0 / 1000.0);");
                        }
                    }
                    out.println("}");
                } finally {
                    if (in != null) {
                        in.close();
                    }
                    if (out != null) {
                        out.close();
                    }
                }
            }
        }
    }

    private static String getUnicodeCharacter(String unicodeCode) {
        if (unicodeCode.equals("005c")) {
            return "\\\\";
        }
        if (unicodeCode.equals("0027")) {
            return "\\'";
        }
        return "\\u" + unicodeCode;
    }
}
