package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSPDF;

/**
 * Colored backgrounds and text lines.
 */
public class Example015 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.addContentListener(new XSContentListenerAdapter() {
			@Override
			public void newPage(XSPDF xsPDF, int pageNumber) {
				int grayValue = 24 * pageNumber;
				xsPDF.setPageBackgroundColor(new Color(grayValue, grayValue, grayValue));
			}

			@Override
			public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
				int xValue = lineNumber * 18;
				xsPDF.setTextFillColor(new Color(xValue, 255 - xValue, 255 - xValue));
			}
		});
		xsPDF.setTextFillColor(Color.WHITE);
		xsPDF.setPageSize(DIN_A7);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.createPdf("pdf/Example 015.pdf");
	}
}
