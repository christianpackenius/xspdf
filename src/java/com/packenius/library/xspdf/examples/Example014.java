package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSPDF;

/**
 * Content listener example.
 */
public class Example014 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setTextParagraphIndentationSpaces(0);
		xsPDF.setAlignment(LEFT_ALIGNED);
		xsPDF.addContentListener(new XSContentListenerAdapter() {
			Color[] colors = { Color.RED, Color.BLUE, Color.MAGENTA, Color.GREEN };

			int colorID = 0;

			@Override
			public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
				if (textPart != " ") {
					xsPDF.setTextFillColor(colors[colorID++ % colors.length]);
				}
			}
		});
		xsPDF.print("In this sentence the text color will change with every text part.\r\n\n");
		xsPDF.setTextFormattingMode(NO_FORMATTING);
		xsPDF.print("In this sentence the text color will change with every text part");
		xsPDF.print(".\r\n");
		xsPDF.createPdf("pdf/Example 014.pdf");
	}
}
