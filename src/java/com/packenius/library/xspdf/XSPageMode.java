/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Distinct page modes for whole PDF document.
 * @author Christian Packenius, 2013.
 */
public enum XSPageMode {
    /**
     * Normal mode - but do not show outlines or thumbs.
     */
    ShowDocumentOnly("UseNone"),

    /**
     * Show thumbnails of the document pages.
     */
    ShowThumbnails("UseThumbs"),

    /**
     * Full Screen - do not show any other window controls, only show the document content.
     */
    FullScreen("FullScreen");

    final String name;

    XSPageMode(String name) {
        this.name = name;
    }
}
