/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Static Constants for Unicode characters.
 * @author Christian Packenius, 2015.
 */
public interface XSUnicodes {
    /**
     * A
     */
    static final char _A = '\u0041';

    /**
     * AE
     */
    static final char _AE = '\u00C6';

    /**
     * Aacute
     */
    static final char _Aacute = '\u00C1';

    /**
     * Abreve
     */
    static final char _Abreve = '\u0102';

    /**
     * Acircumflex
     */
    static final char _Acircumflex = '\u00C2';

    /**
     * Adieresis
     */
    static final char _Adieresis = '\u00C4';

    /**
     * Agrave
     */
    static final char _Agrave = '\u00C0';

    /**
     * Alpha
     */
    static final char _Alpha = '\u0391';

    /**
     * Amacron
     */
    static final char _Amacron = '\u0100';

    /**
     * Aogonek
     */
    static final char _Aogonek = '\u0104';

    /**
     * Aring
     */
    static final char _Aring = '\u00C5';

    /**
     * Atilde
     */
    static final char _Atilde = '\u00C3';

    /**
     * B
     */
    static final char _B = '\u0042';

    /**
     * Beta
     */
    static final char _Beta = '\u0392';

    /**
     * C
     */
    static final char _C = '\u0043';

    /**
     * Cacute
     */
    static final char _Cacute = '\u0106';

    /**
     * Ccaron
     */
    static final char _Ccaron = '\u010C';

    /**
     * Ccedilla
     */
    static final char _Ccedilla = '\u00C7';

    /**
     * Chi
     */
    static final char _Chi = '\u03A7';

    /**
     * D
     */
    static final char _D = '\u0044';

    /**
     * Dcaron
     */
    static final char _Dcaron = '\u010E';

    /**
     * Dcroat
     */
    static final char _Dcroat = '\u0110';

    /**
     * Delta
     */
    static final char _Delta = '\u2206';

    /**
     * E
     */
    static final char _E = '\u0045';

    /**
     * Eacute
     */
    static final char _Eacute = '\u00C9';

    /**
     * Ecaron
     */
    static final char _Ecaron = '\u011A';

    /**
     * Ecircumflex
     */
    static final char _Ecircumflex = '\u00CA';

    /**
     * Edieresis
     */
    static final char _Edieresis = '\u00CB';

    /**
     * Edotaccent
     */
    static final char _Edotaccent = '\u0116';

    /**
     * Egrave
     */
    static final char _Egrave = '\u00C8';

    /**
     * Emacron
     */
    static final char _Emacron = '\u0112';

    /**
     * Eogonek
     */
    static final char _Eogonek = '\u0118';

    /**
     * Epsilon
     */
    static final char _Epsilon = '\u0395';

    /**
     * Eta
     */
    static final char _Eta = '\u0397';

    /**
     * Eth
     */
    static final char _Eth = '\u00D0';

    /**
     * Euro
     */
    static final char _Euro = '\u20AC';

    /**
     * F
     */
    static final char _F = '\u0046';

    /**
     * G
     */
    static final char _G = '\u0047';

    /**
     * Gamma
     */
    static final char _Gamma = '\u0393';

    /**
     * Gbreve
     */
    static final char _Gbreve = '\u011E';

    /**
     * Gcommaaccent
     */
    static final char _Gcommaaccent = '\u0122';

    /**
     * H
     */
    static final char _H = '\u0048';

    /**
     * I
     */
    static final char _I = '\u0049';

    /**
     * Iacute
     */
    static final char _Iacute = '\u00CD';

    /**
     * Icircumflex
     */
    static final char _Icircumflex = '\u00CE';

    /**
     * Idieresis
     */
    static final char _Idieresis = '\u00CF';

    /**
     * Idotaccent
     */
    static final char _Idotaccent = '\u0130';

    /**
     * Ifraktur
     */
    static final char _Ifraktur = '\u2111';

    /**
     * Igrave
     */
    static final char _Igrave = '\u00CC';

    /**
     * Imacron
     */
    static final char _Imacron = '\u012A';

    /**
     * Iogonek
     */
    static final char _Iogonek = '\u012E';

    /**
     * Iota
     */
    static final char _Iota = '\u0399';

    /**
     * J
     */
    static final char _J = '\u004A';

    /**
     * K
     */
    static final char _K = '\u004B';

    /**
     * Kappa
     */
    static final char _Kappa = '\u039A';

    /**
     * Kcommaaccent
     */
    static final char _Kcommaaccent = '\u0136';

    /**
     * L
     */
    static final char _L = '\u004C';

    /**
     * Lacute
     */
    static final char _Lacute = '\u0139';

    /**
     * Lambda
     */
    static final char _Lambda = '\u039B';

    /**
     * Lcaron
     */
    static final char _Lcaron = '\u013D';

    /**
     * Lcommaaccent
     */
    static final char _Lcommaaccent = '\u013B';

    /**
     * Lslash
     */
    static final char _Lslash = '\u0141';

    /**
     * M
     */
    static final char _M = '\u004D';

    /**
     * Mu
     */
    static final char _Mu = '\u039C';

    /**
     * N
     */
    static final char _N = '\u004E';

    /**
     * Nacute
     */
    static final char _Nacute = '\u0143';

    /**
     * Ncaron
     */
    static final char _Ncaron = '\u0147';

    /**
     * Ncommaaccent
     */
    static final char _Ncommaaccent = '\u0145';

    /**
     * Ntilde
     */
    static final char _Ntilde = '\u00D1';

    /**
     * Nu
     */
    static final char _Nu = '\u039D';

    /**
     * O
     */
    static final char _O = '\u004F';

    /**
     * OE
     */
    static final char _OE = '\u0152';

    /**
     * Oacute
     */
    static final char _Oacute = '\u00D3';

    /**
     * Ocircumflex
     */
    static final char _Ocircumflex = '\u00D4';

    /**
     * Odieresis
     */
    static final char _Odieresis = '\u00D6';

    /**
     * Ograve
     */
    static final char _Ograve = '\u00D2';

    /**
     * Ohungarumlaut
     */
    static final char _Ohungarumlaut = '\u0150';

    /**
     * Omacron
     */
    static final char _Omacron = '\u014C';

    /**
     * Omega
     */
    static final char _Omega = '\u2126';

    /**
     * Omicron
     */
    static final char _Omicron = '\u039F';

    /**
     * Oslash
     */
    static final char _Oslash = '\u00D8';

    /**
     * Otilde
     */
    static final char _Otilde = '\u00D5';

    /**
     * P
     */
    static final char _P = '\u0050';

    /**
     * Phi
     */
    static final char _Phi = '\u03A6';

    /**
     * Pi
     */
    static final char _Pi = '\u03A0';

    /**
     * Psi
     */
    static final char _Psi = '\u03A8';

    /**
     * Q
     */
    static final char _Q = '\u0051';

    /**
     * R
     */
    static final char _R = '\u0052';

    /**
     * Racute
     */
    static final char _Racute = '\u0154';

    /**
     * Rcaron
     */
    static final char _Rcaron = '\u0158';

    /**
     * Rcommaaccent
     */
    static final char _Rcommaaccent = '\u0156';

    /**
     * Rfraktur
     */
    static final char _Rfraktur = '\u211C';

    /**
     * Rho
     */
    static final char _Rho = '\u03A1';

    /**
     * S
     */
    static final char _S = '\u0053';

    /**
     * Sacute
     */
    static final char _Sacute = '\u015A';

    /**
     * Scaron
     */
    static final char _Scaron = '\u0160';

    /**
     * Scedilla
     */
    static final char _Scedilla = '\u015E';

    /**
     * Scommaaccent
     */
    static final char _Scommaaccent = '\u0218';

    /**
     * Sigma
     */
    static final char _Sigma = '\u03A3';

    /**
     * T
     */
    static final char _T = '\u0054';

    /**
     * Tau
     */
    static final char _Tau = '\u03A4';

    /**
     * Tcaron
     */
    static final char _Tcaron = '\u0164';

    /**
     * Tcommaaccent
     */
    static final char _Tcommaaccent = '\u0162';

    /**
     * Theta
     */
    static final char _Theta = '\u0398';

    /**
     * Thorn
     */
    static final char _Thorn = '\u00DE';

    /**
     * U
     */
    static final char _U = '\u0055';

    /**
     * Uacute
     */
    static final char _Uacute = '\u00DA';

    /**
     * Ucircumflex
     */
    static final char _Ucircumflex = '\u00DB';

    /**
     * Udieresis
     */
    static final char _Udieresis = '\u00DC';

    /**
     * Ugrave
     */
    static final char _Ugrave = '\u00D9';

    /**
     * Uhungarumlaut
     */
    static final char _Uhungarumlaut = '\u0170';

    /**
     * Umacron
     */
    static final char _Umacron = '\u016A';

    /**
     * Uogonek
     */
    static final char _Uogonek = '\u0172';

    /**
     * Upsilon
     */
    static final char _Upsilon = '\u03A5';

    /**
     * Upsilon1
     */
    static final char _Upsilon1 = '\u03D2';

    /**
     * Uring
     */
    static final char _Uring = '\u016E';

    /**
     * V
     */
    static final char _V = '\u0056';

    /**
     * W
     */
    static final char _W = '\u0057';

    /**
     * X
     */
    static final char _X = '\u0058';

    /**
     * Xi
     */
    static final char _Xi = '\u039E';

    /**
     * Y
     */
    static final char _Y = '\u0059';

    /**
     * Yacute
     */
    static final char _Yacute = '\u00DD';

    /**
     * Ydieresis
     */
    static final char _Ydieresis = '\u0178';

    /**
     * Z
     */
    static final char _Z = '\u005A';

    /**
     * Zacute
     */
    static final char _Zacute = '\u0179';

    /**
     * Zcaron
     */
    static final char _Zcaron = '\u017D';

    /**
     * Zdotaccent
     */
    static final char _Zdotaccent = '\u017B';

    /**
     * Zeta
     */
    static final char _Zeta = '\u0396';

    /**
     * a
     */
    static final char _a = '\u0061';

    /**
     * aacute
     */
    static final char _aacute = '\u00E1';

    /**
     * abreve
     */
    static final char _abreve = '\u0103';

    /**
     * acircumflex
     */
    static final char _acircumflex = '\u00E2';

    /**
     * acute
     */
    static final char _acute = '\u00B4';

    /**
     * adieresis
     */
    static final char _adieresis = '\u00E4';

    /**
     * ae
     */
    static final char _ae = '\u00E6';

    /**
     * agrave
     */
    static final char _agrave = '\u00E0';

    /**
     * aleph
     */
    static final char _aleph = '\u2135';

    /**
     * alpha
     */
    static final char _alpha = '\u03B1';

    /**
     * amacron
     */
    static final char _amacron = '\u0101';

    /**
     * ampersand
     */
    static final char _ampersand = '\u0026';

    /**
     * angle
     */
    static final char _angle = '\u2220';

    /**
     * angleleft
     */
    static final char _angleleft = '\u2329';

    /**
     * angleright
     */
    static final char _angleright = '\u232A';

    /**
     * aogonek
     */
    static final char _aogonek = '\u0105';

    /**
     * apple
     */
    static final char _apple = '\uF8FF';

    /**
     * approxequal
     */
    static final char _approxequal = '\u2248';

    /**
     * aring
     */
    static final char _aring = '\u00E5';

    /**
     * arrowboth
     */
    static final char _arrowboth = '\u2194';

    /**
     * arrowdblboth
     */
    static final char _arrowdblboth = '\u21D4';

    /**
     * arrowdbldown
     */
    static final char _arrowdbldown = '\u21D3';

    /**
     * arrowdblleft
     */
    static final char _arrowdblleft = '\u21D0';

    /**
     * arrowdblright
     */
    static final char _arrowdblright = '\u21D2';

    /**
     * arrowdblup
     */
    static final char _arrowdblup = '\u21D1';

    /**
     * arrowdown
     */
    static final char _arrowdown = '\u2193';

    /**
     * arrowhorizex
     */
    static final char _arrowhorizex = '\uF8E7';

    /**
     * arrowleft
     */
    static final char _arrowleft = '\u2190';

    /**
     * arrowright
     */
    static final char _arrowright = '\u2192';

    /**
     * arrowup
     */
    static final char _arrowup = '\u2191';

    /**
     * arrowvertex
     */
    static final char _arrowvertex = '\uF8E6';

    /**
     * asciicircum
     */
    static final char _asciicircum = '\u005E';

    /**
     * asciitilde
     */
    static final char _asciitilde = '\u007E';

    /**
     * asterisk
     */
    static final char _asterisk = '\u002A';

    /**
     * asteriskmath
     */
    static final char _asteriskmath = '\u2217';

    /**
     * at
     */
    static final char _at = '\u0040';

    /**
     * atilde
     */
    static final char _atilde = '\u00E3';

    /**
     * b
     */
    static final char _b = '\u0062';

    /**
     * backslash
     */
    static final char _backslash = '\\';

    /**
     * bar
     */
    static final char _bar = '\u007C';

    /**
     * beta
     */
    static final char _beta = '\u03B2';

    /**
     * braceex
     */
    static final char _braceex = '\uF8F4';

    /**
     * braceleft
     */
    static final char _braceleft = '\u007B';

    /**
     * braceleftbt
     */
    static final char _braceleftbt = '\uF8F3';

    /**
     * braceleftmid
     */
    static final char _braceleftmid = '\uF8F2';

    /**
     * bracelefttp
     */
    static final char _bracelefttp = '\uF8F1';

    /**
     * braceright
     */
    static final char _braceright = '\u007D';

    /**
     * bracerightbt
     */
    static final char _bracerightbt = '\uF8FE';

    /**
     * bracerightmid
     */
    static final char _bracerightmid = '\uF8FD';

    /**
     * bracerighttp
     */
    static final char _bracerighttp = '\uF8FC';

    /**
     * bracketleft
     */
    static final char _bracketleft = '\u005B';

    /**
     * bracketleftbt
     */
    static final char _bracketleftbt = '\uF8F0';

    /**
     * bracketleftex
     */
    static final char _bracketleftex = '\uF8EF';

    /**
     * bracketlefttp
     */
    static final char _bracketlefttp = '\uF8EE';

    /**
     * bracketright
     */
    static final char _bracketright = '\u005D';

    /**
     * bracketrightbt
     */
    static final char _bracketrightbt = '\uF8FB';

    /**
     * bracketrightex
     */
    static final char _bracketrightex = '\uF8FA';

    /**
     * bracketrighttp
     */
    static final char _bracketrighttp = '\uF8F9';

    /**
     * breve
     */
    static final char _breve = '\u02D8';

    /**
     * brokenbar
     */
    static final char _brokenbar = '\u00A6';

    /**
     * bullet
     */
    static final char _bullet = '\u2022';

    /**
     * c
     */
    static final char _c = '\u0063';

    /**
     * cacute
     */
    static final char _cacute = '\u0107';

    /**
     * caron
     */
    static final char _caron = '\u02C7';

    /**
     * carriagereturn
     */
    static final char _carriagereturn = '\u21B5';

    /**
     * ccaron
     */
    static final char _ccaron = '\u010D';

    /**
     * ccedilla
     */
    static final char _ccedilla = '\u00E7';

    /**
     * cedilla
     */
    static final char _cedilla = '\u00B8';

    /**
     * cent
     */
    static final char _cent = '\u00A2';

    /**
     * chi
     */
    static final char _chi = '\u03C7';

    /**
     * circlemultiply
     */
    static final char _circlemultiply = '\u2297';

    /**
     * circleplus
     */
    static final char _circleplus = '\u2295';

    /**
     * circumflex
     */
    static final char _circumflex = '\u02C6';

    /**
     * club
     */
    static final char _club = '\u2663';

    /**
     * colon
     */
    static final char _colon = '\u003A';

    /**
     * comma
     */
    static final char _comma = '\u002C';

    /**
     * commaaccent
     */
    static final char _commaaccent = '\uF6C3';

    /**
     * congruent
     */
    static final char _congruent = '\u2245';

    /**
     * copyright
     */
    static final char _copyright = '\u00A9';

    /**
     * copyrightsans
     */
    static final char _copyrightsans = '\uF8E9';

    /**
     * copyrightserif
     */
    static final char _copyrightserif = '\uF6D9';

    /**
     * currency
     */
    static final char _currency = '\u00A4';

    /**
     * d
     */
    static final char _d = '\u0064';

    /**
     * dagger
     */
    static final char _dagger = '\u2020';

    /**
     * daggerdbl
     */
    static final char _daggerdbl = '\u2021';

    /**
     * dcaron
     */
    static final char _dcaron = '\u010F';

    /**
     * dcroat
     */
    static final char _dcroat = '\u0111';

    /**
     * degree
     */
    static final char _degree = '\u00B0';

    /**
     * delta
     */
    static final char _delta = '\u03B4';

    /**
     * diamond
     */
    static final char _diamond = '\u2666';

    /**
     * dieresis
     */
    static final char _dieresis = '\u00A8';

    /**
     * divide
     */
    static final char _divide = '\u00F7';

    /**
     * dollar
     */
    static final char _dollar = '\u0024';

    /**
     * dotaccent
     */
    static final char _dotaccent = '\u02D9';

    /**
     * dotlessi
     */
    static final char _dotlessi = '\u0131';

    /**
     * dotmath
     */
    static final char _dotmath = '\u22C5';

    /**
     * e
     */
    static final char _e = '\u0065';

    /**
     * eacute
     */
    static final char _eacute = '\u00E9';

    /**
     * ecaron
     */
    static final char _ecaron = '\u011B';

    /**
     * ecircumflex
     */
    static final char _ecircumflex = '\u00EA';

    /**
     * edieresis
     */
    static final char _edieresis = '\u00EB';

    /**
     * edotaccent
     */
    static final char _edotaccent = '\u0117';

    /**
     * egrave
     */
    static final char _egrave = '\u00E8';

    /**
     * eight
     */
    static final char _eight = '\u0038';

    /**
     * element
     */
    static final char _element = '\u2208';

    /**
     * ellipsis
     */
    static final char _ellipsis = '\u2026';

    /**
     * emacron
     */
    static final char _emacron = '\u0113';

    /**
     * emdash
     */
    static final char _emdash = '\u2014';

    /**
     * emptyset
     */
    static final char _emptyset = '\u2205';

    /**
     * endash
     */
    static final char _endash = '\u2013';

    /**
     * eogonek
     */
    static final char _eogonek = '\u0119';

    /**
     * epsilon
     */
    static final char _epsilon = '\u03B5';

    /**
     * equal
     */
    static final char _equal = '\u003D';

    /**
     * equivalence
     */
    static final char _equivalence = '\u2261';

    /**
     * eta
     */
    static final char _eta = '\u03B7';

    /**
     * eth
     */
    static final char _eth = '\u00F0';

    /**
     * exclam
     */
    static final char _exclam = '\u0021';

    /**
     * exclamdown
     */
    static final char _exclamdown = '\u00A1';

    /**
     * existential
     */
    static final char _existential = '\u2203';

    /**
     * f
     */
    static final char _f = '\u0066';

    /**
     * fi
     */
    static final char _fi = '\uFB01';

    /**
     * five
     */
    static final char _five = '\u0035';

    /**
     * fl
     */
    static final char _fl = '\uFB02';

    /**
     * florin
     */
    static final char _florin = '\u0192';

    /**
     * four
     */
    static final char _four = '\u0034';

    /**
     * fraction
     */
    static final char _fraction = '\u2044';

    /**
     * g
     */
    static final char _g = '\u0067';

    /**
     * gamma
     */
    static final char _gamma = '\u03B3';

    /**
     * gbreve
     */
    static final char _gbreve = '\u011F';

    /**
     * gcommaaccent
     */
    static final char _gcommaaccent = '\u0123';

    /**
     * germandbls
     */
    static final char _germandbls = '\u00DF';

    /**
     * gradient
     */
    static final char _gradient = '\u2207';

    /**
     * grave
     */
    static final char _grave = '\u0060';

    /**
     * greater
     */
    static final char _greater = '\u003E';

    /**
     * greaterequal
     */
    static final char _greaterequal = '\u2265';

    /**
     * guillemotleft
     */
    static final char _guillemotleft = '\u00AB';

    /**
     * guillemotright
     */
    static final char _guillemotright = '\u00BB';

    /**
     * guilsinglleft
     */
    static final char _guilsinglleft = '\u2039';

    /**
     * guilsinglright
     */
    static final char _guilsinglright = '\u203A';

    /**
     * h
     */
    static final char _h = '\u0068';

    /**
     * heart
     */
    static final char _heart = '\u2665';

    /**
     * hungarumlaut
     */
    static final char _hungarumlaut = '\u02DD';

    /**
     * hyphen
     */
    static final char _hyphen = '\u002D';

    /**
     * i
     */
    static final char _i = '\u0069';

    /**
     * iacute
     */
    static final char _iacute = '\u00ED';

    /**
     * icircumflex
     */
    static final char _icircumflex = '\u00EE';

    /**
     * idieresis
     */
    static final char _idieresis = '\u00EF';

    /**
     * igrave
     */
    static final char _igrave = '\u00EC';

    /**
     * imacron
     */
    static final char _imacron = '\u012B';

    /**
     * infinity
     */
    static final char _infinity = '\u221E';

    /**
     * integral
     */
    static final char _integral = '\u222B';

    /**
     * integralbt
     */
    static final char _integralbt = '\u2321';

    /**
     * integralex
     */
    static final char _integralex = '\uF8F5';

    /**
     * integraltp
     */
    static final char _integraltp = '\u2320';

    /**
     * intersection
     */
    static final char _intersection = '\u2229';

    /**
     * iogonek
     */
    static final char _iogonek = '\u012F';

    /**
     * iota
     */
    static final char _iota = '\u03B9';

    /**
     * j
     */
    static final char _j = '\u006A';

    /**
     * k
     */
    static final char _k = '\u006B';

    /**
     * kappa
     */
    static final char _kappa = '\u03BA';

    /**
     * kcommaaccent
     */
    static final char _kcommaaccent = '\u0137';

    /**
     * l
     */
    static final char _l = '\u006C';

    /**
     * lacute
     */
    static final char _lacute = '\u013A';

    /**
     * lambda
     */
    static final char _lambda = '\u03BB';

    /**
     * lcaron
     */
    static final char _lcaron = '\u013E';

    /**
     * lcommaaccent
     */
    static final char _lcommaaccent = '\u013C';

    /**
     * less
     */
    static final char _less = '\u003C';

    /**
     * lessequal
     */
    static final char _lessequal = '\u2264';

    /**
     * logicaland
     */
    static final char _logicaland = '\u2227';

    /**
     * logicalnot
     */
    static final char _logicalnot = '\u00AC';

    /**
     * logicalor
     */
    static final char _logicalor = '\u2228';

    /**
     * lozenge
     */
    static final char _lozenge = '\u25CA';

    /**
     * lslash
     */
    static final char _lslash = '\u0142';

    /**
     * m
     */
    static final char _m = '\u006D';

    /**
     * macron
     */
    static final char _macron = '\u00AF';

    /**
     * minus
     */
    static final char _minus = '\u2212';

    /**
     * minute
     */
    static final char _minute = '\u2032';

    /**
     * mu
     */
    static final char _mu = '\u00B5';

    /**
     * multiply
     */
    static final char _multiply = '\u00D7';

    /**
     * n
     */
    static final char _n = '\u006E';

    /**
     * nacute
     */
    static final char _nacute = '\u0144';

    /**
     * ncaron
     */
    static final char _ncaron = '\u0148';

    /**
     * ncommaaccent
     */
    static final char _ncommaaccent = '\u0146';

    /**
     * nine
     */
    static final char _nine = '\u0039';

    /**
     * notelement
     */
    static final char _notelement = '\u2209';

    /**
     * notequal
     */
    static final char _notequal = '\u2260';

    /**
     * notsubset
     */
    static final char _notsubset = '\u2284';

    /**
     * ntilde
     */
    static final char _ntilde = '\u00F1';

    /**
     * nu
     */
    static final char _nu = '\u03BD';

    /**
     * numbersign
     */
    static final char _numbersign = '\u0023';

    /**
     * o
     */
    static final char _o = '\u006F';

    /**
     * oacute
     */
    static final char _oacute = '\u00F3';

    /**
     * ocircumflex
     */
    static final char _ocircumflex = '\u00F4';

    /**
     * odieresis
     */
    static final char _odieresis = '\u00F6';

    /**
     * oe
     */
    static final char _oe = '\u0153';

    /**
     * ogonek
     */
    static final char _ogonek = '\u02DB';

    /**
     * ograve
     */
    static final char _ograve = '\u00F2';

    /**
     * ohungarumlaut
     */
    static final char _ohungarumlaut = '\u0151';

    /**
     * omacron
     */
    static final char _omacron = '\u014D';

    /**
     * omega
     */
    static final char _omega = '\u03C9';

    /**
     * omega1
     */
    static final char _omega1 = '\u03D6';

    /**
     * omicron
     */
    static final char _omicron = '\u03BF';

    /**
     * one
     */
    static final char _one = '\u0031';

    /**
     * onehalf
     */
    static final char _onehalf = '\u00BD';

    /**
     * onequarter
     */
    static final char _onequarter = '\u00BC';

    /**
     * onesuperior
     */
    static final char _onesuperior = '\u00B9';

    /**
     * ordfeminine
     */
    static final char _ordfeminine = '\u00AA';

    /**
     * ordmasculine
     */
    static final char _ordmasculine = '\u00BA';

    /**
     * oslash
     */
    static final char _oslash = '\u00F8';

    /**
     * otilde
     */
    static final char _otilde = '\u00F5';

    /**
     * p
     */
    static final char _p = '\u0070';

    /**
     * paragraph
     */
    static final char _paragraph = '\u00B6';

    /**
     * parenleft
     */
    static final char _parenleft = '\u0028';

    /**
     * parenleftbt
     */
    static final char _parenleftbt = '\uF8ED';

    /**
     * parenleftex
     */
    static final char _parenleftex = '\uF8EC';

    /**
     * parenlefttp
     */
    static final char _parenlefttp = '\uF8EB';

    /**
     * parenright
     */
    static final char _parenright = '\u0029';

    /**
     * parenrightbt
     */
    static final char _parenrightbt = '\uF8F8';

    /**
     * parenrightex
     */
    static final char _parenrightex = '\uF8F7';

    /**
     * parenrighttp
     */
    static final char _parenrighttp = '\uF8F6';

    /**
     * partialdiff
     */
    static final char _partialdiff = '\u2202';

    /**
     * percent
     */
    static final char _percent = '\u0025';

    /**
     * period
     */
    static final char _period = '\u002E';

    /**
     * periodcentered
     */
    static final char _periodcentered = '\u00B7';

    /**
     * perpendicular
     */
    static final char _perpendicular = '\u22A5';

    /**
     * perthousand
     */
    static final char _perthousand = '\u2030';

    /**
     * phi
     */
    static final char _phi = '\u03C6';

    /**
     * phi1
     */
    static final char _phi1 = '\u03D5';

    /**
     * pi
     */
    static final char _pi = '\u03C0';

    /**
     * plus
     */
    static final char _plus = '\u002B';

    /**
     * plusminus
     */
    static final char _plusminus = '\u00B1';

    /**
     * product
     */
    static final char _product = '\u220F';

    /**
     * propersubset
     */
    static final char _propersubset = '\u2282';

    /**
     * propersuperset
     */
    static final char _propersuperset = '\u2283';

    /**
     * proportional
     */
    static final char _proportional = '\u221D';

    /**
     * psi
     */
    static final char _psi = '\u03C8';

    /**
     * q
     */
    static final char _q = '\u0071';

    /**
     * question
     */
    static final char _question = '\u003F';

    /**
     * questiondown
     */
    static final char _questiondown = '\u00BF';

    /**
     * quotedbl
     */
    static final char _quotedbl = '\'';

    /**
     * quotedblbase
     */
    static final char _quotedblbase = '\u201E';

    /**
     * quotedblleft
     */
    static final char _quotedblleft = '\u201C';

    /**
     * quotedblright
     */
    static final char _quotedblright = '\u201D';

    /**
     * quoteleft
     */
    static final char _quoteleft = '\u2018';

    /**
     * quoteright
     */
    static final char _quoteright = '\u2019';

    /**
     * quotesinglbase
     */
    static final char _quotesinglbase = '\u201A';

    /**
     * quotesingle
     */
    static final char _quotesingle = '\'';

    /**
     * r
     */
    static final char _r = '\u0072';

    /**
     * racute
     */
    static final char _racute = '\u0155';

    /**
     * radical
     */
    static final char _radical = '\u221A';

    /**
     * radicalex
     */
    static final char _radicalex = '\uF8E5';

    /**
     * rcaron
     */
    static final char _rcaron = '\u0159';

    /**
     * rcommaaccent
     */
    static final char _rcommaaccent = '\u0157';

    /**
     * reflexsubset
     */
    static final char _reflexsubset = '\u2286';

    /**
     * reflexsuperset
     */
    static final char _reflexsuperset = '\u2287';

    /**
     * registered
     */
    static final char _registered = '\u00AE';

    /**
     * registersans
     */
    static final char _registersans = '\uF8E8';

    /**
     * registerserif
     */
    static final char _registerserif = '\uF6DA';

    /**
     * rho
     */
    static final char _rho = '\u03C1';

    /**
     * ring
     */
    static final char _ring = '\u02DA';

    /**
     * s
     */
    static final char _s = '\u0073';

    /**
     * sacute
     */
    static final char _sacute = '\u015B';

    /**
     * scaron
     */
    static final char _scaron = '\u0161';

    /**
     * scedilla
     */
    static final char _scedilla = '\u015F';

    /**
     * scommaaccent
     */
    static final char _scommaaccent = '\u0219';

    /**
     * second
     */
    static final char _second = '\u2033';

    /**
     * section
     */
    static final char _section = '\u00A7';

    /**
     * semicolon
     */
    static final char _semicolon = '\u003B';

    /**
     * seven
     */
    static final char _seven = '\u0037';

    /**
     * sigma
     */
    static final char _sigma = '\u03C3';

    /**
     * sigma1
     */
    static final char _sigma1 = '\u03C2';

    /**
     * similar
     */
    static final char _similar = '\u223C';

    /**
     * six
     */
    static final char _six = '\u0036';

    /**
     * slash
     */
    static final char _slash = '\u002F';

    /**
     * space
     */
    static final char _space = '\u0020';

    /**
     * spade
     */
    static final char _spade = '\u2660';

    /**
     * sterling
     */
    static final char _sterling = '\u00A3';

    /**
     * suchthat
     */
    static final char _suchthat = '\u220B';

    /**
     * summation
     */
    static final char _summation = '\u2211';

    /**
     * t
     */
    static final char _t = '\u0074';

    /**
     * tau
     */
    static final char _tau = '\u03C4';

    /**
     * tcaron
     */
    static final char _tcaron = '\u0165';

    /**
     * tcommaaccent
     */
    static final char _tcommaaccent = '\u0163';

    /**
     * therefore
     */
    static final char _therefore = '\u2234';

    /**
     * theta
     */
    static final char _theta = '\u03B8';

    /**
     * theta1
     */
    static final char _theta1 = '\u03D1';

    /**
     * thorn
     */
    static final char _thorn = '\u00FE';

    /**
     * three
     */
    static final char _three = '\u0033';

    /**
     * threequarters
     */
    static final char _threequarters = '\u00BE';

    /**
     * threesuperior
     */
    static final char _threesuperior = '\u00B3';

    /**
     * tilde
     */
    static final char _tilde = '\u02DC';

    /**
     * trademark
     */
    static final char _trademark = '\u2122';

    /**
     * trademarksans
     */
    static final char _trademarksans = '\uF8EA';

    /**
     * trademarkserif
     */
    static final char _trademarkserif = '\uF6DB';

    /**
     * two
     */
    static final char _two = '\u0032';

    /**
     * twosuperior
     */
    static final char _twosuperior = '\u00B2';

    /**
     * u
     */
    static final char _u = '\u0075';

    /**
     * uacute
     */
    static final char _uacute = '\u00FA';

    /**
     * ucircumflex
     */
    static final char _ucircumflex = '\u00FB';

    /**
     * udieresis
     */
    static final char _udieresis = '\u00FC';

    /**
     * ugrave
     */
    static final char _ugrave = '\u00F9';

    /**
     * uhungarumlaut
     */
    static final char _uhungarumlaut = '\u0171';

    /**
     * umacron
     */
    static final char _umacron = '\u016B';

    /**
     * underscore
     */
    static final char _underscore = '\u005F';

    /**
     * union
     */
    static final char _union = '\u222A';

    /**
     * universal
     */
    static final char _universal = '\u2200';

    /**
     * uogonek
     */
    static final char _uogonek = '\u0173';

    /**
     * upsilon
     */
    static final char _upsilon = '\u03C5';

    /**
     * uring
     */
    static final char _uring = '\u016F';

    /**
     * v
     */
    static final char _v = '\u0076';

    /**
     * w
     */
    static final char _w = '\u0077';

    /**
     * weierstrass
     */
    static final char _weierstrass = '\u2118';

    /**
     * x
     */
    static final char _x = '\u0078';

    /**
     * xi
     */
    static final char _xi = '\u03BE';

    /**
     * y
     */
    static final char _y = '\u0079';

    /**
     * yacute
     */
    static final char _yacute = '\u00FD';

    /**
     * ydieresis
     */
    static final char _ydieresis = '\u00FF';

    /**
     * yen
     */
    static final char _yen = '\u00A5';

    /**
     * z
     */
    static final char _z = '\u007A';

    /**
     * zacute
     */
    static final char _zacute = '\u017A';

    /**
     * zcaron
     */
    static final char _zcaron = '\u017E';

    /**
     * zdotaccent
     */
    static final char _zdotaccent = '\u017C';

    /**
     * zero
     */
    static final char _zero = '\u0030';

    /**
     * zeta
     */
    static final char _zeta = '\u03B6';
}
