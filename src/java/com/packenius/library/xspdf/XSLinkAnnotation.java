/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * An annotation that links to a place in the document or to a URI.
 * @author Christian Packenius, 2013.
 */
class XSLinkAnnotation extends XSAnnotation {
    final XSLink link;

    final double x;

    final double y;

    final double width;

    final double height;

    /**
     * Constructor.
     * @param link Link to a destination or an URL.
     * @param x
     * @param y
     * @param width
     * @param height
     */
    XSLinkAnnotation(XSLink link, double x, double y, double width, double height) {
        this.link = link;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(com.packenius.library.xspdf.XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        XSDestination destination = xsPDF.destinations.get(link);
        String destStr;
        if (link.url != null) {
            destStr = "/A<</S/URI/URI(" + XSStatics.escapeStandardStringCharacters(link.url.toString()) + ")>>";
        } else {
            destStr = destination.getPdfContent(xsPDF);
        }
        double borderSize = link.borderSize;
        double xx = x - borderSize;
        double yy = y - borderSize;
        double ww = width + borderSize * 2;
        double hh = height + borderSize * 2;
        String rect = "/Rect[" + xx + " " + yy + " " + (xx + ww) + " " + (yy + hh) + "]";
        return "<</Type/Annot/Subtype/Link" + rect + "/Border[0 0 " + borderSize + "]" + destStr + ">>";
    }
}
