/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

class XSType1StdFontZapfDingbats extends XSType1StdFont {
    public String getFontName() {
        return "ZapfDingbats";
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getLogicalFontName()
     */
    public String getLogicalFontName() {
        return "ZD";
    }

    /**
     * Pairwise data: First of each pair is a unicode character. Second is the width (in 1/1000) of this character.
     */
    private static final char[] validGlyphsAndWidths = new char[] {'\u0020', (char) 278, '\u2192', (char) 838, '\u2194',
        (char) 1016, '\u2195', (char) 458, '\u2460', (char) 788, '\u2461', (char) 788, '\u2462', (char) 788, '\u2463',
        (char) 788, '\u2464', (char) 788, '\u2465', (char) 788, '\u2466', (char) 788, '\u2467', (char) 788, '\u2468',
        (char) 788, '\u2469', (char) 788, '\u25a0', (char) 761, '\u25b2', (char) 892, '\u25bc', (char) 892, '\u25c6',
        (char) 788, '\u25cf', (char) 791, '\u25d7', (char) 438, '\u2605', (char) 816, '\u260e', (char) 719, '\u261b',
        (char) 960, '\u261e', (char) 939, '\u2660', (char) 626, '\u2663', (char) 776, '\u2665', (char) 694, '\u2666',
        (char) 595, '\u2701', (char) 974, '\u2702', (char) 961, '\u2703', (char) 974, '\u2704', (char) 980, '\u2706',
        (char) 789, '\u2707', (char) 790, '\u2708', (char) 791, '\u2709', (char) 690, '\u270c', (char) 549, '\u270d',
        (char) 855, '\u270e', (char) 911, '\u270f', (char) 933, '\u2710', (char) 911, '\u2711', (char) 945, '\u2712',
        (char) 974, '\u2713', (char) 755, '\u2714', (char) 846, '\u2715', (char) 762, '\u2716', (char) 761, '\u2717',
        (char) 571, '\u2718', (char) 677, '\u2719', (char) 763, '\u271a', (char) 760, '\u271b', (char) 759, '\u271c',
        (char) 754, '\u271d', (char) 494, '\u271e', (char) 552, '\u271f', (char) 537, '\u2720', (char) 577, '\u2721',
        (char) 692, '\u2722', (char) 786, '\u2723', (char) 788, '\u2724', (char) 788, '\u2725', (char) 790, '\u2726',
        (char) 793, '\u2727', (char) 794, '\u2729', (char) 823, '\u272a', (char) 789, '\u272b', (char) 841, '\u272c',
        (char) 823, '\u272d', (char) 833, '\u272e', (char) 816, '\u272f', (char) 831, '\u2730', (char) 923, '\u2731',
        (char) 744, '\u2732', (char) 723, '\u2733', (char) 749, '\u2734', (char) 790, '\u2735', (char) 792, '\u2736',
        (char) 695, '\u2737', (char) 776, '\u2738', (char) 768, '\u2739', (char) 792, '\u273a', (char) 759, '\u273b',
        (char) 707, '\u273c', (char) 708, '\u273d', (char) 682, '\u273e', (char) 701, '\u273f', (char) 826, '\u2740',
        (char) 815, '\u2741', (char) 789, '\u2742', (char) 789, '\u2743', (char) 707, '\u2744', (char) 687, '\u2745',
        (char) 696, '\u2746', (char) 689, '\u2747', (char) 786, '\u2748', (char) 787, '\u2749', (char) 713, '\u274a',
        (char) 791, '\u274b', (char) 785, '\u274d', (char) 873, '\u274f', (char) 762, '\u2750', (char) 762, '\u2751',
        (char) 759, '\u2752', (char) 759, '\u2756', (char) 784, '\u2758', (char) 138, '\u2759', (char) 277, '\u275a',
        (char) 415, '\u275b', (char) 392, '\u275c', (char) 392, '\u275d', (char) 668, '\u275e', (char) 668, '\u2761',
        (char) 732, '\u2762', (char) 544, '\u2763', (char) 544, '\u2764', (char) 910, '\u2765', (char) 667, '\u2766',
        (char) 760, '\u2767', (char) 760, '\u2768', (char) 390, '\u2769', (char) 390, '\u276a', (char) 317, '\u276b',
        (char) 317, '\u276c', (char) 276, '\u276d', (char) 276, '\u276e', (char) 509, '\u276f', (char) 509, '\u2770',
        (char) 410, '\u2771', (char) 410, '\u2772', (char) 234, '\u2773', (char) 234, '\u2774', (char) 334, '\u2775',
        (char) 334, '\u2776', (char) 788, '\u2777', (char) 788, '\u2778', (char) 788, '\u2779', (char) 788, '\u277a',
        (char) 788, '\u277b', (char) 788, '\u277c', (char) 788, '\u277d', (char) 788, '\u277e', (char) 788, '\u277f',
        (char) 788, '\u2780', (char) 788, '\u2781', (char) 788, '\u2782', (char) 788, '\u2783', (char) 788, '\u2784',
        (char) 788, '\u2785', (char) 788, '\u2786', (char) 788, '\u2787', (char) 788, '\u2788', (char) 788, '\u2789',
        (char) 788, '\u278a', (char) 788, '\u278b', (char) 788, '\u278c', (char) 788, '\u278d', (char) 788, '\u278e',
        (char) 788, '\u278f', (char) 788, '\u2790', (char) 788, '\u2791', (char) 788, '\u2792', (char) 788, '\u2793',
        (char) 788, '\u2794', (char) 894, '\u2798', (char) 748, '\u2799', (char) 924, '\u279a', (char) 748, '\u279b',
        (char) 918, '\u279c', (char) 927, '\u279d', (char) 928, '\u279e', (char) 928, '\u279f', (char) 834, '\u27a0',
        (char) 873, '\u27a1', (char) 828, '\u27a2', (char) 924, '\u27a3', (char) 924, '\u27a4', (char) 917, '\u27a5',
        (char) 930, '\u27a6', (char) 931, '\u27a7', (char) 463, '\u27a8', (char) 883, '\u27a9', (char) 836, '\u27aa',
        (char) 836, '\u27ab', (char) 867, '\u27ac', (char) 867, '\u27ad', (char) 696, '\u27ae', (char) 696, '\u27af',
        (char) 874, '\u27b1', (char) 874, '\u27b2', (char) 760, '\u27b3', (char) 946, '\u27b4', (char) 771, '\u27b5',
        (char) 865, '\u27b6', (char) 771, '\u27b7', (char) 888, '\u27b8', (char) 967, '\u27b9', (char) 888, '\u27ba',
        (char) 831, '\u27bb', (char) 873, '\u27bc', (char) 927, '\u27bd', (char) 970, '\u27be', (char) 918,};

    /**
     * Pairwise data: First is the character code, second is the standard font encoding byte within the PDF text stream.
     */
    private static final char[] standardEncodingCodes = new char[] {(char) 10000, (char) 48, (char) 10001, (char) 49,
        (char) 10002, (char) 50, (char) 10003, (char) 51, (char) 10004, (char) 52, (char) 10005, (char) 53, (char) 10006,
        (char) 54, (char) 10007, (char) 55, (char) 10008, (char) 56, (char) 10009, (char) 57, (char) 10010, (char) 58,
        (char) 10011, (char) 59, (char) 10012, (char) 60, (char) 10013, (char) 61, (char) 10014, (char) 62, (char) 10015,
        (char) 63, (char) 10016, (char) 64, (char) 10017, (char) 65, (char) 10018, (char) 66, (char) 10019, (char) 67,
        (char) 10020, (char) 68, (char) 10021, (char) 69, (char) 10022, (char) 70, (char) 10023, (char) 71, (char) 10025,
        (char) 73, (char) 10026, (char) 74, (char) 10027, (char) 75, (char) 10028, (char) 76, (char) 10029, (char) 77,
        (char) 10030, (char) 78, (char) 10031, (char) 79, (char) 10032, (char) 80, (char) 10033, (char) 81, (char) 10034,
        (char) 82, (char) 10035, (char) 83, (char) 10036, (char) 84, (char) 10037, (char) 85, (char) 10038, (char) 86,
        (char) 10039, (char) 87, (char) 10040, (char) 88, (char) 10041, (char) 89, (char) 10042, (char) 90, (char) 10043,
        (char) 91, (char) 10044, (char) 92, (char) 10045, (char) 93, (char) 10046, (char) 94, (char) 10047, (char) 95,
        (char) 10048, (char) 96, (char) 10049, (char) 97, (char) 10050, (char) 98, (char) 10051, (char) 99, (char) 10052,
        (char) 100, (char) 10053, (char) 101, (char) 10054, (char) 102, (char) 10055, (char) 103, (char) 10056, (char) 104,
        (char) 10057, (char) 105, (char) 10058, (char) 106, (char) 10059, (char) 107, (char) 10061, (char) 109, (char) 10063,
        (char) 111, (char) 10064, (char) 112, (char) 10065, (char) 113, (char) 10066, (char) 114, (char) 10070, (char) 118,
        (char) 10072, (char) 120, (char) 10073, (char) 121, (char) 10074, (char) 122, (char) 10075, (char) 123, (char) 10076,
        (char) 124, (char) 10077, (char) 125, (char) 10078, (char) 126, (char) 10081, (char) 161, (char) 10082, (char) 162,
        (char) 10083, (char) 163, (char) 10084, (char) 164, (char) 10085, (char) 165, (char) 10086, (char) 166, (char) 10087,
        (char) 167, (char) 10088, (char) 128, (char) 10089, (char) 129, (char) 10090, (char) 130, (char) 10091, (char) 131,
        (char) 10092, (char) 132, (char) 10093, (char) 133, (char) 10094, (char) 134, (char) 10095, (char) 135, (char) 10096,
        (char) 136, (char) 10097, (char) 137, (char) 10098, (char) 138, (char) 10099, (char) 139, (char) 10100, (char) 140,
        (char) 10101, (char) 141, (char) 10102, (char) 182, (char) 10103, (char) 183, (char) 10104, (char) 184, (char) 10105,
        (char) 185, (char) 10106, (char) 186, (char) 10107, (char) 187, (char) 10108, (char) 188, (char) 10109, (char) 189,
        (char) 10110, (char) 190, (char) 10111, (char) 191, (char) 10112, (char) 192, (char) 10113, (char) 193, (char) 10114,
        (char) 194, (char) 10115, (char) 195, (char) 10116, (char) 196, (char) 10117, (char) 197, (char) 10118, (char) 198,
        (char) 10119, (char) 199, (char) 10120, (char) 200, (char) 10121, (char) 201, (char) 10122, (char) 202, (char) 10123,
        (char) 203, (char) 10124, (char) 204, (char) 10125, (char) 205, (char) 10126, (char) 206, (char) 10127, (char) 207,
        (char) 10128, (char) 208, (char) 10129, (char) 209, (char) 10130, (char) 210, (char) 10131, (char) 211, (char) 10132,
        (char) 212, (char) 10136, (char) 216, (char) 10137, (char) 217, (char) 10138, (char) 218, (char) 10139, (char) 219,
        (char) 10140, (char) 220, (char) 10141, (char) 221, (char) 10142, (char) 222, (char) 10143, (char) 223, (char) 10144,
        (char) 224, (char) 10145, (char) 225, (char) 10146, (char) 226, (char) 10147, (char) 227, (char) 10148, (char) 228,
        (char) 10149, (char) 229, (char) 10150, (char) 230, (char) 10151, (char) 231, (char) 10152, (char) 232, (char) 10153,
        (char) 233, (char) 10154, (char) 234, (char) 10155, (char) 235, (char) 10156, (char) 236, (char) 10157, (char) 237,
        (char) 10158, (char) 238, (char) 10159, (char) 239, (char) 10161, (char) 241, (char) 10162, (char) 242, (char) 10163,
        (char) 243, (char) 10164, (char) 244, (char) 10165, (char) 245, (char) 10166, (char) 246, (char) 10167, (char) 247,
        (char) 10168, (char) 248, (char) 10169, (char) 249, (char) 10170, (char) 250, (char) 10171, (char) 251, (char) 10172,
        (char) 252, (char) 10173, (char) 253, (char) 10174, (char) 254, (char) 32, (char) 32, (char) 8594, (char) 213,
        (char) 8596, (char) 214, (char) 8597, (char) 215, (char) 9312, (char) 172, (char) 9313, (char) 173, (char) 9314,
        (char) 174, (char) 9315, (char) 175, (char) 9316, (char) 176, (char) 9317, (char) 177, (char) 9318, (char) 178,
        (char) 9319, (char) 179, (char) 9320, (char) 180, (char) 9321, (char) 181, (char) 9632, (char) 110, (char) 9650,
        (char) 115, (char) 9660, (char) 116, (char) 9670, (char) 117, (char) 9679, (char) 108, (char) 9687, (char) 119,
        (char) 9733, (char) 72, (char) 9742, (char) 37, (char) 9755, (char) 42, (char) 9758, (char) 43, (char) 9824,
        (char) 171, (char) 9827, (char) 168, (char) 9829, (char) 170, (char) 9830, (char) 169, (char) 9985, (char) 33,
        (char) 9986, (char) 34, (char) 9987, (char) 35, (char) 9988, (char) 36, (char) 9990, (char) 38, (char) 9991, (char) 39,
        (char) 9992, (char) 40, (char) 9993, (char) 41, (char) 9996, (char) 44, (char) 9997, (char) 45, (char) 9998, (char) 46,
        (char) 9999, (char) 47,};

    @Override
    void addCharMetrics() {
        for (int i = 0; i < validGlyphsAndWidths.length; i += 2) {
            glyphWidths[validGlyphsAndWidths[i]] = validGlyphsAndWidths[i + 1] / 1000.0;
        }
        for (int i = 0; i < standardEncodingCodes.length; i += 2) {
            standardEncodingCodeFromUnicodeCharacter[standardEncodingCodes[i]] = (byte) standardEncodingCodes[i + 1];
        }
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getFontWithParms(int)
     */
    public XSFontType getFontWithParms(int fontParm) {
        return this;
    }
}
