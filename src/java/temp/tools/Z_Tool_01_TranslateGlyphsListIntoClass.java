/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Christian Packenius, 2013.
 */
class Z_Tool_01_TranslateGlyphsListIntoClass {
	private static final Set<String> glyphNames = new HashSet<String>();

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		// Alle Glyphen aus den afm-Dateien sammeln.
		for (File file : new File("docs/afm-files").listFiles()) {
			if (file.getName().toLowerCase().endsWith(".afm")) {
				BufferedReader in = new BufferedReader(new FileReader(file));
				String line;
				while ((line = in.readLine()) != null) {
					if (line.startsWith("C ")) {
						int k = line.indexOf(" ; N ") + 5;
						glyphNames.add(line.substring(k, line.indexOf(" ;", k)));
					}
				}
				in.close();
			}
		}

		// Hier werden alle notwendigen (und nur diese!) Glyphen in ein eigenes
		// Array gesteckt.
		BufferedReader in = new BufferedReader(new FileReader(new File("docs/glyphlist.txt")));
		PrintStream out = new PrintStream("src/java/com/packenius/library/xspdf/XSUnicodesArray.java");
		out.println("package com.packenius.library.xspdf;");
		out.println();
		out.println("/**");
		out.println(" * Unicode character names codes.");
		out.println(" */");
		out.println("interface XSUnicodesArray {");
		out.println("  /**");
		out.println("   * Array with unicode character names and unicode codec.");
		out.println("   */");
		out.println("  static final String[] unicodes = {");
		String line;
		int count = 0, used = 0;
		while ((line = in.readLine()) != null) {
			// if (count > 0 && count % 1427 == 0) {
			// out.println(" };");
			// out.println(" static final String[] glyphs" + count / 1427 +
			// " = {");
			// }
			line = line.trim();
			if (line.length() > 0 && !line.startsWith("#")) {
				count++;
				System.out.println(line);
				String glyphName = line.substring(0, line.indexOf(';'));
				if (glyphNames.contains(glyphName)) {
					glyphNames.remove(glyphName);
					if (line.startsWith("backslash;")) {
						out.println("    \"backslash\", \"\\\\\",");
					} else if (line.startsWith("controlCR;")) {
						out.println("    \"controlCR\", \"\\r\",");
					} else if (line.startsWith("controlLF;")) {
						out.println("    \"controlLF\", \"\\n\",");
					} else if (line.startsWith("quotedbl;")) {
						out.println("    \"quotedbl\", \"\\\"\",");
					} else {
						int k = line.indexOf(';');
						String codeLine = "    \"" + line.substring(0, k) + "\", \"\\u" + line.substring(k + 1)
								+ "\", ";
						out.println(codeLine);
					}
					used++;
				}
			}
		}
		out.println("  };");
		out.println("}");
		out.close();
		in.close();

		// Unerwünschte restliche Glyphen entfernen.
		for (String glyphName : glyphNames.toArray(new String[0])) {
			if (glyphName.charAt(0) == 'a') {
				try {
					Integer.parseInt(glyphName.substring(1));
					glyphNames.remove(glyphName);
				} catch (Exception e) {
					// Ignore.
				}
			}
		}

		// Ausgabe.
		System.out.println(count + " verschiedene Zeichencodes gefunden.");
		System.out.println(used + " davon übernommen.");
		System.out.println("Nicht gefundene Codes: " + glyphNames.size());
		for (String glyphName : glyphNames) {
			System.out.println(">> " + glyphName);
		}
	}
}
