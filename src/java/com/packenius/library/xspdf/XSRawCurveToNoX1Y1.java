/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * @author Christian Packenius, 2013.
 */
class XSRawCurveToNoX1Y1 implements XSRawContent {
    private double x2, y2, x3, y3;

    XSRawCurveToNoX1Y1(double x2, double y2, double x3, double y3) {
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
    }

    /**
     * @see com.packenius.library.xspdf.XSRawContent#getRawContent(double, double, double, double)
     */
    public String getRawContent(double x, double y, double xFactor, double yFactor) {
        return " " + (x2 * xFactor + x) + " " + (y - y2 * yFactor) + " " + (x3 * xFactor + x) + " " + (y - y3 * yFactor) + " v";
    }
}
