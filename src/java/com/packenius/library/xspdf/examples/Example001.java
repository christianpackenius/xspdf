package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - standard coding
 */
public class Example001 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.print("Hello world");
		xsPDF.createPdf("pdf/Example 001.pdf");
	}
}
