package com.packenius.library.xspdf;

/**
 * Alternative font encoding.
 * 
 * @author Christian Packenius, 2013.
 */
class XST1AltFontEncoding2 extends XSAlternativeFontEncoding {
	/**
	 * Maps an Utf-8 (Unicode?) code (0..65535) to the byte in this font
	 * encoding.
	 */
	static final byte[] altFontEncoding2 = new byte[65536];

	private static final char[] keyValueArray = new char[] { '\u00b1', (char) 127, '\u00a6', (char) 128, '\u00ae',
			(char) 129, '\u011e', (char) 130, '\u0130', (char) 131, '\u2211', (char) 132, '\u00c8', (char) 133,
			'\u0155', (char) 134, '\u014d', (char) 135, '\u0179', (char) 136, '\u017d', (char) 137, '\u2265',
			(char) 138, '\u00d0', (char) 139, '\u00c7', (char) 140, '\u013c', (char) 141, '\u0165', (char) 142,
			'\u0119', (char) 143, '\u0172', (char) 144, '\u00c1', (char) 145, '\u00c4', (char) 146, '\u00e8',
			(char) 147, '\u017a', (char) 148, '\u012f', (char) 149, '\u00d3', (char) 150, '\u00f3', (char) 151,
			'\u0101', (char) 152, '\u015b', (char) 153, '\u00ef', (char) 154, '\u00d4', (char) 155, '\u00d9',
			(char) 156, '\u2206', (char) 157, '\u00fe', (char) 158, '\u00b2', (char) 159, '\u00d6', (char) 160,
			'\u00b5', (char) 176, '\u00ec', (char) 181, '\u0151', (char) 190, '\u0118', (char) 192, '\u0111',
			(char) 201, '\u00be', (char) 204, '\u015e', (char) 209, '\u013e', (char) 210, '\u0136', (char) 211,
			'\u0139', (char) 212, '\u2122', (char) 213, '\u0117', (char) 214, '\u00cc', (char) 215, '\u012a',
			(char) 216, '\u013d', (char) 217, '\u00bd', (char) 218, '\u2264', (char) 219, '\u00f4', (char) 220,
			'\u00f1', (char) 221, '\u0170', (char) 222, '\u00c9', (char) 223, '\u0113', (char) 224, '\u011f',
			(char) 226, '\u00bc', (char) 228, '\u0160', (char) 229, '\u0218', (char) 230, '\u0150', (char) 231,
			'\u00b0', (char) 236, '\u00f2', (char) 237, '\u010c', (char) 238, '\u00f9', (char) 239, '\u221a',
			(char) 240, '\u010e', (char) 242, '\u0157', (char) 243, '\u00d1', (char) 244, '\u00f5', (char) 246,
			'\u0156', (char) 247, };

	private static final String altFontEnc2Diffs = "127 /plusminus /brokenbar /registered /Gbreve /Idotaccent /summation /Egrave /racute /omacron /Zacute /Zcaron"
			+ " /greaterequal /Eth /Ccedilla /lcommaaccent /tcaron /eogonek /Uogonek /Aacute /Adieresis /egrave /zacute"
			+ " /iogonek /Oacute /oacute /amacron /sacute /idieresis /Ocircumflex /Ugrave /Delta /thorn /twosuperior"
			+ " /Odieresis 176 /mu 181 /igrave 190 /ohungarumlaut 192 /Eogonek 201 /dcroat 204 /threequarters 209 /Scedilla"
			+ " /lcaron /Kcommaaccent /Lacute /trademark /edotaccent /Igrave /Imacron /Lcaron /onehalf /lessequal /ocircumflex"
			+ " /ntilde /Uhungarumlaut /Eacute /emacron 226 /gbreve 228 /onequarter /Scaron /Scommaaccent /Ohungarumlaut"
			+ " 236 /degree /ograve /Ccaron /ugrave /radical 242 /Dcaron /rcommaaccent /Ntilde 246 /otilde /Rcommaaccent";

	static {
		for (int i = 0; i < keyValueArray.length; i += 2) {
			altFontEncoding2[keyValueArray[i]] = (byte) keyValueArray[i + 1];
		}
	}

	public static final XST1AltFontEncoding2 instance = new XST1AltFontEncoding2();

	private XST1AltFontEncoding2() {
		// Singleton.
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getPdfDifferencesListContent()
	 */
	@Override
	public String getPdfDifferencesListContent() {
		return altFontEnc2Diffs;
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getID()
	 */
	@Override
	public String getID() {
		return "2";
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getCharacterEncodingMap()
	 */
	@Override
	public byte[] getCharacterEncodingMap() {
		return altFontEncoding2;
	}
}
