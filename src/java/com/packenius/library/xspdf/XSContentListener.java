/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Listener that will be informed every time something happens in the document.
 * @author Christian Packenius, 2013.
 */
public interface XSContentListener {
    /**
     * This method is called every time a new page is started.
     * @param xsPDF Document this call contains to.
     * @param pageNumber The number of the page (the first page is number 1).
     */
    public void newPage(XSPDF xsPDF, int pageNumber);

    /**
     * This method is called every time a new text line is started.
     * @param xsPDF Document this call contains to.
     * @param pageNumber The number of the page (the first page is number 1).
     * @param columnName Internal name of the column.
     * @param lineNumber The number of the text line (the first line on every page is number 1).
     */
    public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber);

    /**
     * This method is called every time a column is activated.
     * @param xsPDF Document this call contains to.
     * @param pageNumber The number of the page (the first page is number 1).
     * @param columnName The internal column name, can be <i>null</i>.
     */
    public void newColumn(XSPDF xsPDF, int pageNumber, String columnName);

    /**
     * This method is called every time before a new text part ist added to a
     * @param xsPDF Document this call contains to.
     * @param pageNumber The number of the page (the first page is number 1).
     * @param columnName The internal column name, can be <i>null</i>.
     * @param textLineID
     * @param textPart
     */
    public void newTextPart(XSPDF xsPDF, int pageNumber, String columnName, int textLineID, String textPart);
}
