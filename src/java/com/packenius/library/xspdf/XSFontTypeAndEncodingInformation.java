/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * @author Christian Packenius, 2013.
 */
class XSFontTypeAndEncodingInformation {
	final XSFontType fontType;

	/**
	 * Font encoding. <i>null</i> is standard encoding.
	 */
	final XSAlternativeFontEncoding fontEncoding;

	/**
	 * Constructor.
	 * 
	 * @param fontType
	 * @param fontEncoding
	 */
	public XSFontTypeAndEncodingInformation(XSFontType fontType, XSAlternativeFontEncoding fontEncoding) {
		this.fontType = fontType;
		this.fontEncoding = fontEncoding;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null || !(o instanceof XSFontTypeAndEncodingInformation)) {
			return false;
		}
		XSFontTypeAndEncodingInformation o2 = (XSFontTypeAndEncodingInformation) o;
		if (!o2.fontType.equals(fontType)) {
			return false;
		}
		if (fontEncoding != null && o2.fontEncoding != null) {
			if (!o2.fontEncoding.equals(fontEncoding)) {
				return false;
			}
		} else if (fontEncoding != null || o2.fontEncoding != null) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// int typeID = fontType.ordinal() << 16;
		// int encID = fontEncoding == null ? 0 :
		// Integer.parseInt(fontEncoding.getID());
		// return typeID | encID;
		return fontType.getFontName().hashCode() ^ fontType.getLogicalFontName().hashCode();
	}
}
