package com.packenius.library.xspdf;

class XST1StdFontCourierOblique extends XST1StdFontCourier {
	@Override
	String getFontName() {
		return "Courier-Oblique";
	}
}
