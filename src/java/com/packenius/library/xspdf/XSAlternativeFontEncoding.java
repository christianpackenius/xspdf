/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * @author Christian Packenius, 2013.
 */
abstract class XSAlternativeFontEncoding {
    abstract String getPdfDifferencesListContent();

    abstract String getID();

    abstract byte[] getCharacterEncodingMap();

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof XSAlternativeFontEncoding)) {
            return false;
        }

        XSAlternativeFontEncoding alt = (XSAlternativeFontEncoding) o;

        if (!getPdfDifferencesListContent().equals(alt.getPdfDifferencesListContent())) {
            return false;
        }
        if (!getID().equals(alt.getID())) {
            return false;
        }

        return true;
    }
}
