/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

class XSType1StdFontCourier extends XSType1StdFont {
    public String getFontName() {
        return "Courier";
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getLogicalFontName()
     */
    public String getLogicalFontName() {
        return "CN";
    }

    @Override
    public double getAscender() {
        return 0.629;
    }

    @Override
    public double getDescender() {
        return -0.157;
    }

    /**
     * List of valid unicode characters in this font.
     */
    private static final char[] validGlyphs = new char[] {'\'', '\\', '\u0020', '\u0021', '\u0022', '\u0023', '\u0024',
        '\u0025', '\u0026', '\u0028', '\u0029', '\u002a', '\u002b', '\u002c', '\u002d', '\u002e', '\u002f', '\u0030', '\u0031',
        '\u0032', '\u0033', '\u0034', '\u0035', '\u0036', '\u0037', '\u0038', '\u0039', '\u003a', '\u003b', '\u003c', '\u003d',
        '\u003e', '\u003f', '\u0040', '\u0041', '\u0042', '\u0043', '\u0044', '\u0045', '\u0046', '\u0047', '\u0048', '\u0049',
        '\u004a', '\u004b', '\u004c', '\u004d', '\u004e', '\u004f', '\u0050', '\u0051', '\u0052', '\u0053', '\u0054', '\u0055',
        '\u0056', '\u0057', '\u0058', '\u0059', '\u005a', '\u005b', '\u005d', '\u005e', '\u005f', '\u0060', '\u0061', '\u0062',
        '\u0063', '\u0064', '\u0065', '\u0066', '\u0067', '\u0068', '\u0069', '\u006a', '\u006b', '\u006c', '\u006d', '\u006e',
        '\u006f', '\u0070', '\u0071', '\u0072', '\u0073', '\u0074', '\u0075', '\u0076', '\u0077', '\u0078', '\u0079', '\u007a',
        '\u007b', '\u007c', '\u007d', '\u007e', '\u00a1', '\u00a2', '\u00a3', '\u00a4', '\u00a5', '\u00a6', '\u00a7', '\u00a8',
        '\u00a9', '\u00aa', '\u00ab', '\u00ac', '\u00ae', '\u00af', '\u00b0', '\u00b1', '\u00b2', '\u00b3', '\u00b4', '\u00b5',
        '\u00b6', '\u00b7', '\u00b8', '\u00b9', '\u00ba', '\u00bb', '\u00bc', '\u00bd', '\u00be', '\u00bf', '\u00c0', '\u00c1',
        '\u00c2', '\u00c3', '\u00c4', '\u00c5', '\u00c6', '\u00c7', '\u00c8', '\u00c9', '\u00ca', '\u00cb', '\u00cc', '\u00cd',
        '\u00ce', '\u00cf', '\u00d0', '\u00d1', '\u00d2', '\u00d3', '\u00d4', '\u00d5', '\u00d6', '\u00d7', '\u00d8', '\u00d9',
        '\u00da', '\u00db', '\u00dc', '\u00dd', '\u00de', '\u00df', '\u00e0', '\u00e1', '\u00e2', '\u00e3', '\u00e4', '\u00e5',
        '\u00e6', '\u00e7', '\u00e8', '\u00e9', '\u00ea', '\u00eb', '\u00ec', '\u00ed', '\u00ee', '\u00ef', '\u00f0', '\u00f1',
        '\u00f2', '\u00f3', '\u00f4', '\u00f5', '\u00f6', '\u00f7', '\u00f8', '\u00f9', '\u00fa', '\u00fb', '\u00fc', '\u00fd',
        '\u00fe', '\u00ff', '\u0100', '\u0101', '\u0102', '\u0103', '\u0104', '\u0105', '\u0106', '\u0107', '\u010c', '\u010d',
        '\u010e', '\u010f', '\u0110', '\u0111', '\u0112', '\u0113', '\u0116', '\u0117', '\u0118', '\u0119', '\u011a', '\u011b',
        '\u011e', '\u011f', '\u0122', '\u0123', '\u012a', '\u012b', '\u012e', '\u012f', '\u0130', '\u0131', '\u0136', '\u0137',
        '\u0139', '\u013a', '\u013b', '\u013c', '\u013d', '\u013e', '\u0141', '\u0142', '\u0143', '\u0144', '\u0145', '\u0146',
        '\u0147', '\u0148', '\u014c', '\u014d', '\u0150', '\u0151', '\u0152', '\u0153', '\u0154', '\u0155', '\u0156', '\u0157',
        '\u0158', '\u0159', '\u015a', '\u015b', '\u015e', '\u015f', '\u0160', '\u0161', '\u0162', '\u0163', '\u0164', '\u0165',
        '\u016a', '\u016b', '\u016e', '\u016f', '\u0170', '\u0171', '\u0172', '\u0173', '\u0178', '\u0179', '\u017a', '\u017b',
        '\u017c', '\u017d', '\u017e', '\u0192', '\u0218', '\u0219', '\u02c6', '\u02c7', '\u02d8', '\u02d9', '\u02da', '\u02db',
        '\u02dc', '\u02dd', '\u2013', '\u2014', '\u2018', '\u2019', '\u201a', '\u201c', '\u201d', '\u201e', '\u2020', '\u2021',
        '\u2022', '\u2026', '\u2030', '\u2039', '\u203a', '\u2044', '\u20ac', '\u2122', '\u2202', '\u2206', '\u2211', '\u2212',
        '\u221a', '\u2260', '\u2264', '\u2265', '\u25ca', '\uf6c3', '\ufb01', '\ufb02',};

    /**
     * Pairwise data: First is the character code, second is the standard font encoding byte within the PDF text stream.
     */
    private static final char[] standardEncodingCodes = new char[] {(char) 100, (char) 100, (char) 101, (char) 101, (char) 102,
        (char) 102, (char) 103, (char) 103, (char) 104, (char) 104, (char) 105, (char) 105, (char) 106, (char) 106, (char) 107,
        (char) 107, (char) 108, (char) 108, (char) 109, (char) 109, (char) 110, (char) 110, (char) 111, (char) 111, (char) 112,
        (char) 112, (char) 113, (char) 113, (char) 114, (char) 114, (char) 115, (char) 115, (char) 116, (char) 116, (char) 117,
        (char) 117, (char) 118, (char) 118, (char) 119, (char) 119, (char) 120, (char) 120, (char) 121, (char) 121, (char) 122,
        (char) 122, (char) 123, (char) 123, (char) 124, (char) 124, (char) 125, (char) 125, (char) 126, (char) 126, (char) 161,
        (char) 161, (char) 162, (char) 162, (char) 163, (char) 163, (char) 164, (char) 168, (char) 165, (char) 165, (char) 167,
        (char) 167, (char) 168, (char) 200, (char) 170, (char) 227, (char) 171, (char) 171, (char) 175, (char) 197, (char) 180,
        (char) 194, (char) 182, (char) 182, (char) 183, (char) 180, (char) 184, (char) 203, (char) 186, (char) 235, (char) 187,
        (char) 187, (char) 191, (char) 191, (char) 198, (char) 225, (char) 216, (char) 233, (char) 223, (char) 251, (char) 230,
        (char) 241, (char) 248, (char) 249, (char) 305, (char) 245, (char) 32, (char) 32, (char) 321, (char) 232, (char) 322,
        (char) 248, (char) 33, (char) 33, (char) 338, (char) 234, (char) 339, (char) 250, (char) 34, (char) 34, (char) 35,
        (char) 35, (char) 36, (char) 36, (char) 37, (char) 37, (char) 38, (char) 38, (char) 39, (char) 169, (char) 40,
        (char) 40, (char) 402, (char) 166, (char) 41, (char) 41, (char) 42, (char) 42, (char) 43, (char) 43, (char) 44,
        (char) 44, (char) 45, (char) 45, (char) 46, (char) 46, (char) 47, (char) 47, (char) 48, (char) 48, (char) 49,
        (char) 49, (char) 50, (char) 50, (char) 51, (char) 51, (char) 52, (char) 52, (char) 53, (char) 53, (char) 54,
        (char) 54, (char) 55, (char) 55, (char) 56, (char) 56, (char) 57, (char) 57, (char) 58, (char) 58, (char) 59,
        (char) 59, (char) 60, (char) 60, (char) 61, (char) 61, (char) 62, (char) 62, (char) 63, (char) 63, (char) 64,
        (char) 64, (char) 64257, (char) 174, (char) 64258, (char) 175, (char) 65, (char) 65, (char) 66, (char) 66, (char) 67,
        (char) 67, (char) 68, (char) 68, (char) 69, (char) 69, (char) 70, (char) 70, (char) 71, (char) 71, (char) 710,
        (char) 195, (char) 711, (char) 207, (char) 72, (char) 72, (char) 728, (char) 198, (char) 729, (char) 199, (char) 73,
        (char) 73, (char) 730, (char) 202, (char) 731, (char) 206, (char) 732, (char) 196, (char) 733, (char) 205, (char) 74,
        (char) 74, (char) 75, (char) 75, (char) 76, (char) 76, (char) 77, (char) 77, (char) 78, (char) 78, (char) 79,
        (char) 79, (char) 80, (char) 80, (char) 81, (char) 81, (char) 82, (char) 82, (char) 8211, (char) 177, (char) 8212,
        (char) 208, (char) 8216, (char) 96, (char) 8217, (char) 39, (char) 8218, (char) 184, (char) 8220, (char) 170,
        (char) 8221, (char) 186, (char) 8222, (char) 185, (char) 8224, (char) 178, (char) 8225, (char) 179, (char) 8226,
        (char) 183, (char) 8230, (char) 188, (char) 8240, (char) 189, (char) 8249, (char) 172, (char) 8250, (char) 173,
        (char) 8260, (char) 164, (char) 83, (char) 83, (char) 84, (char) 84, (char) 85, (char) 85, (char) 86, (char) 86,
        (char) 87, (char) 87, (char) 88, (char) 88, (char) 89, (char) 89, (char) 90, (char) 90, (char) 91, (char) 91,
        (char) 92, (char) 92, (char) 93, (char) 93, (char) 94, (char) 94, (char) 95, (char) 95, (char) 96, (char) 193,
        (char) 97, (char) 97, (char) 98, (char) 98, (char) 99, (char) 99,};

    /**
     * "BOLD" COURIER font.
     */
    private static final XSFontType COURIER_BOLD = new XSType1StdFontCourierBold();

    /**
     * "OBLIQUE" COURIER font.
     */
    private static final XSFontType COURIER_OBLIQUE = new XSType1StdFontCourierOblique();

    /**
     * "BOLD-OBLIQUE" COURIER font.
     */
    private static final XSFontType COURIER_BOLD_OBLIQUE = new XSType1StdFontCourierBoldOblique();

    @Override
    void addCharMetrics() {
        for (char ch : validGlyphs) {
            glyphWidths[ch] = 0.6;
        }
        for (int i = 0; i < standardEncodingCodes.length; i += 2) {
            standardEncodingCodeFromUnicodeCharacter[standardEncodingCodes[i]] = (byte) standardEncodingCodes[i + 1];
        }
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getFontWithParms(int)
     */
    public XSFontType getFontWithParms(int fontParm) {
        switch (fontParm) {
        case 1:
            return XSType1StdFontCourier.COURIER_BOLD;
        case 2:
            return XSType1StdFontCourier.COURIER_OBLIQUE;
        case 3:
            return XSType1StdFontCourier.COURIER_BOLD_OBLIQUE;
        }
        return XS.COURIER;
    }
}
