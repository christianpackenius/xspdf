/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Margin of a page.
 * @author Christian Packenius.
 */
public class XSPageMargin {
    /**
     * Top margin in user units.
     */
    public final double top;

    /**
     * Bottom margin in user units.
     */
    public final double bottom;

    /**
     * Left margin in user units.
     */
    public final double left;

    /**
     * Right margin in user units.
     */
    public final double right;

    /**
     * Horizontal gap between columns.
     */
    public final double columnGapX;

    /**
     * Vertical gap between columns.
     */
    public final double columnGapY;

    /**
     * Constructor.
     * @param top Top margin.
     * @param bottom Bottom margin.
     * @param left Left margin.
     * @param right Right margin.
     * @param columnGapX Horizontal gap between columns.
     * @param columnGapY Vertical gap between columns.
     */
    public XSPageMargin(double top, double bottom, double left, double right, double columnGapX, double columnGapY) {
        if (top < 0 || bottom < 0 || left < 0 || right < 0) {
            throw new XSPdfException("Margin must be positive or zero!");
        }
        if (columnGapX < 0 || columnGapY < 0) {
            throw new XSPdfException("Gaps must be positive or zero!");
        }
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.columnGapX = columnGapX;
        this.columnGapY = columnGapY;
    }

    /**
     * Constructor.
     * @param margin Margin for all sides.
     */
    public XSPageMargin(double margin) {
        this(margin, margin, margin, margin, margin / 2.0, margin / 2.0);
    }

    /**
     * Get sum of left and right margin.
     * @return Sum of left and right margin.
     */
    public double getLeftRightSum() {
        return left + right;
    }

    /**
     * Get sum of top and bottom margin.
     * @return Sum of top and bottom margin.
     */
    public double getTopBottomSum() {
        return top + bottom;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Margin[left=" + left + ",top=" + top + ",right=" + right + ",bottom=" + bottom + ",columnGapX=" + columnGapX
            + ",columnGapY=" + columnGapY;
    }
}
