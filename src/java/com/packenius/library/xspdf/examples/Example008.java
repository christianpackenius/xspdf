package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Text justifying / aligning.
 */
public class Example008 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setPageSize(100, 650);
		xsPDF.setPageMargin(20);
		xsPDF.print("This useless text is left and right aligned.");
		xsPDF.print("The first line is indented and the last line is left aligned only.\n\n");
		xsPDF.setTextParagraphIndentationSpaces(0);
		xsPDF.setLastJustifiedLineRightAligned(true);
		xsPDF.print("This justified text has no intendation in the first line.");
		xsPDF.print("And it is left and right aligned in the last line.\r\r");
		xsPDF.setAlignment(CENTERED);
		xsPDF.print("A centered text example.\r\r");
		xsPDF.setAlignment(LEFT_ALIGNED);
		xsPDF.print("And a left aligned...\r\r");
		xsPDF.setAlignment(RIGHT_ALIGNED);
		xsPDF.print("...and finally a right aligned.");
		xsPDF.createPdf("pdf/Example 008.pdf");
	}
}
