/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Static methods to use.
 * @author Christian Packenius, 2013.
 */
class XSStatics {
    static double convertToUserUnits(XSUnitType unitType, double size) {
        return convertToUserUnits(size, unitType);
    }

    static double convertToUserUnits(double size, XSUnitType unitType) {
        switch (unitType) {
        case userUnits:
            return size;
        case mm:
            return size * 72.0 / 25.4;
        case inch:
            return size * 72.0;
        }
        return size;
    }

    static byte[] concat(byte[]... bas) {
        int fullLength = 0;
        for (byte[] ba : bas) {
            fullLength += ba.length;
        }
        int i = 0;
        byte[] fullBA = new byte[fullLength];
        for (byte[] ba : bas) {
            int baLength = ba.length;
            System.arraycopy(ba, 0, fullBA, i, baLength);
            i += baLength;
        }
        return fullBA;
    }

    /**
     * Escape characters ( \ ) in byte array.
     * @param s String to escape.
     * @return Destination byte array.
     */
    public static byte[] escapeStandardStringCharacters(byte[] s) {
        int adder = 0;
        for (byte b : s) {
            if (b == '\\' || b == '(' || b == ')') {
                adder++;
            }
        }
        if (adder > 0) {
            int baLength = s.length;
            byte[] ba2 = new byte[adder + baLength];
            for (int i = 0, j = 0; i < baLength; i++, j++) {
                byte b = s[i];
                if (b == '\\' || b == '(' || b == ')') {
                    ba2[j++] = '\\';
                }
                ba2[j] = b;
            }
            return ba2;
        }
        // 20150618: OLD!
        // s = s.replace("\\", "\\\\").replace("(", "\\(").replace(")", "\\)");
        return s;
    }

    /**
     * Escape characters ( \ ) in a string.
     * @param s String to escape.
     * @return Destination string.
     */
    public static String escapeStandardStringCharacters(String s) {
        return s.replace("\\", "\\\\").replace("(", "\\(").replace(")", "\\)");
    }
}
