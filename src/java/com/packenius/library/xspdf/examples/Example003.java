package com.packenius.library.xspdf.examples;

import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * "Hello world" - even shorter using <i>use()</i> method
 */
public class Example003 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF.getInstance().use("Hello world", new File("pdf/Example 003.pdf"));
	}
}
