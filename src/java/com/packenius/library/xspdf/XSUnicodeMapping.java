/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.HashMap;
import java.util.Map;

/**
 * Mapping between unicode character and character name.
 * @author Christian Packenius, 2013.
 */
public class XSUnicodeMapping implements XSUnicodesArray, XSUnicodeZapfDingbats {
    /**
     * Maps from Unicode to Adobe glyph fontType.
     */
    private static final Map<Character, String> mapCode2Name = new HashMap<Character, String>();

    /**
     * Maps from Adobe glyph fontType to Unicode.
     */
    private static final Map<String, Character> mapName2Code = new HashMap<String, Character>();

    static {
        // Create "usually" character mappings.
        for (int i = 0; i < unicodes.length; i += 2) {
            String name = unicodes[i];
            char code = unicodes[i + 1].charAt(0);
            mapCode2Name.put(code, name);
            mapName2Code.put(name, code);
        }

        // Create additional ZapfDingbats character mappings.
        for (int i = 0; i < zapfDingbatsCodes.length; i += 2) {
            String name = zapfDingbatsCodes[i];
            char code = zapfDingbatsCodes[i + 1].charAt(0);
            if (mapCode2Name.get(code) == null) {
                mapCode2Name.put(code, name);
            }
            mapName2Code.put(name, code);
        }
    }

    /**
     * Returns the character name of a unicode character.
     * @param unicodeCode
     * @return Character name.
     */
    public static String getNameFromCode(char unicodeCode) {
        return mapCode2Name.get(unicodeCode);
    }

    /**
     * Returns the character name of a unicode character.
     * @param characterName Name of the character.
     * @return Character name.
     */
    public static Character getCodeFromName(String characterName) {
        return mapName2Code.get(characterName);
    }
}
