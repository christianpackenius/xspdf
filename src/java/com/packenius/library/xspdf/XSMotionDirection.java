/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Direction of motion for transition styles SPLIT, BOX and FLY.
 */
public enum XSMotionDirection implements XSPdfContent {
    /**
     * Inward direction motion.
     */
    Inward,

    /**
     * Outward direction motion.
     */
    Outward;

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        switch (this) {
        case Inward:
            return ""; // Default.
        case Outward:
            return "/M/O";
        }
        throw new XSPdfException("Impossible getPdfContent()!");
    }
}
