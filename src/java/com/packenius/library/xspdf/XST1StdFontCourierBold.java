package com.packenius.library.xspdf;

class XST1StdFontCourierBold extends XST1StdFontCourier {
	@Override
	String getFontName() {
		return "Courier-Bold";
	}
}
