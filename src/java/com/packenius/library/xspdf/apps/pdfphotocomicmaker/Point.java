/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

/**
 * @author Christian Packenius, 2013.
 */
public class Point {
    /**
     * X coordinate.
     */
    public double x;

    /**
     * Y coordinate.
     */
    public double y;

    /**
     * Constructor.
     */
    public Point() {
        x = y = 0;
    }

    /**
     * Constructor.
     * @param x X coordinate.
     * @param y Y coordinate.
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

}
