package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Different page formats in a single document.
 */
public class Example006 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setPageSize(LETTER);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.setPageSize(DIN_B0);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.setUnitType(INCH);
		xsPDF.setPageSize(10, 10);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.setPageSize(DIN_C9);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.createPdf("pdf/Example 006.pdf");
	}
}
