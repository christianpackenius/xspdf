/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Alternative font encoding - only for SYMBOL font with 'apple' character usage.
 * @author Christian Packenius, 2013.
 */
class XSType1AltFontEncoding4 extends XSAlternativeFontEncoding {
    /**
     * Maps an Utf-8 (Unicode?) code (0..65535) to the byte in this font encoding.
     */
    static final byte[] altFontEncoding4 = new byte[65536];

    private static final String altFontEnc4Diffs = "255 /apple";

    static {
        altFontEncoding4['\uf8ff'] = (byte) 255;
    }

    public static final XSType1AltFontEncoding4 instance = new XSType1AltFontEncoding4();

    private XSType1AltFontEncoding4() {
        // Singleton.
    }

    /**
     * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getPdfDifferencesListContent()
     */
    @Override
    public String getPdfDifferencesListContent() {
        return altFontEnc4Diffs;
    }

    /**
     * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getID()
     */
    @Override
    public String getID() {
        return "4";
    }

    /**
     * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getCharacterEncodingMap()
     */
    @Override
    public byte[] getCharacterEncodingMap() {
        return altFontEncoding4;
    }
}
