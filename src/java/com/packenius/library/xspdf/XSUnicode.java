package com.packenius.library.xspdf;

/**
 * Unicode character names codes.
 */
interface XSUnicode {
	/**
	 * Array with unicode character names and unicode codec.
	 */
	static final String[] unicodes = { "A", "\u0041", "AE", "\u00C6", "Aacute", "\u00C1", "Abreve", "\u0102",
			"Acircumflex", "\u00C2", "Adieresis", "\u00C4", "Agrave", "\u00C0", "Alpha", "\u0391", "Amacron", "\u0100",
			"Aogonek", "\u0104", "Aring", "\u00C5", "Atilde", "\u00C3", "B", "\u0042", "Beta", "\u0392", "C", "\u0043",
			"Cacute", "\u0106", "Ccaron", "\u010C", "Ccedilla", "\u00C7", "Chi", "\u03A7", "D", "\u0044", "Dcaron",
			"\u010E", "Dcroat", "\u0110", "Delta", "\u2206", "E", "\u0045", "Eacute", "\u00C9", "Ecaron", "\u011A",
			"Ecircumflex", "\u00CA", "Edieresis", "\u00CB", "Edotaccent", "\u0116", "Egrave", "\u00C8", "Emacron",
			"\u0112", "Eogonek", "\u0118", "Epsilon", "\u0395", "Eta", "\u0397", "Eth", "\u00D0", "Euro", "\u20AC", "F",
			"\u0046", "G", "\u0047", "Gamma", "\u0393", "Gbreve", "\u011E", "Gcommaaccent", "\u0122", "H", "\u0048",
			"I", "\u0049", "Iacute", "\u00CD", "Icircumflex", "\u00CE", "Idieresis", "\u00CF", "Idotaccent", "\u0130",
			"Ifraktur", "\u2111", "Igrave", "\u00CC", "Imacron", "\u012A", "Iogonek", "\u012E", "Iota", "\u0399", "J",
			"\u004A", "K", "\u004B", "Kappa", "\u039A", "Kcommaaccent", "\u0136", "L", "\u004C", "Lacute", "\u0139",
			"Lambda", "\u039B", "Lcaron", "\u013D", "Lcommaaccent", "\u013B", "Lslash", "\u0141", "M", "\u004D", "Mu",
			"\u039C", "N", "\u004E", "Nacute", "\u0143", "Ncaron", "\u0147", "Ncommaaccent", "\u0145", "Ntilde",
			"\u00D1", "Nu", "\u039D", "O", "\u004F", "OE", "\u0152", "Oacute", "\u00D3", "Ocircumflex", "\u00D4",
			"Odieresis", "\u00D6", "Ograve", "\u00D2", "Ohungarumlaut", "\u0150", "Omacron", "\u014C", "Omega",
			"\u2126", "Omicron", "\u039F", "Oslash", "\u00D8", "Otilde", "\u00D5", "P", "\u0050", "Phi", "\u03A6", "Pi",
			"\u03A0", "Psi", "\u03A8", "Q", "\u0051", "R", "\u0052", "Racute", "\u0154", "Rcaron", "\u0158",
			"Rcommaaccent", "\u0156", "Rfraktur", "\u211C", "Rho", "\u03A1", "S", "\u0053", "Sacute", "\u015A",
			"Scaron", "\u0160", "Scedilla", "\u015E", "Scommaaccent", "\u0218", "Sigma", "\u03A3", "T", "\u0054", "Tau",
			"\u03A4", "Tcaron", "\u0164", "Tcommaaccent", "\u0162", "Theta", "\u0398", "Thorn", "\u00DE", "U", "\u0055",
			"Uacute", "\u00DA", "Ucircumflex", "\u00DB", "Udieresis", "\u00DC", "Ugrave", "\u00D9", "Uhungarumlaut",
			"\u0170", "Umacron", "\u016A", "Uogonek", "\u0172", "Upsilon", "\u03A5", "Upsilon1", "\u03D2", "Uring",
			"\u016E", "V", "\u0056", "W", "\u0057", "X", "\u0058", "Xi", "\u039E", "Y", "\u0059", "Yacute", "\u00DD",
			"Ydieresis", "\u0178", "Z", "\u005A", "Zacute", "\u0179", "Zcaron", "\u017D", "Zdotaccent", "\u017B",
			"Zeta", "\u0396", "a", "\u0061", "aacute", "\u00E1", "abreve", "\u0103", "acircumflex", "\u00E2", "acute",
			"\u00B4", "adieresis", "\u00E4", "ae", "\u00E6", "agrave", "\u00E0", "aleph", "\u2135", "alpha", "\u03B1",
			"amacron", "\u0101", "ampersand", "\u0026", "angle", "\u2220", "angleleft", "\u2329", "angleright",
			"\u232A", "aogonek", "\u0105", "apple", "\uF8FF", "approxequal", "\u2248", "aring", "\u00E5", "arrowboth",
			"\u2194", "arrowdblboth", "\u21D4", "arrowdbldown", "\u21D3", "arrowdblleft", "\u21D0", "arrowdblright",
			"\u21D2", "arrowdblup", "\u21D1", "arrowdown", "\u2193", "arrowhorizex", "\uF8E7", "arrowleft", "\u2190",
			"arrowright", "\u2192", "arrowup", "\u2191", "arrowvertex", "\uF8E6", "asciicircum", "\u005E", "asciitilde",
			"\u007E", "asterisk", "\u002A", "asteriskmath", "\u2217", "at", "\u0040", "atilde", "\u00E3", "b", "\u0062",
			"backslash", "\\", "bar", "\u007C", "beta", "\u03B2", "braceex", "\uF8F4", "braceleft", "\u007B",
			"braceleftbt", "\uF8F3", "braceleftmid", "\uF8F2", "bracelefttp", "\uF8F1", "braceright", "\u007D",
			"bracerightbt", "\uF8FE", "bracerightmid", "\uF8FD", "bracerighttp", "\uF8FC", "bracketleft", "\u005B",
			"bracketleftbt", "\uF8F0", "bracketleftex", "\uF8EF", "bracketlefttp", "\uF8EE", "bracketright", "\u005D",
			"bracketrightbt", "\uF8FB", "bracketrightex", "\uF8FA", "bracketrighttp", "\uF8F9", "breve", "\u02D8",
			"brokenbar", "\u00A6", "bullet", "\u2022", "c", "\u0063", "cacute", "\u0107", "caron", "\u02C7",
			"carriagereturn", "\u21B5", "ccaron", "\u010D", "ccedilla", "\u00E7", "cedilla", "\u00B8", "cent", "\u00A2",
			"chi", "\u03C7", "circlemultiply", "\u2297", "circleplus", "\u2295", "circumflex", "\u02C6", "club",
			"\u2663", "colon", "\u003A", "comma", "\u002C", "commaaccent", "\uF6C3", "congruent", "\u2245", "copyright",
			"\u00A9", "copyrightsans", "\uF8E9", "copyrightserif", "\uF6D9", "currency", "\u00A4", "d", "\u0064",
			"dagger", "\u2020", "daggerdbl", "\u2021", "dcaron", "\u010F", "dcroat", "\u0111", "degree", "\u00B0",
			"delta", "\u03B4", "diamond", "\u2666", "dieresis", "\u00A8", "divide", "\u00F7", "dollar", "\u0024",
			"dotaccent", "\u02D9", "dotlessi", "\u0131", "dotmath", "\u22C5", "e", "\u0065", "eacute", "\u00E9",
			"ecaron", "\u011B", "ecircumflex", "\u00EA", "edieresis", "\u00EB", "edotaccent", "\u0117", "egrave",
			"\u00E8", "eight", "\u0038", "element", "\u2208", "ellipsis", "\u2026", "emacron", "\u0113", "emdash",
			"\u2014", "emptyset", "\u2205", "endash", "\u2013", "eogonek", "\u0119", "epsilon", "\u03B5", "equal",
			"\u003D", "equivalence", "\u2261", "eta", "\u03B7", "eth", "\u00F0", "exclam", "\u0021", "exclamdown",
			"\u00A1", "existential", "\u2203", "f", "\u0066", "fi", "\uFB01", "five", "\u0035", "fl", "\uFB02",
			"florin", "\u0192", "four", "\u0034", "fraction", "\u2044", "g", "\u0067", "gamma", "\u03B3", "gbreve",
			"\u011F", "gcommaaccent", "\u0123", "germandbls", "\u00DF", "gradient", "\u2207", "grave", "\u0060",
			"greater", "\u003E", "greaterequal", "\u2265", "guillemotleft", "\u00AB", "guillemotright", "\u00BB",
			"guilsinglleft", "\u2039", "guilsinglright", "\u203A", "h", "\u0068", "heart", "\u2665", "hungarumlaut",
			"\u02DD", "hyphen", "\u002D", "i", "\u0069", "iacute", "\u00ED", "icircumflex", "\u00EE", "idieresis",
			"\u00EF", "igrave", "\u00EC", "imacron", "\u012B", "infinity", "\u221E", "integral", "\u222B", "integralbt",
			"\u2321", "integralex", "\uF8F5", "integraltp", "\u2320", "intersection", "\u2229", "iogonek", "\u012F",
			"iota", "\u03B9", "j", "\u006A", "k", "\u006B", "kappa", "\u03BA", "kcommaaccent", "\u0137", "l", "\u006C",
			"lacute", "\u013A", "lambda", "\u03BB", "lcaron", "\u013E", "lcommaaccent", "\u013C", "less", "\u003C",
			"lessequal", "\u2264", "logicaland", "\u2227", "logicalnot", "\u00AC", "logicalor", "\u2228", "lozenge",
			"\u25CA", "lslash", "\u0142", "m", "\u006D", "macron", "\u00AF", "minus", "\u2212", "minute", "\u2032",
			"mu", "\u00B5", "multiply", "\u00D7", "n", "\u006E", "nacute", "\u0144", "ncaron", "\u0148", "ncommaaccent",
			"\u0146", "nine", "\u0039", "notelement", "\u2209", "notequal", "\u2260", "notsubset", "\u2284", "ntilde",
			"\u00F1", "nu", "\u03BD", "numbersign", "\u0023", "o", "\u006F", "oacute", "\u00F3", "ocircumflex",
			"\u00F4", "odieresis", "\u00F6", "oe", "\u0153", "ogonek", "\u02DB", "ograve", "\u00F2", "ohungarumlaut",
			"\u0151", "omacron", "\u014D", "omega", "\u03C9", "omega1", "\u03D6", "omicron", "\u03BF", "one", "\u0031",
			"onehalf", "\u00BD", "onequarter", "\u00BC", "onesuperior", "\u00B9", "ordfeminine", "\u00AA",
			"ordmasculine", "\u00BA", "oslash", "\u00F8", "otilde", "\u00F5", "p", "\u0070", "paragraph", "\u00B6",
			"parenleft", "\u0028", "parenleftbt", "\uF8ED", "parenleftex", "\uF8EC", "parenlefttp", "\uF8EB",
			"parenright", "\u0029", "parenrightbt", "\uF8F8", "parenrightex", "\uF8F7", "parenrighttp", "\uF8F6",
			"partialdiff", "\u2202", "percent", "\u0025", "period", "\u002E", "periodcentered", "\u00B7",
			"perpendicular", "\u22A5", "perthousand", "\u2030", "phi", "\u03C6", "phi1", "\u03D5", "pi", "\u03C0",
			"plus", "\u002B", "plusminus", "\u00B1", "product", "\u220F", "propersubset", "\u2282", "propersuperset",
			"\u2283", "proportional", "\u221D", "psi", "\u03C8", "q", "\u0071", "question", "\u003F", "questiondown",
			"\u00BF", "quotedbl", "\"", "quotedblbase", "\u201E", "quotedblleft", "\u201C", "quotedblright", "\u201D",
			"quoteleft", "\u2018", "quoteright", "\u2019", "quotesinglbase", "\u201A", "quotesingle", "\u0027", "r",
			"\u0072", "racute", "\u0155", "radical", "\u221A", "radicalex", "\uF8E5", "rcaron", "\u0159",
			"rcommaaccent", "\u0157", "reflexsubset", "\u2286", "reflexsuperset", "\u2287", "registered", "\u00AE",
			"registersans", "\uF8E8", "registerserif", "\uF6DA", "rho", "\u03C1", "ring", "\u02DA", "s", "\u0073",
			"sacute", "\u015B", "scaron", "\u0161", "scedilla", "\u015F", "scommaaccent", "\u0219", "second", "\u2033",
			"section", "\u00A7", "semicolon", "\u003B", "seven", "\u0037", "sigma", "\u03C3", "sigma1", "\u03C2",
			"similar", "\u223C", "six", "\u0036", "slash", "\u002F", "space", "\u0020", "spade", "\u2660", "sterling",
			"\u00A3", "suchthat", "\u220B", "summation", "\u2211", "t", "\u0074", "tau", "\u03C4", "tcaron", "\u0165",
			"tcommaaccent", "\u0163", "therefore", "\u2234", "theta", "\u03B8", "theta1", "\u03D1", "thorn", "\u00FE",
			"three", "\u0033", "threequarters", "\u00BE", "threesuperior", "\u00B3", "tilde", "\u02DC", "trademark",
			"\u2122", "trademarksans", "\uF8EA", "trademarkserif", "\uF6DB", "two", "\u0032", "twosuperior", "\u00B2",
			"u", "\u0075", "uacute", "\u00FA", "ucircumflex", "\u00FB", "udieresis", "\u00FC", "ugrave", "\u00F9",
			"uhungarumlaut", "\u0171", "umacron", "\u016B", "underscore", "\u005F", "union", "\u222A", "universal",
			"\u2200", "uogonek", "\u0173", "upsilon", "\u03C5", "uring", "\u016F", "v", "\u0076", "w", "\u0077",
			"weierstrass", "\u2118", "x", "\u0078", "xi", "\u03BE", "y", "\u0079", "yacute", "\u00FD", "ydieresis",
			"\u00FF", "yen", "\u00A5", "z", "\u007A", "zacute", "\u017A", "zcaron", "\u017E", "zdotaccent", "\u017C",
			"zero", "\u0030", "zeta", "\u03B6", };
}
