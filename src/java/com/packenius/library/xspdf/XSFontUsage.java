/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.awt.Color;

/**
 * Holding font data. Only for internal usage!!
 * @author Christian Packenius, 2013.
 */
class XSFontUsage {
    final XSFontType fontType;

    final double fontSize;

    final double spaceWidth;

    final XSAlignment alignment;

    final Color textFillColor;

    final Color textStrokeColor;

    final XSTextRenderingMode renderMode;

    final XSTextFormattingMode formattingMode;

    XSFontUsage(XSFontType fontType, double fontSize, XSAlignment alignment, Color textFillColor, Color textStrokeColor,
            XSTextRenderingMode renderMode, XSTextFormattingMode formattingMode) {
        this.fontType = fontType;
        this.fontSize = fontSize;
        this.alignment = alignment;
        this.textFillColor = textFillColor;
        this.textStrokeColor = textStrokeColor;
        this.renderMode = renderMode;
        this.formattingMode = formattingMode;
        spaceWidth = getCharacterWidth(' ');
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object object) {
        if (object == null || !(object instanceof XSFontUsage)) {
            return false;
        }
        XSFontUsage fontUsage = (XSFontUsage) object;
        return fontUsage.fontType.equals(fontType) && fontUsage.fontSize == fontSize && fontUsage.alignment == alignment
            && fontUsage.textFillColor.equals(textFillColor) && fontUsage.textStrokeColor.equals(textStrokeColor)
            && fontUsage.renderMode == renderMode && fontUsage.formattingMode == formattingMode;
    }

    double getTextPartWidth(String part) {
        double width = 0.0;
        for (char ch : part.toCharArray()) {
            width += getCharacterWidth(ch);
        }
        return width;
    }

    private double getCharacterWidth(char code) {
        // XSType1StdFont fontClass = fontType.fontClass;

        // Character charCode =
        // font.standardEncodingCodeFromUnicodeCharacter.get(code);
        // if (charCode == null) {
        // throw new XSPdfException("Glyph <" +
        // XSUnicodeMapping.getNameFromCode(code) +
        // "> not in standard encoding for code 0x"
        // + Integer.toHexString(code) + " in font " + fontType.fontName + "!");
        // }

        double[] glyphWidths = fontType.getGlyphWidths();
        double glyphWidth = glyphWidths[code];
        if (glyphWidth == 0) {
            throw new XSPdfException("Glyph <" + XSUnicodeMapping.getNameFromCode(code) + "> with code 0x"
                + Integer.toHexString(code) + " not found in font " + fontType.getFontName() + "!");
        }
        return glyphWidth * fontSize;
    }
}
