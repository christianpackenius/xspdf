/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * A "self-made" Type3 font. See chapter 5.5.4 and 4.1 of the PDF reference (sixth edition).
 * @author Christian Packenius, 2015.
 */
public class XSType3Font implements XSFontType {
    /**
     * Anzahl Type3-Fonts.
     */
    private static int t3Count = 0;

    /**
     * Name of the font.
     */
    public final String name;

    private final XSType3Glyph[] glyphs = new XSType3Glyph[65536];

    private final double[] glyphWidths = new double[65536];

    private final byte[] standardEncoding = new byte[65536];

    private final char[] encoding2unicode = new char[256];

    /**
     * Full bounding box (of all glyphs of this font).
     */
    private int bboxLeft = Integer.MAX_VALUE, bboxRight = Integer.MIN_VALUE, bboxTop = Integer.MIN_VALUE,
            bboxBottom = Integer.MAX_VALUE;

    private final int t3ID = ++t3Count;

    /**
     * Konstruktor.
     * @param name
     */
    public XSType3Font(String name) {
        this.name = name;
    }

    /**
     * Adds a new glyph to this Type3 font.
     * @param c Character.
     * @param t3glyph Glyph to set for this character.
     */
    public void add(char c, XSType3Glyph t3glyph) {
        if (t3glyph == null) {
            throw new XSPdfException("Glyph of Type3 font must not be null!");
        }
        if (c < 32 || c > 255) {
            throw new XSPdfException("The glyph with the character #" + (int) c
                + " cannot be standard encoded! Please add encoding param.");
        }
        if (glyphs[c] != null) {
            throw new XSPdfException("The glyph with the character #" + (int) c + " is already defined!");
        }
        if (encoding2unicode[c] != 0) {
            throw new XSPdfException("The encoding #" + (int) c + " is already used!");
        }
        glyphs[c] = t3glyph;
        glyphWidths[c] = t3glyph.width / 1000.0;
        standardEncoding[c] = (byte) c;
        encoding2unicode[c] = c;
        setBBox(t3glyph);
    }

    /**
     * Adds a new glyph to this Type3 font.
     * @param c Character.
     * @param t3glyph Glyph to set for this character.
     * @param encoding Byte value to use for this font.
     */
    public void add(char c, XSType3Glyph t3glyph, byte encoding) {
        if (t3glyph == null) {
            throw new XSPdfException("Glyph of Type3 font must not be null!");
        }
        if (glyphs[c] != null) {
            throw new XSPdfException("The glyph with the character #" + (int) c + " is already defined!");
        }
        if (encoding2unicode[encoding & 255] != 0) {
            throw new XSPdfException("The encoding #" + (encoding & 255) + " is already used!");
        }
        glyphs[c] = t3glyph;
        glyphWidths[c] = t3glyph.width / 1000.0;
        standardEncoding[c] = encoding;
        encoding2unicode[encoding & 255] = c;
        setBBox(t3glyph);
    }

    private void setBBox(XSType3Glyph t3glyph) {
        bboxLeft = Math.min(bboxLeft, t3glyph.left);
        bboxBottom = Math.min(bboxBottom, t3glyph.bottom);
        bboxRight = Math.max(bboxRight, t3glyph.right);
        bboxTop = Math.max(bboxTop, t3glyph.top);
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getFontName()
     */
    public String getFontName() {
        return name;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getLogicalFontName()
     */
    public String getLogicalFontName() {
        return "T3f" + t3ID;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getGlyphWidths()
     */
    public double[] getGlyphWidths() {
        return glyphWidths;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getStandardEncodingCodeFromUnicodeCharacter()
     */
    public byte[] getStandardEncodingCodeFromUnicodeCharacter() {
        return standardEncoding;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getAlternativeFontEncoding(char)
     */
    public XSAlternativeFontEncoding getAlternativeFontEncoding(char ch) {
        throw new RuntimeException("Not (yet) supported!");
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getFontWithParms(int)
     */
    public XSFontType getFontWithParms(int fontParm) {
        return this;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getAscender()
     */
    public double getAscender() {
        return 1.0;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getDescender()
     */
    public double getDescender() {
        return 0.0;
    }

    /**
     * Returns the bounding box of all glyphs of this font.
     * @return Font's bounding box.
     */
    public String getFontBBoxAsVectorString() {
        return "[" + bboxLeft + " " + bboxBottom + " " + bboxRight + " " + bboxTop + "]";
    }

    /**
     * Returns the widths of all glyphs within this font as an array in a string.
     * @return Widths array of glyphs as string.
     */
    public String getGlyphWidthsAsVectorString() {
        int k1 = getFirstCharacterCode();
        int k2 = getLastCharacterCode();
        StringBuilder s = new StringBuilder((k2 - k1 + 2) * 3);
        s.append("[");
        for (int k = k1; k <= k2; k++) {
            if (k > k1) {
                s.append(' ');
            }
            XSType3Glyph glyph = glyphs[encoding2unicode[k]];
            if (glyph != null) {
                s.append(glyph.width);
            } else {
                s.append('0');
            }
        }
        s.append(']');
        return s.toString();
    }

    /**
     * Returns the first used character code.
     * @return First char code.
     */
    public int getFirstCharacterCode() {
        for (int i = 0; i < 256; i++) {
            if (encoding2unicode[i] != 0) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the last used character code.
     * @return Last char code.
     */
    public int getLastCharacterCode() {
        for (int i = 255; i >= 0; i--) {
            if (encoding2unicode[i] != 0) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns the wanted glyph object.
     * @param k Character code of the wanted glyph.
     * @return Glyph or <i>null</i> if not defined.
     */
    public XSType3Glyph getGlyphFromCharCode(int k) {
        if (k < 0 || k > 255) {
            throw new XSPdfException("Char code may only between 0 and 255!");
        }
        return glyphs[k & 255];
    }
}
