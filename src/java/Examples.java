/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentEncoding;
import com.packenius.library.xspdf.XSContentListener;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSDimension;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSUnicodeMapping;

/**
 * Examples for using xsPDF.
 * @author Christian Packenius, 2013.
 */
public class Examples implements XS {
    private static final File tempDir = new File("temp");

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        removeTemporaryFiles();
        tempDir.mkdirs();
        examples();
    }

    private static void removeTemporaryFiles() {
        File[] filelist = tempDir.listFiles();
        if (filelist != null) {
            for (File file : filelist) {
                file.delete();
            }
        }
    }

    /**
     * Create a PDF file with a single DIN A4 page without any content.
     */
    private static void examples() throws IOException {
        example00();
        example01();
        example02();
        example03();
        example04();
        example05();
        example06();
        example07();
        example08();
        example09();
        example10();
        example11();
        example12();
        example13();
        example14();
        example15_16();
        example17();
        example18();
        example19();
        example20();
        example21();
        example22();
        example23();
        example24();
        example25();
        example26();
        example27();
        example28();
        example29();
        example30();
        example31();
        example32();
        example33();
        example34();
        example35();
        example36();
        example37();
    }

    private static void example00() throws IOException {
        // Use fluid API - if you want.
        XSPDF.getInstance().use(NO_ENCODING).print("Hello world!").createPdf(tempDir.getName() + "/test 00 - HelloWorld.pdf");

        XSPDF xsPDF = new XSPDF();
        xsPDF.setFont(TIMES, 18.0, ITALIC);
        xsPDF.use(NO_INDENTATION);
        xsPDF.setContentEncoding(NO_ENCODING);
        xsPDF.setPageSize(DIN_C9);
        xsPDF.use(ROTATE);
        xsPDF.print("Hello world!");
        xsPDF.createPdf(new File(tempDir, "test 00a - HelloWorld.pdf"));

        XSPDF.getInstance().setFont(TIMES, 18.0, ITALIC).use(NO_INDENTATION).setContentEncoding(NO_ENCODING)
            .setPageSize(DIN_C9).use(ROTATE).print("Hello world!").createPdf(new File(tempDir, "test 00b - HelloWorld.pdf"));

        XSPDF.getInstance().use(TIMES, 18, NO_INDENTATION, ITALIC, NO_ENCODING, DIN_C9, ROTATE, "Hello world!")
            .createPdf(new File(tempDir, "test 00c - HelloWorld.pdf"));

        XSPDF.getInstance().use(TIMES, 18, NO_INDENTATION, ITALIC, NO_ENCODING, DIN_C9, ROTATE, "Hello world!",
            new File(tempDir, "test 00d - HelloWorld.pdf"));
    }

    private static void example01() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.newPage();
        xsPDF.createPdf(new File(tempDir, "test 01 - single empty page.pdf"));
    }

    private static void example02() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.newPage();
        xsPDF.newPage();
        xsPDF.createPdf(new File(tempDir, "test 02 - two empty pages.pdf"));
    }

    private static void example03() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("First words");
        xsPDF.createPdf(new File(tempDir, "test 03 - little text.pdf"));
    }

    private static void example04() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("First words");
        xsPDF.print("on a single line.");
        xsPDF.createPdf(new File(tempDir, "test 04 - little text.pdf"));
    }

    private static void example05() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("First words");
        xsPDF.print("on a single line.");
        xsPDF.print("I hope that these words will be enough to enforce a word wrapping to the next line!");
        xsPDF.createPdf(new File(tempDir, "test 05 - word wrapping.pdf"));
    }

    private static void example06() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("First words");
        xsPDF.print("on a single line.");
        xsPDF.print("\r\r");
        xsPDF.print("More text [1] with an empty line before.");
        xsPDF.print("\n\n");
        xsPDF.print("More text [2] with an empty line before.");
        xsPDF.print("\r\n\r\n");
        xsPDF.print("More text [3] with an empty line before.");
        xsPDF.print("\r\n\r\n");
        xsPDF.print("Single");
        xsPDF.print("\r\n\r\n");
        xsPDF.createPdf(new File(tempDir, "test 06 - empty lines.pdf"));
    }

    private static void example07() throws IOException {
        // Showing left justify.
        XSPDF.getInstance().print("Some-quite-long-words but-they-are-not-real-words.")
            .createPdf(new File(tempDir, "test 07 - long words.pdf"));
    }

    private static void example08() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("\n\nAutomatic paging after many words:\r");
        for (int i = 0; i < 1000; i++) {
            xsPDF.print("word-" + i);
        }
        xsPDF.createPdf(new File(tempDir, "test 08 - automatic paging.pdf"));
    }

    private static void example09() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.print("The next page will have no borders.");
        xsPDF.newPage();
        xsPDF.setContentEncoding(NO_ENCODING);
        xsPDF.setTextParagraphIndentationSpaces(0);
        XSDimension currentPageSize = xsPDF.getPageSize();
        double width = currentPageSize.width;
        double height = currentPageSize.height;
        xsPDF.addColumn(0, 0, width, height, null, 0);
        String textAZ = "abcd01234jklmnopqrstuvwxyzABCDE56789KLMNOPQRSTUVWXYZ";
        for (int i = 0; i < 500; i++) {
            xsPDF.use(USER_UNITS, COURIER, 6.0 + 2 * (i / 100));
            int textAzLength = textAZ.length();
            int m = i / textAzLength + 1;
            int k = i % textAzLength;
            xsPDF.print(textAZ.substring(k, Math.min(k + m, textAzLength)));
        }
        xsPDF.createPdf(new File(tempDir, "test 09 - not borders on last page.pdf"));
    }

    private static void example10() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        xsPDF.use(USER_UNITS, TIMES, 8.0);
        xsPDF.print("There is no problem [8, ordinary]");
        xsPDF.use(USER_UNITS, COURIER, 12.0);
        xsPDF.print("to change the font [12, ordinary]");
        xsPDF.use(USER_UNITS, HELVETICA, BOLD, 9.0);
        xsPDF.print("or font size in a single line. [9, bold]");
        xsPDF.print("\r");
        xsPDF.use(USER_UNITS, TIMES, ORDINARY, 36.0);
        xsPDF.print("Use a very big font [36, ordinary]");
        xsPDF.use(USER_UNITS, COURIER, 6.0, ITALIC);
        xsPDF.print("or a small one. [6, italic]");
        xsPDF.print("\r");
        xsPDF.use(USER_UNITS, HELVETICA, 14.0);
        xsPDF.print("Have a try.");
        xsPDF.print("\rJust-a-single-word! [14, ordinary]");
        xsPDF.createPdf(new File(tempDir, "test 10 - many fonts and font sizes in one single text line.pdf"));
    }

    private static void example11() throws IOException {
        XSPDF xsPDF;
        // Line leading examples.
        xsPDF = XSPDF.getInstance();
        // xsPDF.setPageMargin(NO_MARGIN);
        xsPDF.print("This is the first line.");
        // xsPDF.setLineLeading(50, PERCENT);
        xsPDF.print("\rThe second line has 50% line leading. This value is default.");
        xsPDF.setLineLeadingInPercent(200);
        xsPDF.print("\rThe third line has 200% line leading.");
        xsPDF.setLineLeadingInPercent(0);
        xsPDF.print("\rThe forth line has zero line leading.");
        xsPDF.setUnitType(USER_UNITS);
        xsPDF.setLineLeading(4.2);
        xsPDF.print("\rThe fifth line has 4.2 user units line leading.");
        xsPDF.setUnitType(MM);
        xsPDF.setLineLeading(10);
        xsPDF.print("\rThe sixth line has 10 mm line leading.");
        xsPDF.setUnitType(INCH);
        xsPDF.setLineLeading(1);
        xsPDF.print("\rThe seventh line has one inch line leading.");
        xsPDF.setLineLeadingInPercent(100);
        xsPDF.print("\rThe eightth line has 100% inch line leading.");
        xsPDF.createPdf(new File(tempDir, "test 11 - change line leading.pdf"));
    }

    private static void example12() throws IOException {
        XSPDF xsPDF;
        // Landscape page size.
        xsPDF = XSPDF.getInstance();
        xsPDF.use(DIN_A4, ROTATE);
        xsPDF.print("This is a stupidly printed text without any real content or functionality.");
        xsPDF.print("I hope it is long enough for word wrapping even in DIN A4 landscape mode.");
        xsPDF.createPdf(new File(tempDir, "test 12 - landscape page orientation.pdf"));
    }

    private static void example13() throws FileNotFoundException, IOException {
        XSPDF xsPDF;
        // Show all symbols, but not the apple sign.
        xsPDF = XSPDF.getInstance();
        xsPDF.use(SYMBOL, 14);
        BufferedReader in = new BufferedReader(new FileReader("docs/afm-files/Symbol.afm"));
        String line;
        while ((line = in.readLine()) != null) {
            if (line.startsWith("C ") /* && !line.startsWith("C -1") */) {
                String[] tokenArray = line.split(" ");
                List<String> tokenList = Arrays.asList(tokenArray);
                int k = tokenList.indexOf("N");
                xsPDF.use(HELVETICA);
                xsPDF.print(tokenArray[k + 1] + ": ");
                Character ch = XSUnicodeMapping.getCodeFromName(tokenArray[k + 1]);
                // System.out.println("example-13: " + Integer.toHexString(ch) + " " +
                // ch);
                xsPDF.use(SYMBOL);
                xsPDF.print("" + ch);
                xsPDF.use(HELVETICA);
                xsPDF.print(" (0x" + Integer.toHexString(ch) + ")");
                xsPDF.print("\r");
            }
        }
        in.close();
        xsPDF.createPdf(new File(tempDir, "test 13 - all SYMBOL characters.pdf"));
    }

    private static void example14() throws IOException {
        XSPDF xsPDF;
        // Big signs with symbol font.
        xsPDF = XSPDF.getInstance();
        xsPDF.use(SYMBOL, 48);
        xsPDF.setLineLeading(0);
        xsPDF.print("\u2320\u2320\u2320\u2320\u222b\r");
        xsPDF.print("\uf8f5\uf8f5\uf8f5\u2321\r");
        xsPDF.print("\uf8f5\uf8f5\u2321\r");
        xsPDF.print("\uf8f5\u2321\r");
        xsPDF.print("\u2321\r");
        xsPDF.print("\r");
        xsPDF.createPdf(new File(tempDir, "test 14 - long integrals.pdf"));
    }

    private static void example15_16() throws IOException {
        // Many text and different encodings.
        for (int i = 15; i <= 16; i++) {
            XSPDF xsPDF = XSPDF.getInstance();
            XSContentEncoding encoding = i == 15 ? NO_ENCODING : DEFLATE_ENCODING;
            xsPDF.use(HELVETICA, encoding);
            xsPDF.print(LOREM_IPSUM);
            xsPDF.createPdf(new File(tempDir, "test " + i + " - lorem ipsum - " + encoding.name() + ".pdf"));
        }
    }

    private static void example17() throws IOException {
        XSPDF
            .getInstance()
            .use(
                NO_FORMATTING,
                COURIER,
                10,
                DIN_A6,
                NO_MARGIN,
                NO_INDENTATION,
                ROTATE,
                "      ***********                  ***********\n" + "   *****************            *****************\n"
                    + " *********************        *********************\n"
                    + "***********************      ***********************\n"
                    + "************************    ************************\n"
                    + "*************************  *************************\n"
                    + " **************************************************\n"
                    + "  ************************************************\n"
                    + "    ********************************************\n" + "      ****************************************\n"
                    + "         **********************************\n" + "           ******************************\n"
                    + "              ************************\n" + "                ********************\n"
                    + "                   **************\n" + "                     **********\n"
                    + "                       ******\n" + "                         **")
            .createPdf(new File(tempDir, "test 17 - heart.pdf"));
    }

    private static void example18() throws FileNotFoundException, IOException {
        XSPDF xsPDF;
        // Show all symbols that have a code in standard encoding.
        xsPDF = XSPDF.getInstance();
        xsPDF.use(ZAPFDINGBATS, 14);
        BufferedReader in = new BufferedReader(new FileReader("docs/afm-files/ZapfDingbats.afm"));
        String line;
        while ((line = in.readLine()) != null) {
            if (line.startsWith("C ") && !line.startsWith("C -1")) {
                String[] tokenArray = line.split(" ");
                List<String> tokenList = Arrays.asList(tokenArray);
                int k = tokenList.indexOf("N");
                xsPDF.use(COURIER);
                String token = tokenArray[k + 1];
                xsPDF.print(token);
                xsPDF.print("........".substring(token.length()));
                xsPDF.print(": ");
                xsPDF.use(ZAPFDINGBATS);
                Character ch = XSUnicodeMapping.getCodeFromName(token);
                String hexUniCode = Integer.toHexString(ch);
                xsPDF.print("" + ch);
                xsPDF.use(HELVETICA);
                xsPDF.print(" (0x" + hexUniCode + ")");
                xsPDF.print("\r");
            }
        }
        in.close();
        // xsPDF.dumpDocument(new PrintStream(System.out));
        xsPDF.createPdf(new File(tempDir, "test 18 - all ZAPFDINGBATS characters.pdf"));
    }

    private static void example19() throws IOException {
        XSPDF.create(new File(tempDir, "test 19 - alternative encoding and apple symbol.pdf"), ROTATE, NO_ENCODING, "Hallo: "
            + XSUnicodeMapping.getCodeFromName("multiply"), "Hallo: ", "multiply\r\rApfel: ", SYMBOL, "\uf8ff\r", HELVETICA,
            17, "Take an ", SYMBOL, "\uf8ff", HELVETICA, " a day to feel okay!", NEW_PAGE, SYMBOL, 280, "\uf8ff");
    }

    private static void example20() throws IOException {
        XSPDF.create(new File(tempDir, "test 20 - text alignments.pdf"), HELVETICA, 32, "This line is left aligned.\r\r",
            CENTERED, "This line is centered.\r\r", RIGHT_ALIGNED, "This line is right aligned.\r\r", JUSTIFICATION,
            "This is a very, very long", "text block without any sense that should be printed left and right aligned.");
    }

    private static void example21() throws IOException {
        XSPDF.create(new File(tempDir, "test 21 - text colors.pdf"), JUSTIFICATION, COURIER, 48, BOLD, NO_ENCODING, "In",
            Color.GREEN, "this", Color.RED, "text", Color.ORANGE, "every", Color.CYAN, "word", Color.MAGENTA, "has",
            Color.BLUE, "its", Color.GRAY, "own", Color.PINK, "color.");
    }

    private static void example22() throws IOException {
        final XSPDF xsPDF = XSPDF.getInstance();
        XSContentListener xsContentListener = new XSContentListener() {
            public void newPage(XSPDF xsPDF, int pageNumber) {
                Color[] colors = new Color[] {Color.BLUE, Color.GREEN, Color.RED, Color.MAGENTA};
                Color textColor = colors[(pageNumber - 1) % 4];
                xsPDF.setTextFillColor(textColor);
            }

            public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
                // Ignore.
            }

            public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
                // Ignore.
            }

            public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
                // Ignore.
            }
        };
        xsPDF.addContentListener(xsContentListener);
        xsPDF.use(DIN_A5, NO_INDENTATION, ROTATE, HELVETICA, 100, "In this document the color of the text",
            "will change on every page.", new File(tempDir, "test 22 - page listener.pdf"));
    }

    private static void example23() throws IOException {
        final XSPDF xsPDF = XSPDF.getInstance();
        XSContentListener xsContentListener = new XSContentListener() {
            public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
                // Ignore.
            }

            public void newPage(XSPDF xsPDF, int pageNumber) {
                // Ignore.
            }

            public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
                int num = pageNumber + lineNumber - 2;
                Color[] colors = new Color[] {Color.BLUE, Color.GREEN, Color.RED, Color.MAGENTA};
                Color textColor = colors[num % 4];
                xsPDF.setTextFillColor(textColor);
            }

            public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
                // Ignore.
            }
        };
        xsPDF.addContentListener(xsContentListener);
        xsPDF.use(DIN_A4, TIMES, 24, BOLD, "In this document the color of the text", "will change on every new text line:\r\r",
            TIMES, 14, LOREM_IPSUM, new File(tempDir, "test 23 - line listener.pdf"));
    }

    private static void example24() throws IOException {
        XSPDF.create(new File(tempDir, "test 24 - indent.pdf"), NO_ENCODING, INDENTATION_SPACE, JUSTIFICATION,
            "This is a long",
            "text block without any sense that should be printed left and right aligned but with a small indentation in first",
            "text line.\r\r", INDENTATION_SPACE, 4, LEFT_ALIGNED,
            "This is another long text that has a larger indentation and is left aligned.", NO_INDENTATION,
            "\rThis text has no indentation.");
    }

    private static void example25() throws IOException {
        XSPDF.create(new File(tempDir, "test 25 - lorem ipsum games.pdf"), JUSTIFICATION, LOREM_IPSUM, NEW_PAGE, LEFT_ALIGNED,
            LOREM_IPSUM, NEW_PAGE, RIGHT_ALIGNED, LOREM_IPSUM, NEW_PAGE, CENTERED, LOREM_IPSUM);
    }

    private static void example26() throws IOException {
        XSPDF xsPDF;
        xsPDF = new XSPDF();
        xsPDF.use(ROTATE, NO_INDENTATION);
        xsPDF.setFont(COURIER, 32.0, BOLD);
        xsPDF.setTextFillColor(Color.CYAN).setTextStrokeColor(Color.RED);
        xsPDF.setTextRenderMode(TEXT_FILL_AND_STROKE);
        xsPDF.print("This text is filled and stroked.\r");
        xsPDF.setTextRenderMode(TEXT_FILL);
        xsPDF.print("This text is filled.\r");
        xsPDF.setTextRenderMode(TEXT_STROKE);
        xsPDF.print("This text is stroked.\r");
        xsPDF.use(CENTERED, "The 'B' is invisible:\r");
        xsPDF.use(COURIER, BOLD, 160, TEXT_FILL_AND_STROKE, "A", TEXT_INVISIBLE, "B", TEXT_FILL, "C", TEXT_STROKE, "D");
        xsPDF.createPdf(new File(tempDir, "test 26 - text render modes.pdf"));
    }

    private static void example27() throws IOException {
        XSPDF.create(new File(tempDir, "test 27 - single image.pdf"), USER_UNITS, SINGLE_FULL_PAGE_COLUMN, NO_ENCODING,
            ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg")), 0, 0, 200, 150, 0);
    }

    private static void example28() throws IOException {
        XSPDF.create(new File(tempDir, "test 28 - multiple image.pdf"), NO_MARGIN, NO_ENCODING,
            ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg")), 0, 0, 200, 150, 0,
            ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg")), 200, 150, 200, 150, 0);
    }

    private static void example29() throws IOException {
        BufferedImage image = ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg"));
        XSPDF xsPDF =
            XSPDF.getInstance().use(USER_UNITS, NO_ENCODING, NO_MARGIN, image, (DIN_A4.width - 200) / 2,
                (DIN_A4.height - 150) / 2, 200, 150, 10);
        for (int i = 1; i <= 1000; i++) {
            xsPDF.print("" + i);
        }
        xsPDF.createPdf(new File(tempDir, "test 29 - text around an image.pdf"));
    }

    private static void example30() throws IOException {
        XSPDF xsPDF = new XSPDF();
        xsPDF.addContentListener(new XSContentListenerAdapter() {
            Color[] colors = {Color.RED, Color.GREEN, Color.BLUE, Color.MAGENTA};

            int count = 0;

            @Override
            public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
                xsPDF.setColumnBackgroundColor(colors[count++]);
            }
        });
        xsPDF.setContentEncoding(NO_ENCODING);
        xsPDF.setPageSize(200, 200);
        xsPDF.addColumn(10, 10, 140, 140, null, 10);
        xsPDF.addColumn(50, 50, 140, 140, null, 10);
        xsPDF.setFontSize(3.0);
        xsPDF.setPageMargin(NO_MARGIN);
        for (int i = 1; i <= 2000; i++) {
            xsPDF.print("" + i);
        }
        xsPDF.createPdf(new File(tempDir, "test 30 - non-standard columns in a page.pdf"));
    }

    private static void example31() throws IOException {
        final BufferedImage image = ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg"));
        final XSPDF xsPDF = new XSPDF();
        XSDimension pageSize = xsPDF.getPageSize();
        xsPDF.addColumn(pageSize.width / 2.0 - 100, 150, 200, 150, "image-column", 10);
        double columnWidth = pageSize.width / 2 - 15;
        double columnHeight = pageSize.height / 2 - 20;
        xsPDF.addColumn(10, 10, columnWidth, columnHeight, null, 10);
        xsPDF.addColumn(pageSize.width / 2 + 5, 10, columnWidth, columnHeight, null, 10);
        xsPDF.addColumn(10, columnHeight + 20, pageSize.width - 20, columnHeight, null, 10);
        XSContentListener xsContentListener = new XSContentListener() {
            int k = 0;

            Color[] colors = {Color.green, Color.red, Color.blue, Color.pink, Color.cyan, Color.magenta};

            public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
                // Ignore.
            }

            public void newPage(XSPDF xsPDF, int pageNumber) {
                // Ignore.
            }

            public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
                // Ignore.
            }

            public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
                xsPDF.setTextFillColor(colors[k++ % colors.length]);
                if ("image-column".equals(columnName)) {
                    xsPDF.setImage(image, 0, 0, 200, 150, 10);
                }
            }
        };
        xsPDF.addContentListener(xsContentListener);
        // xsPDF.print(LOREM_IPSUM);
        for (int i = 1; i <= 1000; i++) {
            xsPDF.print("" + i);
        }
        xsPDF.createPdf(new File(tempDir, "test 31 - non-standard columns with image column.pdf"));
    }

    private static void example32() throws IOException {
        final BufferedImage image = ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg"));
        final XSPDF xsPDF = new XSPDF();
        xsPDF.addColumn(200, 200, 400, 300, null, 10.0);
        XSContentListener xsContentListener = new XSContentListener() {
            int k = 0;

            Color[] colors = {Color.green, Color.red, Color.blue, Color.pink, Color.cyan, Color.magenta};

            public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
                // Ignore.
            }

            public void newPage(XSPDF xsPDF, int pageNumber) {
                // Ignore.
            }

            public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
                // Ignore.
            }

            public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
                xsPDF.setTextFillColor(colors[k++ % colors.length]);
            }
        };
        xsPDF.addContentListener(xsContentListener);
        xsPDF.setPageSize(800, 800);
        xsPDF.addColumns(3, "dreier");
        xsPDF.setImage(image, 0, 0, 400, 300, 10);
        // xsPDF.print(LOREM_IPSUM);
        for (int i = 1; i <= 1000; i++) {
            xsPDF.print("" + i);
        }
        xsPDF.createPdf(new File(tempDir, "test 32 - easy image column test.pdf"));
    }

    private static void example33() throws IOException {
        final XSPDF xsPDF = new XSPDF();
        xsPDF.rotatePage();
        xsPDF.setFontSize(64);
        xsPDF.setAlignment(CENTERED);
        xsPDF.print("\r\rHello world - full screen!");
        xsPDF.setPageMode(FULL_SCREEN);
        xsPDF.createPdf(new File(tempDir, "test 33 - fullscreen mode.pdf"));
    }

    private static void example34() throws IOException {
        XSPDF xsPDF = new XSPDF();
        BufferedImage image = ImageIO.read(new File("..\\xsPDF-Examples\\img\\graffiti.jpg"));
        int width = image.getWidth(), height = image.getHeight();
        xsPDF.setPageSize(width, height);
        xsPDF.setPageMargin(NO_MARGIN);
        xsPDF.setImage(image, 0, 0, width, height, 0);
        xsPDF.createPdf(new File(tempDir, "test 34 - single paged file from image.pdf"));
    }

    private static void example35() throws IOException {
        BufferedImage image = ImageIO.read(new File("..\\xsPDF-Examples\\img\\graffiti.jpg"));
        int width = image.getWidth(), height = image.getHeight();
        XSPDF.getInstance().setPageSize(width, height).setPageMargin(NO_MARGIN).setImage(image, 0, 0, width, height, 0)
            .createPdf(new File(tempDir, "test 35 - single paged file from image.pdf"));
    }

    private static void example36() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance().rotatePage();
        xsPDF.print("Just look at the page labels in the icon bar on the following pages.");
        xsPDF.newPage().print("Starting with small roman numerals...").setPageLabel(LOWER_ROMAN_PAGE_LABEL, null, 1);
        xsPDF.newPage().newPage().newPage().newPage().newPage().newPage();
        xsPDF.newPage().print("Starting with arabic numeral 2...").setPageLabel(DECIMAL_ARABIC_PAGE_LABEL, null, 2);
        xsPDF.newPage().newPage().newPage().newPage().newPage().newPage();
        xsPDF.newPage().print("String only on this last page!").setPageLabel("Ich bin ein Berliner!");
        xsPDF.createPdf(new File(tempDir, "test 36 - page labels.pdf"));
    }

    private static void example37() throws IOException {
        XSPDF xsPDF = XSPDF.getInstance();
        BufferedImage image = ImageIO.read(new File("docs/other-pdf-files/P1000421-small.jpg"));
        int width = image.getWidth();
        int height = image.getHeight();
        for (int i = 1; i <= 3; i++) {
            xsPDF.setImageCompressionQuality(0.1f + 0.3f * i);
            xsPDF.setImage(image, 20 * i, (i - 1) * height, width, height, 0);
        }
        xsPDF.createPdf(new File(tempDir, "test 37 - image compressions.pdf"));
    }
}
