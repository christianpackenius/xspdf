/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Page list.
 * @author Christian Packenius, 2013.
 */
public class XSPageList implements Iterable<XSPage> {
    private final List<XSPage> pages = new ArrayList<XSPage>();

    /**
     * Remove all pages from page list.
     */
    void clear() {
        pages.clear();
    }

    /**
     * Get the number of pages in this page list.
     * @return Current page list size.
     */
    int size() {
        return pages.size();
    }

    /**
     * Adds a page at the end of the page list.
     * @param page New page to add to the page list.
     */
    public void add(XSPage page) {
        pages.add(page);
    }

    /**
     * Check if there is no page in the page list.
     * @return <i>true</i> if there is no page in the list.
     */
    public boolean isEmpty() {
        return pages.isEmpty();
    }

    /**
     * Get a single page from the list.
     * @param pageID Number of the page (starting with zero for the first page).
     * @return XSPage object.
     */
    public XSPage get(int pageID) {
        return pages.get(pageID);
    }

    /**
     * @see java.lang.Iterable#iterator()
     */
    public Iterator<XSPage> iterator() {
        return new XSPageListIterator(pages.toArray(new XSPage[0]));
    }

    /**
     * Removes the last element in this list.
     * @return <i>true</i> if there has been an element to remove. <i>false</i> otherwise.
     */
    public boolean removeLast() {
        int lastPageID = pages.size() - 1;
        if (lastPageID >= 0) {
            pages.remove(lastPageID);
            return true;
        }
        return false;
    }
}
