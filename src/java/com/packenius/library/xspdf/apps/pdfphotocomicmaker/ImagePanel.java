/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

/**
 * @author Christian Packenius, 2013.
 */
public class ImagePanel extends JPanel implements MouseListener, MouseMotionListener {
    private static final long serialVersionUID = -8076458130560378312L;

    private boolean mouseInImagePanel = false;

    BufferedImage image = null;

    File imageFile = null;

    List<SpeechBalloonMarker> markers = new ArrayList<SpeechBalloonMarker>();

    private SpeechBalloonMarker currentMarker = null;

    private int mouseX, mouseY;

    private double drawWidth, drawHeight;

    /**
     * Constructor.
     */
    public ImagePanel() {
        addMouseMotionListener(this);
        addMouseListener(this);
    }

    /**
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
     */
    public void mouseDragged(MouseEvent event) {
        if (currentMarker != null) {
            currentMarker.mouseDragged(event.getX(), event.getY());
            storeImageData();
        }
        repaint();
    }

    /**
     * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
     */
    public void mouseMoved(MouseEvent event) {
        setCurrentMarkerFromMouseCoordinates(event);
        if (currentMarker != null) {
            currentMarker.setCursorType(this, mouseX, mouseY, drawWidth, drawHeight);
            storeImageData();
        } else {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        repaint();
    }

    /**
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent event) {
        setCurrentMarkerFromMouseCoordinates(event);
        if (currentMarker == null) {
            if (event.getButton() == MouseEvent.BUTTON1) {
                markers.add(currentMarker = new SpeechBalloonMarker(mouseX, mouseY, image, drawWidth, drawHeight));
            } else {
                // try {
                // SingleImagePdfStore.storeAsPdf(image, imageFile, markers);
                new ImagePopupMenu(this).show(event.getComponent(), event.getX(), event.getY());
                // } catch (IOException exception) {
                // JOptionPane.showMessageDialog(this,
                // "Couldn't store image as PDF file!");
                // }
            }
        } else {
            if (event.getButton() == MouseEvent.BUTTON1) {
                if (event.getClickCount() % 2 == 0) {
                    currentMarker.doubleClick();
                }
            } else { // Not left mouse button.
                currentMarker.rightClick(mouseX, mouseY, this);
            }
        }
        storeImageData();
        repaint();
    }

    /**
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent arg0) {
        // System.out.println("Entered");
        mouseInImagePanel = true;
        repaint();
    }

    /**
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent arg0) {
        // System.out.println("Exited");
        mouseInImagePanel = false;
        repaint();
    }

    /**
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent event) {
        if (currentMarker != null) {
            if (event.getButton() == MouseEvent.BUTTON1) {
                currentMarker.mouseDown(event.getX(), event.getY(), drawWidth, drawHeight);
            }
        }
        // dragging = true;
        storeImageData();
        repaint();
    }

    /**
     * @see java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent event) {
        if (currentMarker != null) {
            if (event.getButton() == MouseEvent.BUTTON1) {
                currentMarker.mouseUp(event.getX(), event.getY(), drawWidth, drawHeight);
            }
        }
        setCurrentMarkerFromMouseCoordinates(event);
        // dragging = false;
        storeImageData();
        repaint();
    }

    void setCurrentImage(BufferedImage image, File imageFile) {
        this.image = image;
        this.imageFile = imageFile;
        markers.clear();
        loadImageData();
        repaint();
    }

    /**
     * Store data about all markers from a properties file.
     */
    private void loadImageData() {
        Properties prop = new Properties();
        try {
            InputStream in = new FileInputStream(getPropFile());
            prop.load(in);
            in.close();

            int markerCount = Integer.parseInt(prop.getProperty("markerCount"));
            for (int i = 0; i < markerCount; i++) {
                String type = prop.getProperty(i + "-type");
                SpeechBalloonMarker marker = null;
                if (type.equals(SpeechBalloonMarker.class.getSimpleName())) {
                    marker = new SpeechBalloonMarker(0, 0, image, drawWidth, drawHeight);
                } else {
                    JOptionPane.showMessageDialog(this, "Couldn't load marker of type <" + type + ">!");
                }
                marker.loadMarker(prop, i);
                markers.add(marker);
            }
        } catch (Exception e) {
            // Ignore.
        }
    }

    /**
     * Store data about all markers in a properties file.
     */
    private void storeImageData() {
        Properties prop = new Properties();
        prop.setProperty("markerCount", "" + markers.size());
        int index = 0;
        for (SpeechBalloonMarker marker : markers) {
            marker.storeMarker(prop, "" + index++);
        }
        try {
            FileOutputStream out = new FileOutputStream(getPropFile());
            prop.store(out, "(w) by PDF Photo Comic Maker, (c) by Christian Packenius");
            out.close();
        } catch (Exception e) {
            // Ignore.
        }
    }

    private File getPropFile() throws IOException {
        String pathname = imageFile.getCanonicalPath() + ".comic";
        return new File(pathname);
    }

    /**
     * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // imageFactor = 1.0;
        if (image != null) {
            Graphics2D g2d = (Graphics2D) g;
            Dimension panelSize = getSize();
            drawWidth = getWidth();
            drawHeight = getHeight();
            double xFactor = drawWidth / image.getWidth();
            double yFactor = drawHeight / image.getHeight();
            double factor = xFactor / yFactor;
            if (factor < 1) {
                drawHeight *= factor;
            } else {
                drawWidth /= factor;
            }
            g2d.drawImage(image, 0, 0, (int) drawWidth, (int) drawHeight, this);
            for (SpeechBalloonMarker marker : markers) {
                // marker.setDrawSize((int) drawWidth, (int) drawHeight);
                marker.paintMarker(g2d, marker == currentMarker, drawWidth, drawHeight);
            }
            if (mouseInImagePanel) {
                g.setColor(Color.GREEN);
                g.drawLine(0, mouseY, panelSize.width, mouseY);
                g.drawLine(mouseX, 0, mouseX, panelSize.height);
            }
        }
    }

    class ImagePopupMenu extends JPopupMenu {
        private static final long serialVersionUID = 6218856585290731107L;

        public ImagePopupMenu(ImagePanel imagePanel) {
            String asdf = "Create and show as PDF document";
            // addItem(asdf, new StoreAndShowAsPDF());
            ActionListener storeAndShowAsPDF = new SingleImagePdfStore(imagePanel);
            addItemToPopupMenu(asdf, storeAndShowAsPDF);
        }

        private void addItemToPopupMenu(String asdf, ActionListener actionListener) {
            JMenuItem item = new JMenuItem(asdf);
            item.addActionListener(actionListener);
            add(item);
        }
    }

    private void setCurrentMarkerFromMouseCoordinates(MouseEvent event) {
        mouseX = event.getX();
        mouseY = event.getY();
        currentMarker = null;
        for (SpeechBalloonMarker marker : markers) {
            if (marker.containsPoint(mouseX, mouseY, drawWidth, drawHeight)) {
                currentMarker = marker;
            }
        }
    }
}
