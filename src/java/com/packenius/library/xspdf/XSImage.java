/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Representation of an image on the page.
 * @author Christian Packenius, 2013.
 */
class XSImage {
    final String imageID;

    final double x, y, width, height;

    final int realPixelWidth, realPixelHeight;

    final double margin;

    final XSLink link;

    XSImage(String imageID, double x, double y, double width, double height, int realPixelWidth, int realPixelHeight,
            double margin, XSLink link) {
        this.imageID = imageID;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.realPixelWidth = realPixelWidth;
        this.realPixelHeight = realPixelHeight;
        this.margin = margin;
        this.link = link;
    }
}
