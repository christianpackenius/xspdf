package com.packenius.library.xspdf;

class XST1StdFontCourierBoldOblique extends XST1StdFontCourier {
	@Override
	String getFontName() {
		return "Courier-BoldOblique";
	}
}
