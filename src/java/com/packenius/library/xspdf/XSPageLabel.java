/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Data class for the label of a page.
 * @author Christian Packenius, 2013.
 */
class XSPageLabel implements XSPdfContent {
    /**
     * May be <i>null</i> for user-defined page labels.
     */
    final XSPageLabelStandardType labelStandardType;

    /**
     * Prefix of page label number.
     */
    final String prefix;

    /**
     * First numerical value of a page label. Must be >= 1 for standard labels!
     */
    final int startValue;

    /**
     * Constructor.
     * @param labelStandardType Type of page label.
     * @param prefix Prefix; may be <i>null</i> or empty for no prefix.
     * @param startValue First value of this page or zero for no page numbering.
     */
    XSPageLabel(XSPageLabelStandardType labelStandardType, String prefix, int startValue) {
        if (labelStandardType == null) {
            labelStandardType = XSPageLabelStandardType.DecimalArabicPageLabel;
        }
        if (prefix == null) {
            prefix = "";
        }
        if (startValue < 0) {
            startValue = 0;
        }

        prefix = XSStatics.escapeStandardStringCharacters(prefix);

        this.labelStandardType = labelStandardType;
        this.prefix = prefix;
        this.startValue = startValue;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        String s = startValue == 0 ? "" : labelStandardType.getPdfContent(xsPDF) + "/St " + startValue;
        if (!prefix.isEmpty()) {
            s += " /P (" + prefix + ")";
        }
        return s.trim();
    }
}
