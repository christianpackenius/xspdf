package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Colored columns.
 */
public class Example016 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setColumnBackgroundColor(Color.PINK);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.createPdf("pdf/Example 016.pdf");
	}
}
