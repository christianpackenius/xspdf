package com.packenius.library.xspdf.examples;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Setting page destinations and creating links to them.
 */
public class Example021 implements XS {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		BufferedImage image = ImageIO.read(new File("img/graffiti.jpg"));
		XSPDF xsPDF = XSPDF.getInstance();
		xsPDF.rotatePage();
		xsPDF.addColumns(2, null);

		// First page.
		xsPDF.print("Dummy text.");
		xsPDF.startLink("page 2");
		xsPDF.print("Go to page 2");
		xsPDF.print("Dummy text.\r\r");
		xsPDF.startLink("page 3", 2);
		xsPDF.print("Go to");
		xsPDF.setFontParameters(BOLD);
		xsPDF.print("page 3\r\r");
		xsPDF.setFontParameters(ORDINARY);
		xsPDF.nextColumn();
		xsPDF.setLinkBorderSize(1);
		xsPDF.startLink("page 4", 0);
		double width = xsPDF.getColumnSize().width;
		xsPDF.setImage(image, 0, 0, width, width, 0);
		xsPDF.print(
				"This image (and this text) link to page 4. This is a very, very long print() message to get a line break.");
		xsPDF.stopLink();
		xsPDF.setLinkBorderSize(5);
		xsPDF.startLink(new URL("http://code.google.com/p/xspdf"));
		xsPDF.print("GOTO xsPDF google code page.");

		// Pages 2..4.
		for (int i = 2; i <= 4; i++) {
			xsPDF.newPage();
			xsPDF.setPageDestination("page " + i);
			xsPDF.print("This is page " + i + ".");
		}

		xsPDF.createPdf("pdf/Example 021.pdf");
	}
}
