/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.Iterator;

/**
 * An iterator of all pages of a page list.
 * @author Christian Packenius, 2013.
 */
public class XSPageListIterator implements Iterator<XSPage> {
    private XSPage[] pages;

    private int index = 0;

    private final int max;

    /**
     * Constructor.
     * @param pages Pages of the page list.
     */
    public XSPageListIterator(XSPage[] pages) {
        this.pages = pages;
        max = pages.length;
    }

    /**
     * @see java.util.Iterator#hasNext()
     */
    public boolean hasNext() {
        return index < max;
    }

    /**
     * @see java.util.Iterator#next()
     */
    public XSPage next() {
        return pages[index++];
    }

    /**
     * @see java.util.Iterator#remove()
     */
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
