*xsPDF* - A Java PDF creation library
=====================================

"_xsPDF_" stands for "eXtreme Simple PDF". It lets you create PDF documents programmatically in an easy to use way.

_xsPDF_ has the following characteristics:

 * Creation of PDF documents as file or stream.
 * Creation of quite small files with minimal overhead.
 * Support of different coding styles:
    * old-style
    * fluid API
    * use() method
 * Quite small and easy to use API.
 * Text and image assistance.
 * Listeners for document control.
 * Overlapping columns and layer technique (including transparency at layer level).
 * User defined page labels.
 * Links within a PDF document and to URLs.
 * Creation of presentations / slideshows.
 * Text processing:
    * Word wrapping.
    * Auto formatting or pre-formatted text usage.
    * Usage of 14 PDF standard fonts and rudimentary Type3 fonts.
    * Alignment.
 * Some raw drawing features.
  