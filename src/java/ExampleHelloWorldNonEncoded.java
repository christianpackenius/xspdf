import java.io.IOException;

import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Non-encoded "Hello world" for testing output files.
 * 
 * @author Christian Packenius, 2015.
 */
public class ExampleHelloWorldNonEncoded implements XS {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xspdf = new XSPDF();
		xspdf
			.setTextParagraphIndentationSpaces(0)
			.setContentEncoding(NO_ENCODING)
			.setFont(HELVETICA)
			.print("Hello World!");
		xspdf.createPdf("hello-world.pdf");
	}
}
