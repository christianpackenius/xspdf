/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.Graphics2D;
import java.util.Properties;
import javax.swing.JComponent;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSRaw;

/**
 * @author Christian Packenius, 2013.
 */
public interface Marker {
    /**
     * Check if the given point on the image is part of the marker.
     * @param imageX X coordinate.
     * @param imageY Y coordinate.
     * @return <i>true</i> if the point is part of this marker.
     */
    public boolean containsPoint(double imageX, double imageY);

    /**
     * Paint the marker.
     * @param g2d
     * @param isCurrentMarker
     * @param imageFactor
     */
    public void paint(Graphics2D g2d, boolean isCurrentMarker, double imageFactor);

    /**
     * Set cursor at the given position if this is the current marker.
     * @param imageX X coordinate.
     * @param imageY Y coordinate.
     * @param comp Component to set the cursor in.
     */
    public void setCursor(double imageX, double imageY, JComponent comp);

    /**
     * Starts a mouse down event (usually resizing or moving marker).
     * @param xx
     * @param yy
     */
    public void mouseDown(int xx, int yy);

    /**
     * Ends a mouse down event.
     * @param xx
     * @param yy
     */
    public void mouseUp(int xx, int yy);

    /**
     * Moves the mouse with left button down.
     * @param xx
     * @param yy
     */
    public void mouseDragged(int xx, int yy);

    /**
     * Double clicked a marker.
     * @param xx
     * @param yy
     */
    public void doubleClick(int xx, int yy);

    /**
     * Right mouse click.
     * @param xx
     * @param yy
     * @param comp
     */
    public void rightClick(int xx, int yy, JComponent comp);

    /**
     * Get XSRaw object from marker - this contains the PDF/PS orders for painting the marker.
     * @return XSRaw object for PDF file.
     */
    public XSRaw getRawObject();

    /**
     * Add text to the XSPDF object.
     * @param xsPDF PDF object.
     * @param layerID TODO
     */
    public void addText(XSPDF xsPDF, int layerID);

    /**
     * Store marker in Properties object.
     * @param prop Properties object.
     * @param index Marker index.
     */
    public void storeMarker(Properties prop, int index);

    /**
     * Loads a marker from Properties Object.
     * @param prop
     * @param index
     */
    public void loadMarker(Properties prop, int index);
}
