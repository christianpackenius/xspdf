package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Pages with different margins.
 */
public class Example007 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setPageMargin(NO_MARGIN);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.newPage();
		xsPDF.setUnitType(INCH);
		xsPDF.setPageMargin(0.1);
		xsPDF.print(LOREM_IPSUM);
		xsPDF.createPdf("pdf/Example 007.pdf");
	}
}
