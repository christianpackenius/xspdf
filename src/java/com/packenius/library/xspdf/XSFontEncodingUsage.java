/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Pure data object for holding font encoding and character range within a text string.
 * @author Christian Packenius, 2013.
 */
class XSFontEncodingUsage {
    /**
     * Font encoding object. <i>null</i> for standard encoding.
     */
    final XSAlternativeFontEncoding fontEncoding;

    /**
     * First character within the text string (inklusive).
     */
    final int firstCharacterIndex;

    /**
     * Last character within the text string (exklusive).
     */
    final int lastCharacterIndex;

    /**
     * Constructor.
     * @param fontEncoding Font encoding object. <i>null</i> for standard encoding.
     * @param firstCharacterIndex First character within the text string (inklusive).
     * @param lastCharacterIndex Last character within the text string (exklusive).
     */
    XSFontEncodingUsage(XSAlternativeFontEncoding fontEncoding, int firstCharacterIndex, int lastCharacterIndex) {
        this.fontEncoding = fontEncoding;
        this.firstCharacterIndex = firstCharacterIndex;
        this.lastCharacterIndex = lastCharacterIndex;
    }
}
