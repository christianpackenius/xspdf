package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Background image using different layers.
 */
public class Example019 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		final BufferedImage image = ImageIO.read(new File("img/graffiti.jpg"));
		xsPDF.setContentEncoding(NO_ENCODING);
		xsPDF.setPageSize(300, 300);
		xsPDF.setLayerID(Integer.MIN_VALUE);
		xsPDF.addColumn(0, 0, 300, 300, "background column", 0);
		xsPDF.setLayerID(-100);
		xsPDF.addColumn(0, 20, 300, 300, "single standard column", 0);
		xsPDF.setLayerID(0);
		xsPDF.addColumn(0, 10, 300, 300, "single standard column", 0);

		xsPDF.setImage(image, 0, 0, 300, 300, 0);

		xsPDF.nextColumn();
		xsPDF.setFont(HELVETICA, 150.0);
		xsPDF.setTextFillColor(Color.YELLOW);
		xsPDF.setTextRenderMode(TEXT_FILL);
		xsPDF.setAlignment(CENTERED);
		xsPDF.setLineLeading(0);
		xsPDF.setTextParagraphIndentationSpaces(0);
		xsPDF.print("TE");
		xsPDF.print("ST");

		xsPDF.nextColumn();
		xsPDF.setFont(COURIER, 45.0, BOLD);
		xsPDF.setTextFillColor(Color.WHITE).setTextStrokeColor(Color.BLACK);
		xsPDF.setTextRenderMode(TEXT_FILL_AND_STROKE);
		xsPDF.print("This is a small text on the standard layer.");

		xsPDF.createPdf("pdf/Example 019.pdf");
	}
}
