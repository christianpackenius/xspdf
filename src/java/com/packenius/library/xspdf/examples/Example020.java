package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;

import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Presentation Mode: FullScreen Mode with automatic paging and optical effects
 * between pages.
 */
public class Example020 implements XS {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = XSPDF.getInstance();
		xsPDF.setPresentationMode(1.0);
		xsPDF.rotatePage();

		xsPDF.use(CENTERED, HELVETICA, BOLD, 140, "Hello\rWorld!");
		xsPDF.setPageBackgroundColor(Color.WHITE);
		xsPDF.setTransitionStyleSplit(HORIZONTAL, INWARD, 2.0);

		xsPDF.newPage().print("This\ris an");
		xsPDF.setPageBackgroundColor(new Color(240, 240, 240));
		xsPDF.setTransitionStyleBlinds(VERTICAL, 2.0);

		xsPDF.newPage().print("example\rof");
		xsPDF.setPageBackgroundColor(new Color(224, 224, 224));
		xsPDF.setTransitionStyleBox(OUTWARD, 2.0);

		xsPDF.newPage().print("how to\rcreate");
		xsPDF.setPageBackgroundColor(new Color(208, 208, 208));
		xsPDF.setTransitionStyleWipe(LEFT_TO_RIGHT, 2.0);

		xsPDF.newPage().print("SLIDE\rSHOWS");
		xsPDF.setPageBackgroundColor(new Color(192, 192, 192));
		xsPDF.setTransitionStyleDissolve(2.0);

		xsPDF.newPage().print("in a\rsmall");
		xsPDF.setPageBackgroundColor(new Color(176, 176, 176));
		xsPDF.setTransitionStyleGlitter(TOP_LEFT_TO_BOTTOM_RIGHT, 2.0);

		xsPDF.newPage().print("PDF\rdocument.");
		xsPDF.setPageBackgroundColor(new Color(160, 160, 160));
		xsPDF.setTransitionStyleFly(OUTWARD, TOP_TO_BOTTOM, 2.0);

		xsPDF.newPage().print("Lay\rback.");
		xsPDF.setPageBackgroundColor(new Color(144, 144, 144));
		xsPDF.setTransitionStylePush(TOP_TO_BOTTOM, 2.0);

		xsPDF.newPage().print("Have\rfun.");
		xsPDF.setPageBackgroundColor(new Color(176, 176, 176));
		xsPDF.setTransitionStyleCover(LEFT_TO_RIGHT, 2.0);

		xsPDF.newPage().print("Enjoy\rthe show.");
		xsPDF.setPageBackgroundColor(new Color(192, 192, 192));
		xsPDF.setTransitionStyleUncover(TOP_TO_BOTTOM, 2.0);

		xsPDF.newPage().print("Do it\ragain!");
		xsPDF.setPageBackgroundColor(new Color(208, 208, 208));
		xsPDF.setTransitionStyleFade(2.0);

		xsPDF.newPage().setTextFillColor(Color.WHITE).print("\r\rThe end.");
		xsPDF.setPageBackgroundColor(Color.BLACK);

		xsPDF.createPdf("pdf/Example 020.pdf");
	}
}
