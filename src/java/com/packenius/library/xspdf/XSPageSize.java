/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Size of a PDF page.
 * @author Christian Packenius, 2013.
 */
public final class XSPageSize {
    /**
     * Width of a page in user units (1/72 inch).
     */
    public final double width;

    /**
     * Height of a page in user units (1/72 inch).
     */
    public final double height;

    /**
     * Constructor.
     * @param width Width of the page in user units (1/72 inch).
     * @param height Height of the page in user units (1/72 inch).
     */
    public XSPageSize(double width, double height) {
        if (width <= 0 || height <= 0) {
            throw new XSPdfException("Illegal page size!");
        }
        this.width = width;
        this.height = height;
    }

    /**
     * Returns a page size with exchanged height and width of this one.
     * @return Rotated page size object.
     */
    public XSPageSize rotate() {
        return new XSPageSize(height, width);
    }

    /**
     * Returns a page size that has bee build with unit type information.
     * @param width Width of the page.
     * @param height Height of the page.
     * @param unitType Unit of width and height.
     * @return Page size object.
     */
    public static XSPageSize getPageSizeWithUnitType(double width, double height, XSUnitType unitType) {
        return new XSPageSize(XSStatics.convertToUserUnits(width, unitType), XSStatics.convertToUserUnits(height, unitType));
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PageSize[" + width + "x" + height + "]";
    }
}
