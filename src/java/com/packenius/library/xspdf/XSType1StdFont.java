/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Christian Packenius, 2013.
 */
abstract class XSType1StdFont implements XSFontType {
    /**
     * Maps a character code (0..65535) to the width of this glyph.
     */
    final double[] glyphWidths = new double[65536];

    /**
     * Maps a character code (0..65535) to the byte in the standard font encoding.
     */
    final byte[] standardEncodingCodeFromUnicodeCharacter = new byte[65536];

    /**
     * Maps a glyph fontType pair ("glyphname1-glyphname2") to the kerning distance.
     */
    final Map<String, Double> glyphPairKerning = new HashMap<String, Double>();

    public double getAscender() {
        // Please override!
        return 1.0;
    }

    public double getDescender() {
        // Please override!
        return 0.0;
    }

    XSType1StdFont() {
        addCharMetrics();
        addKernPairs();
    }

    abstract void addCharMetrics();

    void addKernPairs() {
        // Overwrite!
    }

    double getKerning(String glyph1, String glyph2) {
        Double kerning = glyphPairKerning.get(glyph1 + "-" + glyph2);
        if (kerning == null) {
            return 0;
        }
        return kerning;
    }

    /**
     * If a character is part of this font but not part of the standard font encoding, get another encoding here.
     * @param character
     * @return Alternative font encoding for this character.
     */
    public XSAlternativeFontEncoding getAlternativeFontEncoding(char character) {
        String fontName = getFontName();
        // System.out.println("XSType1StdFont.name := " + fontName);
        if (fontName.equals("Symbol")) {
            if (character == '\uF8FF') {
                return XSType1AltFontEncoding4.instance;
            }
            throw new XSPdfException("Symbol with character 0x" + Integer.toHexString(character) + " is standard encoded!");
        }
        if (fontName.equals("ZapfDingbats")) {
            throw new XSPdfException(fontName + " has no alternative font encoding!");
        }
        if (standardEncodingCodeFromUnicodeCharacter[character] != 0) {
            // Programming error?!
            throw new XSPdfException(fontName + " with character 0x" + Integer.toHexString(character) + " is standard encoded!");
        }
        if (XSType1AltFontEncoding1.altFontEncoding1[character] != 0) {
            return XSType1AltFontEncoding1.instance;
        }
        if (XSType1AltFontEncoding2.altFontEncoding2[character] != 0) {
            return XSType1AltFontEncoding2.instance;
        }
        if (XSType1AltFontEncoding3.altFontEncoding3[character] != 0) {
            return XSType1AltFontEncoding3.instance;
        }
        throw new XSPdfException("Glyph 0x" + Integer.toHexString(character) + " does not exist in font " + fontName);
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getGlyphWidths()
     */
    public double[] getGlyphWidths() {
        return glyphWidths;
    }

    /**
     * @see com.packenius.library.xspdf.XSFontType#getStandardEncodingCodeFromUnicodeCharacter()
     */
    public byte[] getStandardEncodingCodeFromUnicodeCharacter() {
        return standardEncodingCodeFromUnicodeCharacter;
    }
}
