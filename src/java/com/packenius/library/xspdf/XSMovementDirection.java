/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Direction of movement for transition styles WIPE, GLITTER, FLY, COVER, UNCOVER and PUSH.
 */
public enum XSMovementDirection implements XSPdfContent {
    /**
     * Left to right.
     */
    LeftToRight,

    /**
     * Bottom to top (WIPE only).
     */
    BottomToTop,

    /**
     * Right to left (WIPE only).
     */
    RightToLeft,

    /**
     * Top to bottom.
     */
    TopToBottom,

    /**
     * Top-left to bottom-right (GLITTER only).
     */
    TopLeftToBottomRight;

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        switch (this) {
        case LeftToRight:
            return "/Di 0";
        case BottomToTop:
            return "/Di 90";
        case RightToLeft:
            return "/Di 180";
        case TopToBottom:
            return "/Di 270";
        case TopLeftToBottomRight:
            return "/Di 315";
        }
        throw new XSPdfException("Impossible getPdfContent()!");
    }
}
