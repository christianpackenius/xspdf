package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSContentListenerAdapter;
import com.packenius.library.xspdf.XSPDF;

/**
 * Using images.
 */
public class Example018 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		final BufferedImage image = ImageIO.read(new File("img/graffiti.jpg"));
		xsPDF.setFontSize(10.0);
		xsPDF.setUnitType(MM);
		xsPDF.setPageSize(300, 200);
		xsPDF.addColumn(50, 40, 50, 48, "inner yellow column", 15);
		xsPDF.addColumn(20, 20, 170, 160, "second pink column", 20);
		xsPDF.addColumn(200, 20, 80, 160, "third green column", 20);
		xsPDF.addContentListener(new XSContentListenerAdapter() {
			@Override
			public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
				if (columnName.equals("inner yellow column")) {
					xsPDF.setColumnBackgroundColor(Color.yellow);
				} else if (columnName.equals("second pink column")) {
					xsPDF.setImage(image, 55, 50, 60, 60, 6); // Image-A
					xsPDF.setImage(image, 160, 50, 30, 30, 6); // Image-B
					xsPDF.setColumnBackgroundColor(Color.pink);
				} else { // Third green column.
					xsPDF.setImage(image, 2, 2, 20, 20, 4); // Image-C
					xsPDF.setImage(image, -20, 105, 30, 30, 6); // Image-D
					xsPDF.setColumnBackgroundColor(Color.green);
				}
			}
		});
		for (int i = 1; i <= 1000; i++) {
			xsPDF.print("" + i);
		}
		xsPDF.createPdf("pdf/Example 018.pdf");
	}
}
