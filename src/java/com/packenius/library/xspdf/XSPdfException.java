/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Any xsPDF exception.
 * @author Christian Packenius.
 */
public class XSPdfException extends RuntimeException {
    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 5586893459746042097L;

    /**
     * Constructor.
     * @param errorMessage Error message.
     */
    public XSPdfException(String errorMessage) {
        super(errorMessage);
    }
}
