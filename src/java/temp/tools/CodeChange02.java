/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Christian Packenius, 2015.
 */
public class CodeChange02 {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        for (File file : new File("src/java/com/packenius/library/xspdf/").listFiles()) {
            if (file.getName().startsWith("XSType1StdFont") && file.getName().endsWith(".java")) {
                workFontJavaFile(file);
            }
        }
    }

    /**
     * @param file
     * @throws IOException
     */
    private static void workFontJavaFile(File file) throws IOException {
        System.out.println(file);
        List<String> list = new ArrayList<String>();
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line;
        while ((line = in.readLine()) != null) {
            if (line.trim().startsWith("glyphWidths.put(")) {
                line = line.replace("glyphWidths.put(", "");
                line = line.replace("', ", "', (char) ");
                line = line.replace(".0 / 1000.0);", ", //");
            }
            list.add(line);
        }
        in.close();

        PrintStream out = new PrintStream(new FileOutputStream(file));
        for (String s : list) {
            out.println(s);
        }
        out.close();
    }
}
