/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSRaw;

/**
 * Example of how to use raw objects.
 */
public class ExampleRaw2 implements XS {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        XSRaw rawFigure = new XSRaw();
        rawFigure.setLineWidth(2.0);
        rawFigure.setStrokingColorRGB(1.0, 0.0, 0.0);
        rawFigure.setNonStrokingColorGray(0.7);
        rawFigure.moveTo(10, 10);
        rawFigure.lineTo(90, 10);
        rawFigure.curveTo(90, 90, 10, 90, 10, 10);
        rawFigure.closeFillAndStrokePathUsingEvenOddRule();

        XSPDF xsPDF = new XSPDF();
        xsPDF.setPageSize(100, 80);
        xsPDF.addColumn(0, 0, 100, 100, null, 0);
        xsPDF.addRawObjectToLayer(rawFigure, 0);
        xsPDF.addRawObjectToLayer(rawFigure, 0, 0, 0, 0.5, 0.5);
        xsPDF.addRawObjectToLayer(rawFigure, 0, 50, 0, 0.5, 0.5);
        xsPDF.addRawObjectToLayer(rawFigure, 0, 37.5, 62.5, 0.25, -0.5);
        File pdfFile = new File("raw2.pdf");
        xsPDF.createPdf(pdfFile);
        Desktop.getDesktop().open(pdfFile);
    }
}
