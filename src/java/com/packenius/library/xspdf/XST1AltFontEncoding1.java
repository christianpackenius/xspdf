package com.packenius.library.xspdf;

/**
 * Alternative font encoding.
 * 
 * @author Christian Packenius, 2013.
 */
class XST1AltFontEncoding1 extends XSAlternativeFontEncoding {
	/**
	 * Maps an Utf-8 (Unicode?) code (0..65535) to the byte in this font
	 * encoding.
	 */
	static final byte[] altFontEncoding1 = new byte[65536];

	private static final char[] keyValueArray = new char[] { '\u00cf', (char) 127, '\u00e9', (char) 128, '\u0103',
			(char) 129, '\u0171', (char) 130, '\u011b', (char) 131, '\u0178', (char) 132, '\u00f7', (char) 133,
			'\u00dd', (char) 134, '\u00c2', (char) 135, '\u00e1', (char) 136, '\u00db', (char) 137, '\u00fd',
			(char) 138, '\u0219', (char) 139, '\u00ea', (char) 140, '\u016e', (char) 141, '\u00dc', (char) 142,
			'\u0105', (char) 143, '\u00da', (char) 144, '\u0173', (char) 145, '\u00cb', (char) 146, '\u0110',
			(char) 147, '\uf6c3', (char) 148, '\u00a9', (char) 149, '\u0112', (char) 150, '\u010d', (char) 151,
			'\u00e5', (char) 152, '\u0145', (char) 153, '\u013a', (char) 154, '\u00e0', (char) 155, '\u0162',
			(char) 156, '\u0106', (char) 157, '\u00e3', (char) 158, '\u0116', (char) 159, '\u0161', (char) 160,
			'\u015f', (char) 176, '\u00ed', (char) 181, '\u25ca', (char) 190, '\u0158', (char) 192, '\u0122',
			(char) 201, '\u00fb', (char) 204, '\u00e2', (char) 209, '\u0100', (char) 210, '\u0159', (char) 211,
			'\u00e7', (char) 212, '\u017b', (char) 213, '\u00de', (char) 214, '\u014c', (char) 215, '\u0154',
			(char) 216, '\u015a', (char) 217, '\u010f', (char) 218, '\u016a', (char) 219, '\u016f', (char) 220,
			'\u00b3', (char) 221, '\u00d2', (char) 222, '\u00c0', (char) 223, '\u0102', (char) 224, '\u00d7',
			(char) 226, '\u00fa', (char) 228, '\u0164', (char) 229, '\u2202', (char) 230, '\u00ff', (char) 231,
			'\u0143', (char) 236, '\u00ee', (char) 237, '\u00ca', (char) 238, '\u00e4', (char) 239, '\u00eb',
			(char) 240, '\u0107', (char) 242, '\u0144', (char) 243, '\u016b', (char) 244, '\u0147', (char) 246,
			'\u00cd', (char) 247, };

	static {
		for (int i = 0; i < keyValueArray.length; i += 2) {
			altFontEncoding1[keyValueArray[i]] = (byte) keyValueArray[i + 1];
		}
	}

	private static final String altFontEnc1Diffs = "127 /Idieresis /eacute /abreve /uhungarumlaut /ecaron /Ydieresis /divide /Yacute /Acircumflex /aacute /Ucircumflex"
			+ " /yacute /scommaaccent /ecircumflex /Uring /Udieresis /aogonek /Uacute /uogonek /Edieresis /Dcroat /commaaccent"
			+ " /copyright /Emacron /ccaron /aring /Ncommaaccent /lacute /agrave /Tcommaaccent /Cacute /atilde /Edotaccent"
			+ " /scaron 176 /scedilla 181 /iacute 190 /lozenge 192 /Rcaron 201 /Gcommaaccent 204 /ucircumflex 209 /acircumflex"
			+ " /Amacron /rcaron /ccedilla /Zdotaccent /Thorn /Omacron /Racute /Sacute /dcaron /Umacron /uring /threesuperior"
			+ " /Ograve /Agrave /Abreve 226 /multiply 228 /uacute /Tcaron /partialdiff /ydieresis 236 /Nacute /icircumflex"
			+ " /Ecircumflex /adieresis /edieresis 242 /cacute /nacute /umacron 246 /Ncaron /Iacute";

	public static final XST1AltFontEncoding1 instance = new XST1AltFontEncoding1();

	private XST1AltFontEncoding1() {
		// Singleton.
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getPdfDifferencesListContent()
	 */
	@Override
	public String getPdfDifferencesListContent() {
		return altFontEnc1Diffs;
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getID()
	 */
	@Override
	public String getID() {
		return "1";
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getCharacterEncodingMap()
	 */
	@Override
	public byte[] getCharacterEncodingMap() {
		return altFontEncoding1;
	}
}
