package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Formatting modes.
 */
public class Example011 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setAlignment(LEFT_ALIGNED);
		xsPDF.setTextParagraphIndentationSpaces(0);
		xsPDF.setFont(COURIER, BOLD, ITALIC).print("Auto format:\r\n");
		xsPDF.setFontFamily(HELVETICA);
		xsPDF.print("    This  are        four words.      \r\n");
		xsPDF.print("              This");
		xsPDF.print("are");
		xsPDF.print("four                ");
		xsPDF.print("words.\r\n\r\n");
		xsPDF.setFont(COURIER, BOLD, ITALIC).print("No formatting:\r\n");
		xsPDF.setFontFamily(HELVETICA);
		xsPDF.setTextFormattingMode(NO_FORMATTING);
		xsPDF.print("    This  are        four words.      \r\n");
		xsPDF.print("              This");
		xsPDF.print("are");
		xsPDF.print("four                ");
		xsPDF.print("words.\r\n\r\n");
		xsPDF.setTextFormattingMode(AUTO_FORMAT);
		xsPDF.setFont(COURIER, BOLD, ITALIC).print("Auto format:\r\n");
		xsPDF.setFontFamily(HELVETICA);
		xsPDF.print("    This  are        four words.      \r\n");
		xsPDF.print("              This");
		xsPDF.print("are");
		xsPDF.print("four                ");
		xsPDF.print("words.\r\n\r\n");
		xsPDF.createPdf("pdf/Example 011.pdf");
	}
}
