/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.IOException;
import com.packenius.library.xspdf.XS;

/**
 * Live coding example for xsPDF.
 * @author Christian Packenius, 2015.
 */
public class ExampleForLiveCoding implements XS {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // Vorbereitung: File, Image, XSPDF+createPDF, Desktop-open

        // - "Normales" Coding.
        // - Hallo Welt - Inhalt erläutern.
        // - NO_ENCODING

        // Kurze Erläuterung: XS + XSPDF

        // - fluid API
        // - use-Methode / create-Methode
        // - Seiten / Seitengrößen.
        // - Lorem Ipsum
        // - Fonts
        // - Farben & Listener
        // - Spalten
        // - Image - 24Bit!
        // - Fullscreenmode / ShowThumbnails
        // - Fullscreenmode + Transitionstyles
    }
}
