/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Create a format for "Google Code" from "package.html".<br>
 * Wiki markup help<br>
 * =Heading1=<br>
 * ==Heading2==<br>
 * ===Heading3===<br>
 * <br>
 * * bold* _italic_<br>
 * `inline code`<br>
 * escape: `*`<br>
 * <br>
 * Indent lists 2 spaces:<br>
 * * bullet item<br>
 * # numbered list<br>
 * <br>
 * {{{<br>
 * verbatim code block<br>
 * }}}<br>
 * <br>
 * Horizontal rule<br>
 * ----<br>
 * <br>
 * WikiWordLink<br>
 * [http://domain/page label]<br>
 * http://domain/page<br>
 * || table || cells ||<br>
 * @author Christian Packenius, 2013.
 */
public class Z_Tool_99_HTML2GoogleFormat {
    /**
     * @param args Unused.
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader("src\\java\\com\\packenius\\library\\xspdf\\package.html"));
        new File("temp").mkdir();
        // PrintStream out = new PrintStream("temp/google.code.txt");
        String s;
        while ((s = in.readLine()) != null) {
            String s2 = translateLine(s);
            System.out.println(s2);
            // out.println(s2);
        }
        System.out.println("_*This tutorial will be continued soon...*_");
        // out.close();
        in.close();
    }

    private static String translateLine(String s) {
        s = s.replace("\t", "  ");
        if (s.equals("<body>") || s.equals("</body>")) {
            return "";
        }
        s = s.replace("<h2>", "==").replace("</h2>", "==");
        s = s.replace("<h3>", "===").replace("</h3>", "===");
        s = s.replace("<i>", "_").replace("</i>", "_");
        s = s.replace("<b>", "*").replace("</b>", "*");
        s = s.replace("<p>", "").replace("</p>", "");
        s = s.replace("<ul>", "").replace("</ul>", "");
        s = s.replace("<li>", " * ").replace("</li>", "");
        s = s.replace("<pre><code>", "{{{\r\n").replace("</code></pre>", "}}}");
        if (s.contains("<") && s.contains(">")) {
            throw new RuntimeException(s);
        }
        return s;
    }
}
