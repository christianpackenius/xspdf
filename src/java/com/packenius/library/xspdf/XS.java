/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Constants for using in xsPDF library.
 * 
 * @author Christian Packenius, 2013.
 */
public interface XS {
	/**
	 * "Lorem ipsum" - pseudo / dummy text.
	 */
	public static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore "
			+ "et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut "
			+ "aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu "
			+ "fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia "
			+ "deserunt mollit anim id est laborum.\n\n-\n\n"
			+ "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore "
			+ "eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum "
			+ "zzril delenit augue duis dolore te feugait nulla facilisi. Lorem ipsum dolor sit amet, consectetuer "
			+ "adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.\n\n"
			+ "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex "
			+ "ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie "
			+ "consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui "
			+ "blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\n\n"
			+ "Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer "
			+ "possim assum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod "
			+ "tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud "
			+ "exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.\n\n"
			+ "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum "
			+ "dolore eu feugiat nulla facilisis.\n\n"
			+ "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata "
			+ "sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed "
			+ "diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos "
			+ "et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem "
			+ "ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, At accusam aliquyam diam "
			+ "diam dolore dolores duo eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita ea "
			+ "et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero voluptua. est Lorem ipsum dolor sit "
			+ "amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut "
			+ "labore et dolore magna aliquyam erat.\n\n"
			+ "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam "
			+ "erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, "
			+ "no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing "
			+ "elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. "
			+ "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata "
			+ "sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed "
			+ "diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero "
			+ "eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est "
			+ "Lorem ipsum dolor sit amet.";

	/**
	 * "ORDINARY" HELVETICA font.
	 */
	public static final XSFontType HELVETICA = new XSType1StdFontHelvetica();

	/**
	 * "ORDINARY" COURIER font.
	 */
	public static final XSFontType COURIER = new XSType1StdFontCourier();

	/**
	 * "ORDINARY" TIMES font.
	 */
	public static final XSFontType TIMES = new XSType1StdFontTimesRoman();

	/**
	 * Symbol font.
	 */
	public static final XSFontType SYMBOL = new XSType1StdFontSymbol();

	/**
	 * Zapf Dingbats font.
	 */
	public static final XSFontType ZAPFDINGBATS = new XSType1StdFontZapfDingbats();

	/**
	 * ORDINARY (not bold, not italic) font parameter.
	 */
	public static final XSFontParameter ORDINARY = XSFontParameter.Ordinary;

	/**
	 * PLAIN (ORDINARY, not bold, not italic) font parameter.
	 */
	public static final XSFontParameter PLAIN = XSFontParameter.Ordinary;

	/**
	 * BOLD font parameter.
	 */
	public static final XSFontParameter BOLD = XSFontParameter.Bold;

	/**
	 * ITALIC font parameter.
	 */
	public static final XSFontParameter ITALIC = XSFontParameter.Italic;

	/**
	 * OBLIQUE (ITALIC) font parameter.
	 */
	public static final XSFontParameter OBLIQUE = XSFontParameter.Italic;

	/**
	 * Unit in user units (1/72 inch).
	 */
	public static final XSUnitType USER_UNITS = XSUnitType.userUnits;

	/**
	 * Unit in mm (1/25.4 inch).
	 */
	public static final XSUnitType MM = XSUnitType.mm;

	/**
	 * Unit in inch.
	 */
	public static final XSUnitType INCH = XSUnitType.inch;

	/**
	 * Margin for pages without margin.
	 */
	public static final XSPageMargin NO_MARGIN = new XSPageMargin(0);

	/**
	 * Margin for pages with 10 mm margin.
	 */
	public static final XSPageMargin MARGIN_10MM = new XSPageMargin(10);

	/**
	 * Margin for pages with 20 mm margin.
	 */
	public static final XSPageMargin MARGIN_20MM = new XSPageMargin(20);

	/**
	 * DIN A0 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A0 = XSPageSize.getPageSizeWithUnitType(841, 1189, MM);

	/**
	 * DIN A1 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A1 = XSPageSize.getPageSizeWithUnitType(594, 841, MM);

	/**
	 * DIN A2 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A2 = XSPageSize.getPageSizeWithUnitType(420, 594, MM);

	/**
	 * DIN A3 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A3 = XSPageSize.getPageSizeWithUnitType(297, 420, MM);

	/**
	 * DIN A4 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A4 = XSPageSize.getPageSizeWithUnitType(210, 297, MM);

	/**
	 * DIN A5 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A5 = XSPageSize.getPageSizeWithUnitType(148, 210, MM);

	/**
	 * DIN A6 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A6 = XSPageSize.getPageSizeWithUnitType(105, 148, MM);

	/**
	 * DIN A7 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A7 = XSPageSize.getPageSizeWithUnitType(74, 105, MM);

	/**
	 * DIN A8 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A8 = XSPageSize.getPageSizeWithUnitType(52, 74, MM);

	/**
	 * DIN A9 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_A9 = XSPageSize.getPageSizeWithUnitType(37, 52, MM);

	/**
	 * DIN B0 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B0 = XSPageSize.getPageSizeWithUnitType(1000, 1414, MM);

	/**
	 * DIN B1 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B1 = XSPageSize.getPageSizeWithUnitType(707, 1000, MM);

	/**
	 * DIN B2 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B2 = XSPageSize.getPageSizeWithUnitType(500, 707, MM);

	/**
	 * DIN B3 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B3 = XSPageSize.getPageSizeWithUnitType(353, 500, MM);

	/**
	 * DIN B4 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B4 = XSPageSize.getPageSizeWithUnitType(250, 353, MM);

	/**
	 * DIN B5 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B5 = XSPageSize.getPageSizeWithUnitType(176, 250, MM);

	/**
	 * DIN B6 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B6 = XSPageSize.getPageSizeWithUnitType(125, 176, MM);

	/**
	 * DIN B7 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B7 = XSPageSize.getPageSizeWithUnitType(88, 125, MM);

	/**
	 * DIN B8 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B8 = XSPageSize.getPageSizeWithUnitType(62, 88, MM);

	/**
	 * DIN B9 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_B9 = XSPageSize.getPageSizeWithUnitType(44, 62, MM);

	/**
	 * DIN C0 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C0 = XSPageSize.getPageSizeWithUnitType(917, 1297, MM);

	/**
	 * DIN C1 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C1 = XSPageSize.getPageSizeWithUnitType(648, 917, MM);

	/**
	 * DIN C2 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C2 = XSPageSize.getPageSizeWithUnitType(458, 648, MM);

	/**
	 * DIN C3 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C3 = XSPageSize.getPageSizeWithUnitType(324, 458, MM);

	/**
	 * DIN C4 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C4 = XSPageSize.getPageSizeWithUnitType(229, 324, MM);

	/**
	 * DIN C5 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C5 = XSPageSize.getPageSizeWithUnitType(162, 229, MM);

	/**
	 * DIN C6 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C6 = XSPageSize.getPageSizeWithUnitType(114, 162, MM);

	/**
	 * DIN C7 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C7 = XSPageSize.getPageSizeWithUnitType(81, 114, MM);

	/**
	 * DIN C8 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C8 = XSPageSize.getPageSizeWithUnitType(57, 81, MM);

	/**
	 * DIN C9 page size with portrait orientation.
	 */
	public static final XSPageSize DIN_C9 = XSPageSize.getPageSizeWithUnitType(40, 57, MM);

	/**
	 * INVOICE page size (North America).
	 */
	public static final XSPageSize INVOICE = XSPageSize.getPageSizeWithUnitType(140, 216, MM);

	/**
	 * EXECUTIVE page size (North America).
	 */
	public static final XSPageSize EXECUTIVE = XSPageSize.getPageSizeWithUnitType(184, 267, MM);

	/**
	 * LEGAL page size (North America).
	 */
	public static final XSPageSize LEGAL = XSPageSize.getPageSizeWithUnitType(216, 356, MM);

	/**
	 * LETTER page size (North America).
	 */
	public static final XSPageSize LETTER = XSPageSize.getPageSizeWithUnitType(216, 279, MM);

	/**
	 * TABLOID page size (North America).
	 */
	public static final XSPageSize TABLOID = XSPageSize.getPageSizeWithUnitType(279, 432, MM);

	/**
	 * LEDGER page size (North America).
	 */
	public static final XSPageSize LEDGER = TABLOID;

	/**
	 * BROADSHEET page size (North America).
	 */
	public static final XSPageSize BROADSHEET = XSPageSize.getPageSizeWithUnitType(432, 559, MM);

	/**
	 * Special value used to change width and height of a page size.
	 */
	public static final XSSpecials ROTATE = XSSpecials.Rotate;

	/**
	 * Special value used to move to the next page.
	 */
	public static final XSSpecials NEW_PAGE = XSSpecials.NewPage;

	/**
	 * Special value used to remove all pages.
	 */
	public static final XSSpecials DELETE_ALL_PAGES = XSSpecials.DeleteAllPages;

	/**
	 * Do not encode page content.
	 */
	public static final XSContentEncoding NO_ENCODING = XSContentEncoding.NoEncoding;

	/**
	 * Encode page contents with deflate algorithm.
	 */
	public static final XSContentEncoding DEFLATE_ENCODING = XSContentEncoding.DeflateEncoding;

	/**
	 * Alignment: Left aligned.
	 */
	public static final XSAlignment LEFT_ALIGNED = XSAlignment.LeftAligned;

	/**
	 * Alignment: Right aligned.
	 */
	public static final XSAlignment RIGHT_ALIGNED = XSAlignment.RightAligned;

	/**
	 * Alignment: Left and right aligned.
	 */
	public static final XSAlignment JUSTIFICATION = XSAlignment.Justification;

	/**
	 * Alignment: Left and right aligned.
	 */
	public static final XSAlignment LEFT_AND_RIGHT_ALIGNED = XSAlignment.Justification;

	/**
	 * Alignment: Centered.
	 */
	public static final XSAlignment CENTERED = XSAlignment.Centered;

	/**
	 * No text paragraph indentation.
	 */
	public static final XSTextParagraphIndentation NO_INDENTATION = XSTextParagraphIndentation.NoIndentation;

	/**
	 * Use a single space as indentation.
	 */
	public static final XSTextParagraphIndentation INDENTATION_SPACE = XSTextParagraphIndentation.IndentationSpace;

	/**
	 * When using justification (left and right aligned) text, last line also
	 * should be full justified (left and right aligned).
	 */
	public static final XSSpecials LAST_LINE_FULL_JUSTIFIED = XSSpecials.LastLineFullJustified;

	/**
	 * When using justification (left and right aligned) text, last line shall
	 * be only left aligned.
	 */
	public static final XSSpecials LAST_LINE_LEFT_ALIGNED = XSSpecials.LastLineLeftAligned;

	/**
	 * Fill the glyphs.
	 */
	public static final XSTextRenderingMode TEXT_FILL = XSTextRenderingMode.Fill;

	/**
	 * Stroke the border of the glyphs.
	 */
	public static final XSTextRenderingMode TEXT_STROKE = XSTextRenderingMode.Stroke;

	/**
	 * Fill the glyphs and then stroke their borders.
	 */
	public static final XSTextRenderingMode TEXT_FILL_AND_STROKE = XSTextRenderingMode.FillAndStroke;

	/**
	 * Invisible glyphs.
	 */
	public static final XSTextRenderingMode TEXT_INVISIBLE = XSTextRenderingMode.Invisible;

	/**
	 * Auto formatting - trimming, double space removing and more.
	 */
	public static final XSTextFormattingMode AUTO_FORMAT = XSTextFormattingMode.AutoFormat;

	/**
	 * No formatting - every space is a single word.
	 */
	public static final XSTextFormattingMode NO_FORMATTING = XSTextFormattingMode.NoFormatting;

	/**
	 * Set a single column into the page that has no margin.
	 */
	public static final XSSpecials SINGLE_FULL_PAGE_COLUMN = XSSpecials.SingleFullPageColumn;

	/**
	 * Full Screen - do not show any other window controls, only show the
	 * document content.
	 */
	public static final XSPageMode FULL_SCREEN = XSPageMode.FullScreen;

	/**
	 * Normal mode - but do not show outlines or thumbs.
	 */
	public static final XSPageMode SHOW_DOCUMENT_ONLY = XSPageMode.ShowDocumentOnly;

	/**
	 * Normal mode - but show thumbnails.
	 */
	public static final XSPageMode SHOW_THUMBNAILS = XSPageMode.ShowThumbnails;

	/**
	 * Split style.
	 */
	public static final XSTransitionStyle SPLIT = XSTransitionStyle.Split;

	/**
	 * Blinds style.
	 */
	public static final XSTransitionStyle BLIND = XSTransitionStyle.Blinds;

	/**
	 * Box style.
	 */
	public static final XSTransitionStyle BOX = XSTransitionStyle.Box;

	/**
	 * Wipe style.
	 */
	public static final XSTransitionStyle WIPE = XSTransitionStyle.Wipe;

	/**
	 * Dissolve style.
	 */
	public static final XSTransitionStyle DISSOLVE = XSTransitionStyle.Dissolve;

	/**
	 * Glitter style.
	 */
	public static final XSTransitionStyle GLITTER = XSTransitionStyle.Glitter;

	/**
	 * Replace style (default).
	 */
	public static final XSTransitionStyle REPLACE = XSTransitionStyle.Replace;

	/**
	 * Fly style.
	 */
	public static final XSTransitionStyle FLY = XSTransitionStyle.Fly;

	/**
	 * Push style.
	 */
	public static final XSTransitionStyle PUSH = XSTransitionStyle.Push;

	/**
	 * Cover style.
	 */
	public static final XSTransitionStyle COVER = XSTransitionStyle.Cover;

	/**
	 * Uncover style.
	 */
	public static final XSTransitionStyle UNCOVER = XSTransitionStyle.Uncover;

	/**
	 * Fade style.
	 */
	public static final XSTransitionStyle FADE = XSTransitionStyle.Fade;

	/**
	 * Horizontal line style.
	 */
	public static final XSDirection HORIZONTAL = XSDirection.Horizontal;

	/**
	 * Vertical line style.
	 */
	public static final XSDirection VERTICAL = XSDirection.Vertical;

	/**
	 * Inward direction motion.
	 */
	public static final XSMotionDirection INWARD = XSMotionDirection.Inward;

	/**
	 * Outward direction motion.
	 */
	public static final XSMotionDirection OUTWARD = XSMotionDirection.Outward;

	/**
	 * Left to right.
	 */
	public static final XSMovementDirection LEFT_TO_RIGHT = XSMovementDirection.LeftToRight;

	/**
	 * Bottom to top (WIPE only).
	 */
	public static final XSMovementDirection BOTTOM_TO_TOP = XSMovementDirection.BottomToTop;

	/**
	 * Right to left (WIPE only).
	 */
	public static final XSMovementDirection RIGHT_TO_LEFT = XSMovementDirection.RightToLeft;

	/**
	 * Top to bottom.
	 */
	public static final XSMovementDirection TOP_TO_BOTTOM = XSMovementDirection.TopToBottom;

	/**
	 * Top-left to bottom-right (GLITTER only).
	 */
	public static final XSMovementDirection TOP_LEFT_TO_BOTTOM_RIGHT = XSMovementDirection.TopLeftToBottomRight;

	/**
	 * Default transition mode - No transition, just switch to next page.
	 */
	public static final XSTransitionMode DEFAULT_TRANSITION_MODE = XSTransitionMode.defaultInstance;

	/**
	 * Use "usual" arabic numerals for page labeling: 1, 2, 3, 4, ...
	 */
	public static final XSPageLabelStandardType DECIMAL_ARABIC_PAGE_LABEL = XSPageLabelStandardType.DecimalArabicPageLabel;

	/**
	 * Use uppercase roman numerals for page labeling: I, II, III, IV, ...
	 */
	public static final XSPageLabelStandardType UPPER_ROMAN_PAGE_LABEL = XSPageLabelStandardType.UpperRomanPageLabel;

	/**
	 * Use lowercase roman numerals for page labeling: i, ii, iii, iv, ...
	 */
	public static final XSPageLabelStandardType LOWER_ROMAN_PAGE_LABEL = XSPageLabelStandardType.LowerRomanPageLabel;

	/**
	 * Use uppercase letters for page labeling: A, B, C, D, ...
	 */
	public static final XSPageLabelStandardType UPPER_LETTERS_PAGE_LABEL = XSPageLabelStandardType.UpperLettersPageLabel;

	/**
	 * Use lowercase letters for page labeling: a, b, c, d, ...
	 */
	public static final XSPageLabelStandardType LOWER_LETTERS_PAGE_LABEL = XSPageLabelStandardType.LowerLettersPageLabel;
}
