package com.packenius.library.xspdf.examples;

import java.awt.Color;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Text render modes.
 */
public class Example010 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.setFontFamily(HELVETICA);
		xsPDF.setFontSize(40);
		xsPDF.setTextFillColor(Color.RED);
		xsPDF.setTextStrokeColor(Color.GREEN);
		xsPDF.print("FILL (default)\r\r");
		xsPDF.setTextRenderMode(TEXT_STROKE);
		xsPDF.print("STROKE\r\r");
		xsPDF.setTextRenderMode(TEXT_FILL_AND_STROKE);
		xsPDF.print("FILL and STROKE\r\r");
		xsPDF.setTextRenderMode(TEXT_INVISIBLE);
		xsPDF.print("INVISIBLE");
		xsPDF.setTextRenderMode(TEXT_FILL);
		xsPDF.print(" (invisible)");
		xsPDF.createPdf("pdf/Example 010.pdf");
	}
}
