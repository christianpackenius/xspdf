/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSRaw;

/**
 * @author Christian Packenius, 2013.
 */
public class ExampleRaw implements XS {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        XSPDF xsPDF = new XSPDF();
        xsPDF.setContentEncoding(NO_ENCODING).setPageSize(100, 100);
        xsPDF.addColumn(0, 0, 100, 100, null, 0);
        XSRaw rawColor1 = new XSRaw();
        rawColor1.setStrokingColorGray(0.2);
        rawColor1.setNonStrokingColorGray(0.5);
        XSRaw rawColor2 = new XSRaw();
        rawColor2.setStrokingColorRGB(1.0, 0.0, 0.2);
        rawColor2.setNonStrokingColorRGB(0.0, 1.0, 0.2);
        XSRaw rawFigure = new XSRaw();
        rawFigure.setLineWidth(0);
        rawFigure.moveTo(0, 0);
        rawFigure.lineTo(100, 0);
        rawFigure.curveTo(100, 100, 100, 100, 0, 100);
        rawFigure.closeFillAndStrokePathUsingEvenOddRule();
        xsPDF.addRawObjectToLayer(rawColor2, 0);
        xsPDF.addRawObjectToLayer(rawFigure, 0);
        xsPDF.addRawObjectToLayer(rawColor1, 0);
        xsPDF.addRawObjectToLayer(rawFigure, 0, 10, 10, 0.8, 0.8);
        xsPDF.addRawObjectToLayer(rawColor2, 0);
        xsPDF.addRawObjectToLayer(rawFigure, 0, 20, 20, 0.6, 0.6);
        File pdfFile = new File("raw.pdf");
        xsPDF.createPdf(pdfFile);
        Desktop.getDesktop().open(pdfFile);
    }
}
