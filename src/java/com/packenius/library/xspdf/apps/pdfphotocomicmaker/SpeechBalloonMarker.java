/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSAlignment;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSRaw;

/**
 * All informations about a single speech balloon marker with zero, one or many peaks.
 * @author Christian Packenius, 2013.
 */
public class SpeechBalloonMarker {
    /**
     * Coordinates and sizes of the marker itself. Values: 0.0 .. 1.0.
     */
    private double markerX, markerY, width, height;

    /**
     * Text to print.
     */
    private String text = "";

    /**
     * Corner roundings of this marker. Values: 0.0 .. 1.0.
     */
    private double rounding = 0.03;

    /**
     * Font size to use. Values: 0.0 .. 1.0.
     */
    private double fontsize = 0.03;

    /**
     * Line width for marker and peak lines.
     */
    private double linewidth = 0.0018;

    /**
     * Dragging values. These values are real pixel values.
     */
    private int dragStartX, dragStartY;

    private boolean dragLeft = false, dragRight = false, dragTop = false, dragBottom = false;

    private int deltaX = 0, deltaY = 0;

    private double tempX, tempY, tempWidth, tempHeight;

    /**
     * Marker peaks.
     */
    private List<Point> peaks = new ArrayList<Point>();

    private Point currentPeak = null;

    /**
     * Real image size.
     */
    private final int imageWidth, imageHeight;

    /**
     * Constructor.
     */
    SpeechBalloonMarker(double x, double y, BufferedImage image, double drawWidth, double drawHeight) {
        imageWidth = image.getWidth();
        imageHeight = image.getHeight();
        markerX = x / drawWidth - 0.1;
        markerY = y / drawHeight - 0.05;
        width = 0.2;
        height = 0.1;
    }

    /**
     * Check if the given point on the image is part of the marker.
     * @return <i>true</i> if the point is part of this marker.
     */
    boolean containsPoint(double x, double y, double drawWidth, double drawHeight) {
        x /= drawWidth;
        y /= drawHeight;

        // Point within the (non-corner-rounded) marker rectangle?
        boolean bx = markerX <= x && x <= markerX + width;
        boolean by = markerY <= y && y <= markerY + height;

        // Point near a corner?
        // Set "currentPeak" here.
        currentPeak = null;
        for (Point peak : peaks) {
            if (Math.abs(x - peak.x) <= rounding / 3.0) {
                if (Math.abs(y - peak.y) <= rounding / 3.0) {
                    currentPeak = peak;
                }
            }
        }

        // Decision!
        if (bx && by) {
            return true;
        }
        return currentPeak != null;
    }

    /**
     * Set cursor at the given position if this is the current marker.
     */
    void setCursorType(JComponent comp, double mouseX, double mouseY, double drawWidth, double drawHeight) {
        if (currentPeak != null) {
            comp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } else {
            mouseX /= drawWidth;
            mouseY /= drawHeight;
            setCursorTypeFromMarkerCoordinates(comp, mouseX, mouseY);
        }
    }

    private void setCursorTypeFromMarkerCoordinates(JComponent comp, double mouseX, double mouseY) {
        boolean left = Math.abs(markerX - mouseX) <= rounding / 3.0;
        boolean top = Math.abs(markerY - mouseY) <= rounding / 3.0;
        boolean right = Math.abs(markerX + width - mouseX) <= rounding / 3.0;
        boolean bottom = Math.abs(markerY + height - mouseY) <= rounding / 3.0;
        int cursor = Cursor.HAND_CURSOR;
        if (left && top || right && bottom) {
            cursor = Cursor.NW_RESIZE_CURSOR;
        } else if (left && bottom || right && top) {
            cursor = Cursor.NE_RESIZE_CURSOR;
        } else if (left || right) {
            cursor = Cursor.W_RESIZE_CURSOR;
        } else if (top || bottom) {
            cursor = Cursor.N_RESIZE_CURSOR;
        }
        comp.setCursor(Cursor.getPredefinedCursor(cursor));
    }

    /**
     * Starts a mouse down event (usually resizing or moving marker).
     */
    void mouseDown(int x, int y, double drawWidth, double drawHeight) {
        dragStartX = x;
        dragStartY = y;
        if (currentPeak == null) {
            double xx = x / drawWidth;
            double yy = y / drawHeight;
            dragLeft = Math.abs(markerX - xx) <= rounding;
            dragTop = Math.abs(markerY - yy) <= rounding;
            dragRight = Math.abs(markerX + width - xx) <= rounding;
            dragBottom = Math.abs(markerY + height - yy) <= rounding;
            checkFullMarkerDragging();
        }
    }

    private void checkFullMarkerDragging() {
        if (!(dragLeft || dragTop || dragRight || dragBottom)) {
            dragLeft = dragRight = dragTop = dragBottom = true;
        }
    }

    /**
     * Ends a mouse down event.
     */
    void mouseUp(int x, int y, double drawWidth, double drawHeight) {
        mouseDragged(x, y);
        if (currentPeak == null) {
            markerX = tempX / drawWidth;
            markerY = tempY / drawHeight;
            width = tempWidth / drawWidth;
            height = tempHeight / drawHeight;
        } else {
            currentPeak.x += deltaX / drawWidth;
            currentPeak.y += deltaY / drawHeight;
        }
        deltaX = deltaY = 0;
        dragLeft = dragRight = dragTop = dragBottom = false;
    }

    /**
     * Moves the mouse with left button down.
     */
    void mouseDragged(int x, int y) {
        deltaX = x - dragStartX;
        deltaY = y - dragStartY;
    }

    /**
     * Double clicked a marker. New peaks will be right from existing peaks.
     */
    void doubleClick() {
        if (peaks.isEmpty()) {
            peaks.add(new Point(markerX, Math.min(markerY + height + rounding * 4, 1.0)));
        } else {
            Point peak = peaks.get(peaks.size() - 1);
            peaks.add(new Point(peak.x + rounding * 3, peak.y));
        }
    }

    /**
     * Right mouse click.
     */
    void rightClick(int xx, int yy, JComponent comp) {
        String localText = text.replace("\r", "\\r").replace("\n", "\\n");
        localText = JOptionPane.showInputDialog(null, "Please type text for speech balloon:", localText);
        if (localText != null) {
            text = localText.replace("\\r", "\r").replace("\\n", "\n").replace("\r", "\n");
        }
    }

    /**
     * Get XSRaw object from marker - this contains the PDF/PS orders for painting the marker.
     * @return XSRaw object for PDF file.
     */
    XSRaw getRawObject() {
        XSRaw raw = new XSRaw();
        raw.saveGraphicsState();
        raw.setLineWidth(linewidth * imageHeight);
        raw.setStrokingColorRGB(0.2, 0.2, 0.2);
        raw.setNonStrokingColorRGB(0.6, 0.9, 0.5);
        double rh = rounding * imageHeight;
        double mx = markerX * imageWidth;
        double my = markerY * imageHeight;
        double mw = width * imageWidth;
        double mh = height * imageHeight;

        // Drawing is made clock-wise.

        // Left top corner.
        raw.moveTo(mx + rh, my);
        raw.curveTo(mx, my, mx, my, mx, my + rh);

        // Left side.
        raw.lineTo(mx, my + mh - rh);

        // Left bottom corner.
        raw.curveTo(mx, my + mh, mx, my + mh, mx + rh, my + mh);

        // Peaks.
        int sideFactor = 1;
        for (Point peak : peaks) {
            int px = (int) (peak.x * imageWidth);
            int py = (int) (peak.y * imageHeight);
            raw.lineTo(px, py);
            sideFactor++;
            raw.lineTo(mx + rh * sideFactor, my + mh);
        }

        // Rest of bottom side.
        raw.lineTo(mx + mw - rh, my + mh);

        // Bottom right corner.
        raw.curveTo(mx + mw, my + mh, mx + mw, my + mh, mx + mw, my + mh - rh);

        // Right side.
        raw.lineTo(mx + mw, my + rh);

        // Right top corner.
        raw.curveTo(mx + mw, my, mx + mw, my, mx + mw - rh, my);

        // Top side not necessary - we close the path.

        // raw.close();
        // raw.modifyClippingPathUsingEvenOddRule();
        // raw.fillAndStrokePathUsingEvenOddRule();
        raw.closeFillAndStrokePathUsingEvenOddRule();
        raw.restoreGraphicsState();
        return raw;
    }

    void addTextToPDF(XSPDF xsPDF, int layerID) {
        List<String> lines = getLinesFromString(text);
        double localFontsize = fontsize * imageHeight;
        double xk = markerX * imageWidth;
        // Correction "localFontsize / 5.0" is not explainable but necessary.
        // :-(
        double yk = markerY * imageHeight + (height * imageHeight - localFontsize * lines.size()) / 2.0 + localFontsize / 5.0;
        xsPDF.addColumn(xk, yk, width * imageWidth, imageHeight - yk, "Text column @ layer " + layerID, 0);
        xsPDF.nextColumn();
        xsPDF.setTextParagraphIndentationSpaces(0);
        xsPDF.setFont(XS.TIMES, localFontsize);
        xsPDF.setLineLeading(0);
        xsPDF.setAlignment(XSAlignment.Centered);
        // xsPDF.setLayerTransparency(layerID, 0.5);
        xsPDF.setTextFillColor(Color.BLACK);
        for (String line : lines) {
            xsPDF.print(line + "\r");
        }
    }

    void paintMarker(Graphics2D g2d, boolean isCurrentMarker, double drawWidth, double drawHeight) {
        setTemporaryValues(drawWidth, drawHeight);

        int x = (int) tempX;
        int y = (int) tempY;
        int w = (int) tempWidth;
        int h = (int) tempHeight;
        int drawRounding = (int) (rounding * drawHeight);

        Stroke stroke = new BasicStroke((float) (linewidth * drawHeight));
        g2d.setStroke(stroke);
        g2d.setColor(new Color(isCurrentMarker ? 0.0f : 0.6f, 0.9f, 0.5f/* , 0.3f */));
        g2d.fillRoundRect(x, y, w, h, drawRounding, drawRounding);
        g2d.setColor(isCurrentMarker ? Color.RED : new Color(0.2f, 0.2f, 0.2f));
        g2d.drawRoundRect(x, y, w, h, drawRounding, drawRounding);

        paintText(g2d, drawWidth, drawHeight);
        paintPeaks(g2d, drawWidth, drawHeight);
    }

    private void setTemporaryValues(double drawWidth, double drawHeight) {
        double drawRounding = rounding * drawHeight;
        tempX = markerX * drawWidth + (dragLeft ? deltaX : 0);
        tempY = markerY * drawHeight + (dragTop ? deltaY : 0);
        tempWidth = width * drawWidth + (dragRight ? deltaX : 0);
        tempHeight = height * drawHeight + (dragBottom ? deltaY : 0);
        tempWidth -= dragLeft ? deltaX : 0;
        tempHeight -= dragTop ? deltaY : 0;
        double minSize = 2 * drawRounding;
        if (tempWidth < minSize) {
            tempWidth = minSize;
        }
        if (tempHeight < minSize) {
            tempHeight = minSize;
        }
    }

    private void paintText(Graphics2D g2d, double drawWidth, double drawHeight) {
        // Das XSPDF-Objekt wird im Anschluss lediglich zur Bestimmung der "echten" Textbreite (also jener im PDF) verwendet.
        XSPDF xsPDF = new XSPDF();
        xsPDF.setPageMode(XS.FULL_SCREEN).setContentEncoding(XS.NO_ENCODING);
        xsPDF.setUnitType(XS.USER_UNITS);
        xsPDF.setPageSize(imageWidth, imageHeight);
        xsPDF.addColumn(0, 0, imageWidth, imageHeight, "Image column", 0);
        xsPDF.nextColumn();
        double localFontsize = fontsize * imageHeight;
        xsPDF.setFont(XS.TIMES, localFontsize);

        Font font = new Font("Times New Roman", Font.PLAIN, (int) (fontsize * drawHeight));
        g2d.setFont(font);

        FontMetrics metrics = g2d.getFontMetrics();
        List<String> lines = getLinesFromString(text);
        float xk = (float) (tempX + rounding * drawHeight / 2.0);
        float yk = (float) (tempY + rounding * drawHeight / 2.0 + metrics.getAscent());
        for (String line : lines) {
            g2d.drawString(line, xk, yk);

            // Zeile zeichnen, die die echte Textbreite im sp�teren PDF anzeigt.
            int textLineWidth = (int) xsPDF.getTextLineWidth(line);
            textLineWidth *= drawHeight / imageHeight;
            g2d.drawLine((int) xk, (int) yk, (int) xk + textLineWidth, (int) yk);

            yk += fontsize * drawHeight;
        }
    }

    private void paintPeaks(Graphics2D g2d, double drawWidth, double drawHeight) {
        int y = (int) (tempY + tempHeight);
        int sideFactor = 1;
        for (Point peak : peaks) {
            int px = (int) (peak.x * drawWidth + (peak == currentPeak ? deltaX : 0));
            int py = (int) (peak.y * drawHeight + (peak == currentPeak ? deltaY : 0));
            int x = (int) (tempX + rounding * drawHeight * sideFactor);
            g2d.drawLine(x, y, px, py); // Down to peak...
            sideFactor++;
            x = (int) (tempX + rounding * drawHeight * sideFactor);
            g2d.drawLine(x, y, px, py); // ...and up to marker downside again.
        }
    }

    /**
     * Get a list of single text lines.
     * @param s The string to convert.
     * @return The lines list.
     */
    public static List<String> getLinesFromString(String s) {
        List<String> lines = new ArrayList<String>();
        while (s.contains("\n")) {
            int i = s.indexOf('\n');
            lines.add(s.substring(0, i));
            s = s.substring(i + 1);
        }
        lines.add(s);
        return lines;
    }

    /**
     * Store marker in Properties object.
     */
    void storeMarker(Properties prop, String prefix) {
        prop.setProperty(prefix + "-type", getClass().getSimpleName());
        prop.setProperty(prefix + "-text", text);
        prop.setProperty(prefix + "-x", "" + markerX);
        prop.setProperty(prefix + "-y", "" + markerY);
        prop.setProperty(prefix + "-width", "" + width);
        prop.setProperty(prefix + "-height", "" + height);
        prop.setProperty(prefix + "-peakCount", "" + peaks.size());

        for (int i = 0; i < peaks.size(); i++) {
            prop.setProperty(prefix + "-" + i + "-peak-x", "" + peaks.get(i).x);
            prop.setProperty(prefix + "-" + i + "-peak-y", "" + peaks.get(i).y);
        }
    }

    /**
     * Loads a marker from Properties Object.
     */
    void loadMarker(Properties prop, int index) {
        text = prop.getProperty(index + "-text", "");
        markerX = getDoubleFromProp(prop, index + "-x");
        markerY = getDoubleFromProp(prop, index + "-y");
        width = getDoubleFromProp(prop, index + "-width");
        height = getDoubleFromProp(prop, index + "-height");
        loadPeaks(prop, index);
    }

    private void loadPeaks(Properties prop, int index) {
        int peakCount = Integer.parseInt(prop.getProperty(index + "-peakCount"));
        peaks.clear();
        for (int i = 0; i < peakCount; i++) {
            double px = getDoubleFromProp(prop, index + "-" + i + "-peak-x");
            double py = getDoubleFromProp(prop, index + "-" + i + "-peak-y");
            peaks.add(new Point(px, py));
        }
    }

    private double getDoubleFromProp(Properties prop, String key) {
        return Double.parseDouble(prop.getProperty(key, "0"));
    }
}
