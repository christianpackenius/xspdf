/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

import java.net.URL;

/**
 * A link to a destination name or to a URI.
 * @author Christian Packenius, 2013.
 */
class XSLink {
    final String destinationName;

    final URL url;

    final double borderSize;

    XSLink(String destinationName, double borderSize) {
        this.destinationName = destinationName;
        url = null;
        this.borderSize = borderSize;
    }

    XSLink(URL url, double borderSize) {
        destinationName = null;
        this.url = url;
        this.borderSize = borderSize;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof XSLink)) {
            return false;
        }
        XSLink obj2 = (XSLink) obj;
        if (destinationName != null && !destinationName.equals(obj2.destinationName)) {
            return false;
        }
        if (url != null && !url.equals(obj2.url)) {
            return false;
        }
        return true;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        if (destinationName != null) {
            return destinationName.hashCode();
        } else {
            return url.hashCode();
        }
    }

    @Override
    public String toString() {
        if (destinationName != null) {
            return "Link[" + destinationName + "]";
        }
        return "URL-Link[" + url.toString() + "]";
    }
}
