/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.io.IOException;
import com.packenius.library.xspdf.XSPDF;
import com.packenius.library.xspdf.XSTextWidthTooLargeException;

/**
 * @author Christian Packenius, 2015.
 */
public class ExampleTooLongWord {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        createPdfFile(1);
        createPdfFile(2);
    }

    private static void createPdfFile(int id) throws IOException {
        XSPDF xsPDF = new XSPDF();
        xsPDF.setColumnWidthCheck(id == 2);
        xsPDF.print("Hello");
        try {
            xsPDF
                .print("WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW");
        } catch (XSTextWidthTooLargeException exc) {
            System.out.println("Got exception in too long word!");
        }
        xsPDF.print("world!");
        xsPDF.createPdf(ExampleTooLongWord.class.getSimpleName() + "-" + id + ".pdf");
    }
}
