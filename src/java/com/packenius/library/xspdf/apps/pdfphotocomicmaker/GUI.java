/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf.apps.pdfphotocomicmaker;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.ListCellRenderer;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Full GUI of PDF Photo Comic Maker.
 */
public class GUI implements FileListener, ListCellRenderer<Component>, ListSelectionListener {
    private final JFrame frame;

    private DefaultListModel<Component> imageListModel;

    private JList<Component> imageList;

    private JScrollPane imageListScroller;

    private ImagePanel imagePanel;

    private List<File> imageFiles = new ArrayList<File>();

    File currentDirectory = null;

    /**
     * Constructor.
     */
    public GUI() {
        frame = new JFrame("PDF Photo Comic Maker - (w) by Christian Packenius, 2013");
        frame.setSize(800, 600);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        int hScroll = ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
        int vScroll = ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
        DirectoryTree directoryTree = new DirectoryTree();
        directoryTree.addFileListener(this);
        JScrollPane directoryTreeScroller = new JScrollPane(directoryTree.tree, vScroll, hScroll);

        imageListModel = new DefaultListModel<Component>();
        imageList = new JList<Component>(imageListModel);
        imageList.setCellRenderer(this);
        imageList.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        imageList.setVisibleRowCount(1);
        imageList.addListSelectionListener(this);
        imageListScroller = new JScrollPane(imageList, vScroll, hScroll);

        imagePanel = new ImagePanel();
        JScrollPane imagePanelScroller = new JScrollPane(imagePanel, vScroll, hScroll);

        int hSplit = JSplitPane.HORIZONTAL_SPLIT;

        JPanel borderPanel = new JPanel(new BorderLayout());
        borderPanel.add(imageListScroller, BorderLayout.NORTH);
        borderPanel.add(imagePanelScroller, BorderLayout.CENTER);
        JSplitPane splitPane = new JSplitPane(hSplit, directoryTreeScroller, borderPanel);
        splitPane.setDividerLocation(200);

        Container cp = frame.getContentPane();
        cp.add(splitPane);

        frame.setVisible(true);
    }

    /**
     * @see com.packenius.library.xspdf.apps.pdfphotocomicmaker.FileListener#changedDirectory(java.io.File)
     */
    public void changedDirectory(File currentDirectory) {
        imageListModel.clear();
        updateImageList(currentDirectory);
        frame.validate();
    }

    /**
     * @param currentDirectory
     */
    private void updateImageList(final File currentDirectory) {
        this.currentDirectory = currentDirectory;
        final GUI gui = this;
        imageFiles.clear();
        Thread thread = new Thread(new Runnable() {
            public void run() {
                imageListModel.clear();
                synchronized (GUI.class) {
                    File[] files = currentDirectory.listFiles();
                    if (files != null) {
                        for (File file : files) {
                            String filename = file.getName();
                            if (filename.toLowerCase().endsWith(".jpg")) {
                                try {
                                    // System.out.println(filename);
                                    BufferedImage image = ImageIO.read(file);
                                    int width = image.getWidth();
                                    int height = image.getHeight();
                                    if (width >= height) {
                                        height = height * 128 / width;
                                        width = 128;
                                    } else {
                                        width = width * 128 / height;
                                        height = 128;
                                    }
                                    BufferedImage icon = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                                    Graphics gr = icon.getGraphics();
                                    gr.drawImage(image, (128 - width) / 2, (128 - height) / 2, width, height, null);
                                    gr.dispose();
                                    final JButton imageButton = new JButton(filename);
                                    imageButton.setIcon(new ImageIcon(icon));
                                    imageButton.setVerticalTextPosition(SwingConstants.BOTTOM);
                                    imageButton.setHorizontalTextPosition(SwingConstants.CENTER);
                                    if (gui.currentDirectory != currentDirectory) {
                                        break;
                                    }
                                    imageFiles.add(file);
                                    try {
                                        SwingUtilities.invokeAndWait(new Runnable() {
                                            public void run() {
                                                imageListModel.addElement(imageButton);
                                                frame.validate();
                                            }
                                        });
                                    } catch (Exception exception) {
                                        // Ignore!
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.start();
    }

    /**
     * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
     */
    public void valueChanged(ListSelectionEvent arg0) {
        try {
            int selectedIndex = imageList.getSelectedIndex();
            if (selectedIndex < 0) {
                imagePanel.setCurrentImage(null, null);
            } else {
                File imageFile = imageFiles.get(selectedIndex);
                imagePanel.setCurrentImage(ImageIO.read(imageFile), imageFile);
            }
            frame.validate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean,
     *      boolean)
     */
    public Component getListCellRendererComponent(JList<? extends Component> list, Component value, int index,
            boolean isSelected, boolean cellHasFocus) {
        return value;
    }
}
