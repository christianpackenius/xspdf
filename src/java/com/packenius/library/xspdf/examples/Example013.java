package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Line leading examples.
 */
public class Example013 implements XS {
	/**
	 * @param args
	 *            Unused.
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = new XSPDF();
		xsPDF.rotatePage();
		xsPDF.setFontSize(24.0);
		xsPDF.print("First line (line leading 50% - default).");
		xsPDF.print("\rSecond line - now set line leading to zero:");
		xsPDF.setLineLeading(0);
		xsPDF.print("\rThird line - after this line the line leading will be 300%:");
		xsPDF.setLineLeadingInPercent(300);
		xsPDF.print("\rForth line. Another font size:");
		xsPDF.setFontSize(18);
		xsPDF.print("\nTwo lines with negative line leading between them:");
		xsPDF.print("\rABCDEFG");
		xsPDF.setLineLeading(-10);
		xsPDF.print("\rHIJKLMN");
		xsPDF.createPdf("pdf/Example 013.pdf");
	}
}
