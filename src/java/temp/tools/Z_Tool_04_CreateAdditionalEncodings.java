/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package temp.tools;

import com.packenius.library.xspdf.XSUnicodeMapping;

/**
 * @author Christian Packenius, 2013.
 */
public class Z_Tool_04_CreateAdditionalEncodings {
    static final int[] freeIndexes = new int[] {127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142,
        143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 176, 181, 190, 192, 201, 204,
        209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 226, 228, 229, 230, 231, 236, 237, 238,
        239, 240, 242, 243, 244, 246, 247};

    static final String[] notStandardChars = new String[] {"\\u00cf", "\\u00e9", "\\u0103", "\\u0171", "\\u011b", "\\u0178",
        "\\u00f7", "\\u00dd", "\\u00c2", "\\u00e1", "\\u00db", "\\u00fd", "\\u0219", "\\u00ea", "\\u016e", "\\u00dc",
        "\\u0105", "\\u00da", "\\u0173", "\\u00cb", "\\u0110", "\\uf6c3", "\\u00a9", "\\u0112", "\\u010d", "\\u00e5",
        "\\u0145", "\\u013a", "\\u00e0", "\\u0162", "\\u0106", "\\u00e3", "\\u0116", "\\u0161", "\\u015f", "\\u00ed",
        "\\u25ca", "\\u0158", "\\u0122", "\\u00fb", "\\u00e2", "\\u0100", "\\u0159", "\\u00e7", "\\u017b", "\\u00de",
        "\\u014c", "\\u0154", "\\u015a", "\\u010f", "\\u016a", "\\u016f", "\\u00b3", "\\u00d2", "\\u00c0", "\\u0102",
        "\\u00d7", "\\u00fa", "\\u0164", "\\u2202", "\\u00ff", "\\u0143", "\\u00ee", "\\u00ca", "\\u00e4", "\\u00eb",
        "\\u0107", "\\u0144", "\\u016b", "\\u0147", "\\u00cd", "\\u00b1", "\\u00a6", "\\u00ae", "\\u011e", "\\u0130",
        "\\u2211", "\\u00c8", "\\u0155", "\\u014d", "\\u0179", "\\u017d", "\\u2265", "\\u00d0", "\\u00c7", "\\u013c",
        "\\u0165", "\\u0119", "\\u0172", "\\u00c1", "\\u00c4", "\\u00e8", "\\u017a", "\\u012f", "\\u00d3", "\\u00f3",
        "\\u0101", "\\u015b", "\\u00ef", "\\u00d4", "\\u00d9", "\\u2206", "\\u00fe", "\\u00b2", "\\u00d6", "\\u00b5",
        "\\u00ec", "\\u0151", "\\u0118", "\\u0111", "\\u00be", "\\u015e", "\\u013e", "\\u0136", "\\u0139", "\\u2122",
        "\\u0117", "\\u00cc", "\\u012a", "\\u013d", "\\u00bd", "\\u2264", "\\u00f4", "\\u00f1", "\\u0170", "\\u00c9",
        "\\u0113", "\\u011f", "\\u00bc", "\\u0160", "\\u0218", "\\u0150", "\\u00b0", "\\u00f2", "\\u010c", "\\u00f9",
        "\\u221a", "\\u010e", "\\u0157", "\\u00d1", "\\u00f5", "\\u0156", "\\u013b", "\\u00c3", "\\u0104", "\\u00c5",
        "\\u00d5", "\\u017c", "\\u011a", "\\u012e", "\\u0137", "\\u2212", "\\u00ce", "\\u0148", "\\u0163", "\\u00ac",
        "\\u00f6", "\\u00fc", "\\u2260", "\\u0123", "\\u00f0", "\\u017e", "\\u0146", "\\u00b9", "\\u012b", "\\u20ac"};

    // static final String[] endings = {"umlaut", "circumflex", "breve",
    // "accent", "dieresis", "acute"};
    // static final int[] endingsCounter = new int[endings.length];

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(freeIndexes.length + " free indizes.");
        System.out.println(notStandardChars.length + " not standard characters:");
        int i = 0;
        int encID = 1;
        int lastCodeIndex = 0;
        String differences = "";
        for (String charAsString : notStandardChars) {
            int codeIndex = freeIndexes[i];
            char ch = (char) Integer.parseInt(charAsString.substring(2), 16);
            String uniName = XSUnicodeMapping.getNameFromCode(ch);
            if (lastCodeIndex + 1 == codeIndex) {
                lastCodeIndex++;
                differences += " /" + uniName;
            } else {
                lastCodeIndex = codeIndex;
                differences += " " + codeIndex + " /" + uniName;
            }
            System.out.println("    altFontEncoding" + encID + ".put('" + charAsString + "', (char)" + codeIndex + "); // "
                + uniName);
            i++;
            i %= freeIndexes.length;
            if (i == 0) {
                encID++;
                differences += "\r\n";
            }
        }
        // for (char ch : notStandardChars) {
        // String uniName = XSUnicodeMapping.getNameFromCode(ch);
        // System.out.println("> " + uniName);
        // for (int i = 0; i < endings.length; i++) {
        // String ending = endings[i];
        // if (uniName.endsWith(ending)) {
        // endingsCounter[i]++;
        // }
        // }
        // }
        // for (int i = 0; i < endings.length; i++) {
        // String ending = endings[i];
        // System.out.println("Ending '" + ending + "': " + endingsCounter[i] +
        // " exemplars.");
        // }
        System.out.println("Differences:");
        System.out.println(differences);
    }
}
