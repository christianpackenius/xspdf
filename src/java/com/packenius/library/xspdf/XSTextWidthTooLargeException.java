/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * This exception is thrown if the text part is too large for being printed in any text line / column / page.
 * @author Christian Packenius, 2013.
 */
public class XSTextWidthTooLargeException extends XSPdfException {
    private static final long serialVersionUID = -4820651005373236064L;

    /**
     * Constructor.
     * @param errorMessage Message of the exception.
     */
    public XSTextWidthTooLargeException(String errorMessage) {
        super(errorMessage);
    }
}
