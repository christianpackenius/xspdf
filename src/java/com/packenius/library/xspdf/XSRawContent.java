/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Interface of a single PDF/Postscript order that can be written into the content stream of a PDF page object.
 * @author Christian Packenius, 2013.
 */
interface XSRawContent {
    /**
     * Get the raw PDF content stream order of this object.
     * @param x Base of X coordinate.
     * @param y Base of Y coordinate. Use this to convert the points from left/top (user-friendly) to left bottom (PDF
     *            specification art) based coordinates.
     * @param xFactor Horizontal factor (usually 1.0).
     * @param yFactor Vertical factor (usually 1.0).
     * @return String replacement of this raw object.
     */
    String getRawContent(double x, double y, double xFactor, double yFactor);
}
