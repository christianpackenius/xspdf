/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Destination that marks a page.
 * @author Christian Packenius, 2013.
 */
class XSPageDestination implements XSDestination {
    /**
     * Number of page to be marked (>= 0).
     */
    final int pageID;

    /**
     * Constructor.
     * @param pageID Page ID, starting with 0.
     */
    XSPageDestination(int pageID) {
        this.pageID = pageID;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(com.packenius.library.xspdf.XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        int pageObjectID = xsPDF.pages.get(pageID).pdfObjectID;
        return "/Dest[" + pageObjectID + " 0 R/XYZ null null 0]>>";
    }
}
