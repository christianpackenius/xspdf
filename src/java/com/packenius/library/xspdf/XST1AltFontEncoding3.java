package com.packenius.library.xspdf;

/**
 * Alternative font encoding.
 * 
 * @author Christian Packenius, 2013.
 */
class XST1AltFontEncoding3 extends XSAlternativeFontEncoding {
	/**
	 * Maps an Utf-8 (Unicode?) code (0..65535) to the byte in this font
	 * encoding.
	 */
	static final byte[] altFontEncoding3 = new byte[65536];

	private static final char[] keyValueArray = new char[] { '\u013b', (char) 127, '\u00c3', (char) 128, '\u0104',
			(char) 129, '\u00c5', (char) 130, '\u00d5', (char) 131, '\u017c', (char) 132, '\u011a', (char) 133,
			'\u012e', (char) 134, '\u0137', (char) 135, '\u2212', (char) 136, '\u00ce', (char) 137, '\u0148',
			(char) 138, '\u0163', (char) 139, '\u00ac', (char) 140, '\u00f6', (char) 141, '\u00fc', (char) 142,
			'\u2260', (char) 143, '\u0123', (char) 144, '\u00f0', (char) 145, '\u017e', (char) 146, '\u0146',
			(char) 147, '\u00b9', (char) 148, '\u012b', (char) 149, '\u20ac', (char) 150, };

	private static final String altFontEnc3Diffs = "127 /Lcommaaccent /Atilde /Aogonek /Aring /Otilde /zdotaccent /Ecaron "
			+ "/Iogonek /kcommaaccent /minus /Icircumflex /ncaron "
			+ "/tcommaaccent /logicalnot /odieresis /udieresis /notequal /gcommaaccent "
			+ "/eth /zcaron /ncommaaccent /onesuperior /imacron /Euro";

	static {
		for (int i = 0; i < keyValueArray.length; i += 2) {
			altFontEncoding3[keyValueArray[i]] = (byte) keyValueArray[i + 1];
		}
	}

	public static final XST1AltFontEncoding3 instance = new XST1AltFontEncoding3();

	private XST1AltFontEncoding3() {
		// Singleton.
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getPdfDifferencesListContent()
	 */
	@Override
	public String getPdfDifferencesListContent() {
		return altFontEnc3Diffs;
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getID()
	 */
	@Override
	public String getID() {
		return "3";
	}

	/**
	 * @see com.packenius.library.xspdf.XSAlternativeFontEncoding#getCharacterEncodingMap()
	 */
	@Override
	public byte[] getCharacterEncodingMap() {
		return altFontEncoding3;
	}
}
