/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package temp.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Add LGPL header to every source file under "src/java".
 * @author Christian Packenius.
 */
public class Z_Tool_98_AddGplHeaderBeforeDeploying {
    // private static File baseDir;

    private static int javaCount = 0;

    private static int otherCount = 0;

    private static List<String> gplHeader = new ArrayList<String>();

    /**
     * Start application.
     * @param args Unused.
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // baseDir = args.length == 0 ? new File(".").getCanonicalFile() : new File(args[0]);
        readDeployingHeader();
        // for (File dir : baseDir.listFiles()) {
        // if (dir.isDirectory()) {
        // String projectName = dir.getName();
        // if (!projectName.startsWith(".")) {
        // work(new File(baseDir, "../" + projectName + "/src/java"));
        // }
        // }
        // }
        work(new File("."));
        System.out.println("Worked " + javaCount + " java and " + otherCount + " non-java files.");
    }

    private static void readDeployingHeader() throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(new File("docs/deploying.txt")));
        String line;
        while ((line = in.readLine()) != null) {
            if (line.length() >= 2 && line.charAt(1) == '*') {
                System.out.println(line);
                gplHeader.add(line);
            }
        }
        in.close();
    }

    private static void work(File file) throws IOException {
        if (file.isDirectory()) {
            workDir(file);
        } else if (file.isFile()) {
            workFile(file);
        }
    }

    private static void workFile(File file) throws IOException {
        if (file.getName().toLowerCase().endsWith(".java")) {
            javaCount++;
            workJavaFile(file);
        } else {
            otherCount++;
        }
    }

    private static void workJavaFile(File file) throws IOException {
        System.out.println("Working: " + file.getCanonicalPath());
        File dummyFile = new File("dummy.java");
        BufferedReader in = new BufferedReader(new FileReader(file));
        PrintStream out = new PrintStream(new FileOutputStream(dummyFile));
        String line;

        writeNewGplHeader(out);
        line = ignoreOldHeader(in);
        copyJavaFileContent(in, out, line);

        out.close();
        in.close();

        file.delete();
        dummyFile.renameTo(file);
    }

    private static void copyJavaFileContent(BufferedReader in, PrintStream out, String line) throws IOException {
        if (line != null) {
            out.println(line);
            while ((line = in.readLine()) != null) {
                out.println(line);
            }
        }
    }

    private static String ignoreOldHeader(BufferedReader in) throws IOException {
        String line;
        while ((line = in.readLine()) != null) {
            if (line.length() < 2 || line.charAt(1) != '*') {
                break;
            }
        }
        return line;
    }

    private static void writeNewGplHeader(PrintStream out) {
        for (String s : gplHeader) {
            out.println(s);
        }
    }

    private static void workDir(File dir) throws IOException {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                work(file);
            }
        }
    }
}
