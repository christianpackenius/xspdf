/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSFontParameter;
import com.packenius.library.xspdf.XSPDF;

/**
 * Example for usage of tab character (0x09 / '\t') when disabling Auto-Formatting.
 * @author Christian Packenius, 2013.
 */
public class ExampleNoFormattingWithTabs implements XS {
    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        XSPDF xsPDF = new XSPDF().use(NO_FORMATTING);
        xsPDF.setFont(COURIER, 10, XSFontParameter.Bold);
        xsPDF.print("With default tabbing (4):\r");
        printSampleStrings(xsPDF);
        for (int k = -3; k <= 6; k += 3) {
            xsPDF.newPage();
            xsPDF.setFont(COURIER, 10, XSFontParameter.Bold);
            xsPDF.print("With tabbing " + k + ":\r");
            xsPDF.setTabSpacesCount(k);
            printSampleStrings(xsPDF);
        }
        File pdfFile = new File(ExampleNoFormattingWithTabs.class.getSimpleName() + ".pdf");
        xsPDF.createPdf(pdfFile);
        Desktop.getDesktop().open(pdfFile);
    }

    private static void printSampleStrings(XSPDF xsPDF) {
        xsPDF.setFont(COURIER, 10, XSFontParameter.Ordinary);
        xsPDF.print("          111111111122222222223333333333\n");
        xsPDF.print("0123456789012345678901234567890123456789\n");
        xsPDF.print("T\tT\tT\tT\tT\tT\n");
        xsPDF.print("Hello\tWorld!\n");
        xsPDF.print("Hello");
        xsPDF.print("\tWorld!\n");
        xsPDF.print("This\tis\ta\tsmall\tsentence.\r");
        xsPDF.print("This");
        xsPDF.print("\tis");
        xsPDF.print("\ta");
        xsPDF.print("\tsmall");
        xsPDF.print("\tsentence.\r");
        String stars = "";
        for (int i = 0; i < 15; i++) {
            xsPDF.print(stars + "\t" + stars + "\t!\r");
            stars += "*";
        }
        xsPDF.print("\n");
    }
}
