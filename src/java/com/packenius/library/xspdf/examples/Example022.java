package com.packenius.library.xspdf.examples;

import java.io.IOException;
import com.packenius.library.xspdf.XS;
import com.packenius.library.xspdf.XSPDF;

/**
 * Setting line page destination and creating a links to it.
 */
public class Example022 implements XS {
	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		XSPDF xsPDF = XSPDF.getInstance();
		xsPDF.print("100 lines to jump between...\r\r");

		for (int i = 1; i <= 100; i++) {
			xsPDF.setLineDestination("line-" + i);
			xsPDF.print("Line " + i);
			int rnd = (int) (Math.random() * 100) + 1;
			xsPDF.startLink("line-" + rnd);
			xsPDF.print(" (Go to line " + rnd + ".)\r");
		}

		xsPDF.createPdf("pdf/Example 022.pdf");
	}
}
