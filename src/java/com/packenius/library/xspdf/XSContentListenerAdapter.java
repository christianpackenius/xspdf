/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Adapter for XSContentListener interface.
 * @author Christian Packenius, 2013.
 */
public class XSContentListenerAdapter implements XSContentListener {
    /**
     * @see com.packenius.library.xspdf.XSContentListener#newPage(XSPDF, int)
     */
    public void newPage(XSPDF xsPDF, int pageNumber) {
        // Please override me!
    }

    /**
     * @see com.packenius.library.xspdf.XSContentListener#newTextLine(XSPDF, int, java.lang.String, int)
     */
    public void newTextLine(XSPDF xsPDF, int pageNumber, String columnName, int lineNumber) {
        // Please override me!
    }

    /**
     * @see com.packenius.library.xspdf.XSContentListener#newColumn(XSPDF, int, java.lang.String)
     */
    public void newColumn(XSPDF xsPDF, int pageID, String columnName) {
        // Please override me!
    }

    /**
     * @see com.packenius.library.xspdf.XSContentListener#newTextPart(com.packenius.library.xspdf.XSPDF, int, java.lang.String,
     *      int, java.lang.String)
     */
    public void newTextPart(XSPDF xsPDF, int pageID, String columnName, int textLineID, String textPart) {
        // Please override me!
    }
}
