/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * A text word with a font usage. This is part of a text line on a PDF page.
 * @author Christian Packenius.
 */
class XSTextPart {
    final String text;

    final XSFontUsage fontUsage;

    final double width;

    final XSLink link;

    double realX1 = 0.0;

    double realX2 = 0.0;

    /**
     * Constructor.
     * @param textWord
     * @param fontUsage
     * @param width
     * @param link
     */
    public XSTextPart(String textWord, XSFontUsage fontUsage, double width, XSLink link) {
        text = textWord;
        this.fontUsage = fontUsage;
        this.width = width;
        this.link = link;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return text;
    }
}
