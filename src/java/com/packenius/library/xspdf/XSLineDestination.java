/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Destination that marks a line on a page.
 * @author Christian Packenius, 2013.
 */
class XSLineDestination implements XSDestination {
    /**
     * Number of page to be marked (>= 0).
     */
    final int pageID;

    /**
     * Line on the page (from top of the page).
     */
    final double lineY;

    /**
     * Constructor.
     * @param pageID Page ID, starting with 0.
     * @param lineY Line on the page (from top of the page).
     */
    XSLineDestination(int pageID, double lineY) {
        this.pageID = pageID;
        this.lineY = lineY;
    }

    /**
     * @see com.packenius.library.xspdf.XSPdfContent#getPdfContent(com.packenius.library.xspdf.XSPDF)
     */
    public String getPdfContent(XSPDF xsPDF) {
        XSPage page = xsPDF.pages.get(pageID);
        int pageObjectID = page.pdfObjectID;
        return "/Dest[" + pageObjectID + " 0 R/XYZ null " + lineY + " 0]>>";
    }
}
