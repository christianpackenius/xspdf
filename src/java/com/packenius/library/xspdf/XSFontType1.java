/**
 * xsPDF - A Java PDF creation library
 *
 * Copyright (C) 2013-2015 Christian Packenius, christian@packenius.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU LESSER GENERAL PUBLIC LICENSE as published
 * by the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.packenius.library.xspdf;

/**
 * Font names for all 14 Type 1 standard fonts.
 * @author Christian Packenius, 2013.
 */
@Deprecated
public enum XSFontType1 {
    /**
     * "ORDINARY" Helvetica font.
     */
    Helvetica(new XSType1StdFontHelvetica()),

    /**
     * "Bold" Helvetica font.
     */
    HelveticaBold(new XSType1StdFontHelveticaBold()),

    /**
     * "Oblique" Helvetica font.
     */
    HelveticaOblique(new XSType1StdFontHelveticaOblique()),

    /**
     * "Bold, oblique" Helvetica font.
     */
    HelveticaBoldOblique(new XSType1StdFontHelveticaBoldOblique()),

    /**
     * "ORDINARY" Courier font.
     */
    Courier(new XSType1StdFontCourier()),

    /**
     * "Bold" Courier font.
     */
    CourierBold(new XSType1StdFontCourierBold()),

    /**
     * "Oblique" Courier font.
     */
    CourierOblique(new XSType1StdFontCourierOblique()),

    /**
     * "Bold, oblique" Courier font.
     */
    CourierBoldOblique(new XSType1StdFontCourierBoldOblique()),

    /**
     * "ORDINARY" Times font.
     */
    Times(new XSType1StdFontTimesRoman()),

    /**
     * "Bold" Times font.
     */
    TimesBold(new XSType1StdFontTimesBold()),

    /**
     * "Italic" Times font.
     */
    TimesItalic(new XSType1StdFontTimesItalic()),

    /**
     * "Bold, Italic" Times font.
     */
    TimesBoldItalic(new XSType1StdFontTimesBoldItalic()),

    /**
     * Symbol font.
     */
    Symbol(new XSType1StdFontSymbol()),

    /**
     * Symbol font.
     */
    ZapfDingbats(new XSType1StdFontZapfDingbats());

    // final String fontName;
    //
    // final String logicalName;

    /**
     * Real font behind this virtual font.
     */
    public final XSType1StdFont font;

    XSFontType1(XSType1StdFont font) {
        // this.fontName = fontName;
        // this.logicalName = logicalName;
        this.font = font;
    }

    // /**
    // * @see com.packenius.library.xspdf.XSFontType#getFontName()
    // */
    // public String getFontName() {
    // return fontName;
    // }
    //
    // /**
    // * @see com.packenius.library.xspdf.XSFontType#getLogicalFontName()
    // */
    // public String getLogicalFontName() {
    // return logicalName;
    // }
}
